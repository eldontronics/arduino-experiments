//DFRobot.com
//Compatible with the Arduino IDE 1.0
//Library version:1.1
#include <Wire.h>
#include <LiquidCrystal_I2C.h>

#if defined(ARDUINO) && ARDUINO >= 100
#define printByte(args)  write(args);
#else
#define printByte(args)  print(args,BYTE);
#endif

uint8_t bell[8]  = {0x4, 0xe, 0xe, 0xe, 0x1f, 0x0, 0x4};
uint8_t note[8]  = {0x2, 0x3, 0x2, 0xe, 0x1e, 0xc, 0x0};
uint8_t clock[8] = {0x0, 0xe, 0x15, 0x17, 0x11, 0xe, 0x0};
uint8_t heart[8] = {0x0, 0xa, 0x1f, 0x1f, 0xe, 0x4, 0x0};
uint8_t duck[8]  = {0x0, 0xc, 0x1d, 0xf, 0xf, 0x6, 0x0};
uint8_t check[8] = {0x0, 0x1, 0x3, 0x16, 0x1c, 0x8, 0x0};
uint8_t cross[8] = {0x0, 0x1b, 0xe, 0x4, 0xe, 0x1b, 0x0};
uint8_t retarrow[8] = {  0x1, 0x1, 0x5, 0x9, 0x1f, 0x8, 0x4};

LiquidCrystal_I2C lcd1(0x3B, 16, 2); // set the LCD address to 0x27 for a 16 chars and 2 line display
LiquidCrystal_I2C lcd2(0x3F, 16, 2); // set the LCD address to 0x27 for a 16 chars and 2 line display
LiquidCrystal_I2C lcd3(0x3E, 16, 2); // set the LCD address to 0x27 for a 16 chars and 2 line display
int ledPin = 11;


void setup()
{

  lcd1.init();
  lcd1.noBacklight();
  lcd2.init();
  lcd2.noBacklight();
  lcd3.init();
  lcd3.noBacklight();


  delay(1000);
  lcd1.backlight();
  lcd2.backlight();
  lcd3.backlight();

  lcd1.createChar(0, bell);
  lcd1.createChar(1, note);
  lcd1.createChar(2, clock);
  lcd1.createChar(3, heart);
  lcd1.createChar(4, duck);
  lcd1.createChar(5, check);
  lcd1.createChar(6, cross);
  lcd1.createChar(7, retarrow);
  lcd1.home();

  lcd2.createChar(0, bell);
  lcd2.createChar(1, note);
  lcd2.createChar(2, clock);
  lcd2.createChar(3, heart);
  lcd2.createChar(4, duck);
  lcd2.createChar(5, check);
  lcd2.createChar(6, cross);
  lcd2.createChar(7, retarrow);
  lcd2.home();

  lcd3.createChar(0, bell);
  lcd3.createChar(1, note);
  lcd3.createChar(2, clock);
  lcd3.createChar(3, heart);
  lcd3.createChar(4, duck);
  lcd3.createChar(5, check);
  lcd3.createChar(6, cross);
  lcd3.createChar(7, retarrow);
  lcd3.home();


  lcd1.print("LCD 1");
  lcd2.print("LCD 2");
  lcd3.print("LCD 3");


  //lcd.printByte(3);
  delay(5000);
  displayKeyCodes();

}

// display all keycodes
void displayKeyCodes(void) {
  uint8_t i = 0;
  
  while (1) {
  
    lcd1.clear();
    lcd1.print("Codes 0x"); lcd1.print(i, HEX);
    lcd1.print("-0x"); lcd1.print(i + 16, HEX);
    lcd1.setCursor(0, 1);

    lcd2.clear();
    lcd2.print("Codes 0x"); lcd2.print(i, HEX);
    lcd2.print("-0x"); lcd2.print(i + 16, HEX);
    lcd2.setCursor(0, 1);

    lcd3.clear();
    lcd3.print("Codes 0x"); lcd3.print(i, HEX);
    lcd3.print("-0x"); lcd3.print(i + 16, HEX);
    lcd3.setCursor(0, 1);

    for (int j = 0; j < 16; j++) {
      lcd1.printByte(i + j);      
      lcd2.printByte(i + j);
      lcd3.printByte(i + j);
      
    }
    i += 16;

    delay(1000);
  }
}

void loop()
{


}


