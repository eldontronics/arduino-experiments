#if defined (__arm__) && defined (__SAM3X8E__)
#define _delay_us(us) delayMicroseconds(us)
#else
#include <util/delay.h>
#endif

#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define OLED_RESET 4
Adafruit_SSD1306 display(OLED_RESET);

#define LOGO16_GLCD_HEIGHT 64
#define LOGO16_GLCD_WIDTH  128

// DISTANCE SENSOR
int trigPin = 5;
int echoPin = 4;

long soundSpeed = 343; // meters per second



void setup()   {
  Serial.begin(9600);
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);  // initialize with the I2C addr 0x3C (for the 128x32)

  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);

  // Clear the buffer.
  display.clearDisplay();
  display.display(); // should always invoke this

}


void loop() {

  float duration, distance;

  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);

  duration = pulseIn(echoPin, HIGH); // value is in microseconds
  duration = duration / 1000000; // convert microseconds to seconds
  distance = (duration / 2) * soundSpeed;
  distance = distance * 100;

  if (distance < 2000) {
    Serial.print(distance);
    Serial.println(" centimeters");
    display.clearDisplay();
    display.setTextSize(3);
    display.setTextColor(WHITE);
    display.setCursor(0, 0);
    display.println(distance);
    display.setTextSize(1);
    display.setCursor(0, 25);
    display.println("CENTIMETERS");
    display.display();

  }

  delay(100);
}
