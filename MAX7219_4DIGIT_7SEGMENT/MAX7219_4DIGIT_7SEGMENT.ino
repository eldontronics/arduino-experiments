#include <SPI.h>

const int slaveSelect= 10; //pin used to enable the active slave
const int numberofDigits= 4;
const int maxCount= 9999;
int number=0;

void setup()
{
  Serial.begin(9600); 
  SPI.begin(); //initialize SPI
  pinMode(slaveSelect, OUTPUT);
  digitalWrite(slaveSelect, LOW); //select Slave
  
  //prepare the 7219 to display 7-segment data
  sendCommand (12,1); //normal mode (default is shutdown mode)
  sendCommand (15,0); //Display test off
  sendCommand (10,8); //set medium intensity (range is 0-15)
  sendCommand (11, numberofDigits); //7219 digit scan limit command
  sendCommand (9, 255); //decode command, use standard 7-segment digits
  digitalWrite(slaveSelect, HIGH); //deselect slave
}

void loop(){
  //display a number from serial port terminated by end of line character
  if(Serial.available())
  {
    char ch= Serial.read();
    if (ch == '\n')
    {
      displayNumber(number);
      number=0;
    }
    else{
      number= (number * 10) + ch - '0';
    }
  }
}

//function to display up to 4 digits on a 7-segment display
void displayNumber (int number)
{
  for (int i=0; i < numberofDigits; i++)
  {
    byte character= number % 10; //get the value of the rightmost digit
    if (number == 0 && i > 0) character = 0xf;
    sendCommand(numberofDigits-i, character);
    number= number/10;
  }
}

void sendCommand(int command, int value)
  {
    digitalWrite(slaveSelect, LOW); //chip select is active low
    SPI.transfer(command);
    SPI.transfer(value);
    digitalWrite(slaveSelect,HIGH);
  }
