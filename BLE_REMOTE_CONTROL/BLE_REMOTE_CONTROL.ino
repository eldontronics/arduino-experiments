#include <SoftwareSerial.h>
#include <AFMotor.h>

AF_DCMotor motor1(3); //1 means M1 in which the DC motor is connected
AF_DCMotor motor2(4); //2 means M2 in which the DC motor is connected

SoftwareSerial bluetooth (A0, A1); // A0 is TX on BLE, A1 is RX in BLE

String receivedBluetoothString = "";
boolean commandFinished = false;

String UP_PATTERN     = "-54-53-55-47-46-52";
String DOWN_PATTERN   = "-54-56-53-56-55-47-46-52";
String LEFT_PATTERN   = "-54-53-56-55-47-46-52";
String RIGHT_PATTERN  = "-54-56-53-55-47-46-52";


void setup() {
  bluetooth.begin(9600);
  Serial.begin(9600);

  motor1.setSpeed(255);
  motor1.run(RELEASE);

  motor2.setSpeed(255);
  motor2.run(RELEASE);

  //  motor1.run(FORWARD);
  //  motor2.run(FORWARD);

  Serial.println("Waiting for code...");
}

void loop() {

 // while (true) {}

  while (bluetooth.available() > 0) {
    char receivedBluetoothChar = bluetooth.read();
        if ((int)receivedBluetoothChar < 0) {
          receivedBluetoothString += (int)receivedBluetoothChar;
    
          if ((int)receivedBluetoothChar == -52) {
            commandFinished = true;
          }
        }

  }

    if (commandFinished) {
  
      if (receivedBluetoothString == UP_PATTERN) {
        Serial.println("UP");
        motor1.run(FORWARD);
        motor2.run(FORWARD);
        delay(300);
      }
  
      if (receivedBluetoothString == DOWN_PATTERN) {
        Serial.println("DOWN");
        motor1.run(BACKWARD);
        motor2.run(BACKWARD);
        delay(300);
      }
  
      if (receivedBluetoothString == LEFT_PATTERN) {
        Serial.println("LEFT");
        motor2.run(FORWARD);
        delay(300);
      }
  
      if (receivedBluetoothString == RIGHT_PATTERN) {
        Serial.println("RIGHT");
        motor1.run(FORWARD);
        delay(300);
      }
      else {
        motor1.run(RELEASE);
        motor2.run(RELEASE);
        delay(10);
      }
  
      commandFinished = false;
      receivedBluetoothString = "";

    }

}
