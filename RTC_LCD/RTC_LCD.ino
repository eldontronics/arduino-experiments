#include <MyRealTimeClock.h>
#include <LiquidCrystal_I2C.h>

LiquidCrystal_I2C lcd(0x3F,16,2);  // set the LCD address to 0x27 for a 16 chars and 2 line display
MyRealTimeClock myRTC(5, 6, 7); // Assign Digital Pins CLK, DAT, RST
 
bool isShowColon = false;

void setup() {
  
  // Second 00 | Minute 59 | Hour 10 | Day of Week 05 |Day 12 |  Month 07 | Year 2015 
  //myRTC.setDS1302Time(00, 45, 19, 01, 3, 06, 2017);
  Serial.begin(9600);
  lcd.init();
  lcd.noBacklight();
  delay(100);
  lcd.backlight();
  outputToLCD("System","ready...");
  
}

void loop() {
  displayTime3();  
  delay(1000);
}

String addZeroes(int value, int lengthDigit){
  String sValue = (String)value;
  int valueLength = sValue.length();

  for(int i = 1; i<= (lengthDigit - valueLength); i++){
    sValue = "0" + sValue;
  }

  return sValue;
}

void displayTime() {
//  myRTC.updateTime();
//  display.clearDisplay();
//  display.setCursor(0,0);
//  display.setTextSize(1);
//  display.setTextColor(WHITE);
//  display.print(myRTC.year);
//  display.print("/");
//  display.print(addZeroes(myRTC.month, 2));
//  display.print("/");
//  display.println(addZeroes(myRTC.dayofmonth, 2));
//  display.setTextSize(2);
//  display.print(addZeroes(myRTC.hours, 2));
//  display.print(":");
//  display.print(addZeroes(myRTC.minutes, 2));
//  display.print(":");
//  display.print(addZeroes(myRTC.seconds, 2));
//  display.display();
}

void displayTime2() {
//  myRTC.updateTime();
//  display.clearDisplay();
//  display.setCursor(0,0);
//  display.setTextSize(1);
//  display.setTextColor(WHITE);
//  display.println("Day  : " + getDayOfTheWeekName(myRTC.dayofweek));
//  display.println("Date : " + getMonthName(myRTC.month) + " " + myRTC.dayofmonth + ", " + myRTC.year); 
//  display.println("Time : ");
//
//  display.setCursor(0,30);
//  display.setTextSize(2);
//  display.println(displayFormattedTime());
//  display.display();
}

void displayTime3() {
  myRTC.updateTime();
//  display.print(getDayOfTheWeekName(myRTC.dayofweek).substring(0,3) + ", ");
//  display.println(addZeroes(myRTC.month,2) + "-" + addZeroes(myRTC.dayofmonth, 2) + "-" + addZeroes(myRTC.year, 2)); 
//  display.setCursor(10,30);
//  display.setTextSize(2);
//outputToLCD(addZeroes(myRTC.month,2) + "-" + addZeroes(myRTC.dayofmonth, 2) + "-" + addZeroes(myRTC.year, 2), displayFormattedTime());

  outputToLCD("  " + addZeroes(myRTC.dayofmonth, 2) + " " + getMonthName(myRTC.month).substring(0,3)  + " " + addZeroes(myRTC.year, 2), "  " + displayFormattedTime());
 // display.display();
}

String displayFormattedTime(){
  String dayPeriod = " AM";
  String dayHours = myRTC.hours + "";

  isShowColon = (myRTC.seconds % 2 == 0);
  if(myRTC.hours > 12){
    dayPeriod = " PM";
    dayHours = myRTC.hours - 12;
  }
  else{
    dayHours = myRTC.hours;
  }

  return ("" + dayHours + ":" + addZeroes(myRTC.minutes, 2) + ":" + addZeroes(myRTC.seconds, 2) + dayPeriod);


//  if(isShowColon){
//    return ("" + dayHours + ":" + addZeroes(myRTC.minutes, 2) + ":" + addZeroes(myRTC.seconds, 2) + dayPeriod);
//  }
//  return ("" + dayHours + " " + addZeroes(myRTC.minutes, 2) + ":" + addZeroes(myRTC.seconds, 2) + dayPeriod);
}

String getDayOfTheWeekName(int iDayOfTheWeek){
  switch(iDayOfTheWeek){
    case 1:{
      return "Sunday";
      break;
    }
    case 2:{
      return "Monday";
      break;
    }
    case 3:{
      return "Tuesday";
      break;
    }
    case 4:{
      return "Wednesday";
      break;
    }
    case 5:{
      return "Thursday";
      break;
    }
    case 6:{
      return "Friday";
      break;
    }
    case 7:{
      return "Saturday";
      break;
    }
  }
}

String getMonthName(int iMonth){
  switch(iMonth){
    case 1:{
      return "January";
      break;
    }
   case 2:{
      return "February";
      break;
    }
   case 3:{
      return "March";
      break;
    }
   case 4:{
      return "April";
      break;
    }
   case 5:{
      return "May";
      break;
    }
   case 6:{
      return "June";
      break;
    }
   case 7:{
      return "July";
      break;
    }
   case 8:{
      return "August";
      break;
    }
   case 9:{
      return "September";
      break;
    }
   case 10:{
      return "October";
      break;
    }
   case 11:{
      return "November";
      break;
    }
   case 12:{
      return "December";
      break;
    }
  }  
}

void outputToLCD(String rowOneMessage, String rowTwoMessage){
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print(rowOneMessage);
  lcd.setCursor(0, 1);
  lcd.print(rowTwoMessage);  
}

