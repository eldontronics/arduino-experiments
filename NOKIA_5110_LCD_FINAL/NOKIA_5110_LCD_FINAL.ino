/*********************************************************************
  This is an example sketch for our Monochrome Nokia 5110 LCD Displays

  Pick one up today in the adafruit shop!
  ------> http://www.adafruit.com/products/338

  These displays use SPI to communicate, 4 or 5 pins are required to
  interface

  Adafruit invests time and resources providing this open source code,
  please support Adafruit and open-source hardware by purchasing
  products from Adafruit!

  Written by Limor Fried/Ladyada  for Adafruit Industries.
  BSD license, check license.txt for more information
  All text above, and the splash screen must be included in any redistribution
*********************************************************************/

#include <SPI.h>
#include <Adafruit_GFX.h>
#include <Adafruit_PCD8544.h>

// Software SPI (slower updates, more flexible pin options):
// pin 7 - Serial clock out (SCLK)
// pin 6 - Serial data out (DIN)
// pin 5 - Data/Command select (D/C)
// pin 4 - LCD chip select (CS)
// pin 3 - LCD reset (RST)

Adafruit_PCD8544 display = Adafruit_PCD8544(3, 4, 5, 7, 6); // CLK, DIN, DC, CS, RST

// Hardware SPI (faster, but must use certain hardware pins):
// SCK is LCD serial clock (SCLK) - this is pin 13 on Arduino Uno
// MOSI is LCD DIN - this is pin 11 on an Arduino Uno
// pin 5 - Data/Command select (D/C)
// pin 4 - LCD chip select (CS)
// pin 3 - LCD reset (RST)
//Adafruit_PCD8544 display = Adafruit_PCD8544(5, 4, 3);
// Note with hardware SPI MISO and SS pins aren't used but will still be read
// and written to during SPI transfer.  Be careful sharing these pins

int seconds = 0;
int minutes = 0;
int contrastLevel = 0;

void setup()   {
  Serial.begin(9600);
  display.begin();
  display.setContrast(20); // 20/40? for the red one, 25 for the blue one.
  display.clearDisplay();
  display.display(); // show splashscreen
  delay(100);
}


void loop() {

  contrastLevel = map( analogRead(A0), 0, 1023, 0, 50);
  //Serial.println(contrastLevel);
  display.setContrast(contrastLevel);
  display.display();

  display.clearDisplay();
  display.setTextSize(2);
  display.setTextColor(BLACK);
  display.setCursor(0, 0);
  display.println(millis());
  display.println(millis() / 100);
  display.println(millis() / 1000);
  display.display();

  display.setTextSize(1);
  display.setCursor(68, 40);
  display.setTextColor(BLACK);
  //display.print("Cntrst:");
  display.print(contrastLevel);
  display.display();

  delay(10);
  //  display.setCursor(0, 27);
  //  display.print(seconds);





}
