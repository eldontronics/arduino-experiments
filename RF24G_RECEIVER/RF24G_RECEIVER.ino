
/* This is a small sketch that listens
   for packets and forwards them back to the sender.
*/

#include <rf24g.h>
// we must instantiate the RF24_G object outside of the setup function so it is available in the loop function
RF24_G test;

#include <LiquidCrystal_I2C.h>
LiquidCrystal_I2C lcd(0x3B, 16, 2); // set the LCD address to 0x27 for a 16 chars and 2 line display

int i = 0;
void setup() {

  lcd.init();                      // initialize the lcd
  lcd.backlight();
  lcd.setCursor(0, 0);
  lcd.print("Initializing");
  lcd.setCursor(0, 1);
  lcd.print("LCD 16x2...");
  delay(2000);

  outputToLCD("NRF24L01 is now", "ready to receive", 2000);
  Serial.begin(9600);
  // create the RF24G object with an address of 1, using pins 7 and 8
  test = RF24_G(1, 7, 8);
  // print out the details of the radio's configuration (useful for debug)
}

void loop() {
  // declare packet variable
  packet receiver;

  // declare string to place the packet payload in
  int payload;

  // check if the radio has any packets in the receive queue
  if (test.available() == true) {
    Serial.println("packet received!");
    // read the data into the packet
    test.read(&receiver);

    // print the packet number of the received packet
    // if these are not consecutive packets are being lost due to timeouts.
    Serial.print("count: ");
    Serial.println(receiver.getCnt());

    // print the source address of the received packet
    Serial.print("address: ");
    Serial.println(receiver.getAddress());

    // load the payload into the payload string
    receiver.readPayload(&payload, sizeof(int));

    // print the payload
    Serial.print("payload: ");
    Serial.println(payload);
    Serial.println("");
    outputToLCD("Payload :", (String)payload, 100);

    // since the address in the packet object is already
    // set to the address of the receiver, it doesn't need to be changed
    // hence, we can write the packet back to the receiver
    // we may check to see if the transmission failed, if so we just drop the packet
    if (test.write(&receiver) == false) {
      Serial.println("transmit back failed!");
      Serial.println("dropping packet...");
    }
  }
  else {
    //outputToLCD("No data to","receive...", 100);
  }
}

void outputToLCD(String message1, String message2, int delayValue) {
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print(message1);
  lcd.setCursor(0, 1);
  lcd.print(message2);
  delay(delayValue);
}

