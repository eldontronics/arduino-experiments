#include "LedControl.h"
#include <MyRealTimeClock.h>

MyRealTimeClock myRTC(5, 6, 7); // Assign Digital Pins CLK, DAT, RST

int pinDIN = 12;
int pinCLK = 11;
int pinCS  = 10;
int numLedMatrix = 4;

LedControl lc = LedControl(pinDIN, pinCLK, pinCS, numLedMatrix);

// delay time between faces
unsigned long delaytime = 1000;


byte num[10][8] {
  {B00000000, B00000000, B00111110, B01010001, B01001001, B01000101, B00111110, B00000000}, // 0
  {B00000000, B00000000, B01000010, B01000010, B01111111, B01000000, B01000000, B00000000}, // 1
  {B00000000, B00000000, B01000010, B01100001, B01010001, B01001001, B01000110, B00000000}, // 2
  {B00000000, B00000000, B00100001, B01000001, B01000101, B01001011, B00110001, B00000000}, // 3
  {B00000000, B00000000, B00011000, B00010100, B00010010, B01111111, B00010000, B00000000}, // 4
  {B00000000, B00000000, B00100111, B01000101, B01000101, B01000101, B00111001, B00000000}, // 5
  {B00000000, B00000000, B00111100, B01001010, B01001001, B01001001, B00110000, B00000000}, // 6
  {B00000000, B00000000, B00000001, B01110001, B00001001, B00000101, B00000011, B00000000}, // 7
  {B00000000, B00000000, B00110110, B01001001, B01001001, B01001001, B00110110, B00000000}, // 8
  {B00000000, B00000000, B00000110, B01001001, B01001001, B00101001, B00011110, B00000000}  // 9
};

bool isShowColon = false;

void setup() {

  // Initialize the Matrices
  for (int i = 0; i <= numLedMatrix - 1; i++) {
    lc.shutdown(i, false);
    lc.setIntensity(i, 8);
    lc.clearDisplay(i);
  }

  // Second 00 | Minute 59 | Hour 10 | Day of Week Sun = 1, Mon = 2, ... |Day 12 |  Month 07 | Year 2015
  //  myRTC.setDS1302Time(00, 12, 21, 03, 03, 04, 2018);
  Serial.begin(9600);
}

void loop() {
  displayTime();
}

String addZeroes(int value, int lengthDigit) {
  String sValue = (String)value;
  int valueLength = sValue.length();

  for (int i = 1; i <= (lengthDigit - valueLength); i++) {
    sValue = "0" + sValue;
  }

  return sValue;
}

String displayFormattedTime() {
  String dayPeriod = " AM";
  String dayHours = myRTC.hours + "";

  isShowColon = (myRTC.seconds % 2 == 0);
  if (myRTC.hours > 12) {
    dayPeriod = " PM";
    dayHours = myRTC.hours - 12;
  }
  else {
    dayHours = myRTC.hours;
  }

  if (isShowColon) {
    return (" " + dayHours + ":" + addZeroes(myRTC.minutes, 2) + dayPeriod);
  }
  return (" " + dayHours + " " + addZeroes(myRTC.minutes, 2) + dayPeriod);
}

String getDayOfTheWeekName(int iDayOfTheWeek) {
  switch (iDayOfTheWeek) {
    case 1: {
        return "Sunday";
        break;
      }
    case 2: {
        return "Monday";
        break;
      }
    case 3: {
        return "Tuesday";
        break;
      }
    case 4: {
        return "Wednesday";
        break;
      }
    case 5: {
        return "Thursday";
        break;
      }
    case 6: {
        return "Friday";
        break;
      }
    case 7: {
        return "Saturday";
        break;
      }
  }
}

String getMonthName(int iMonth) {
  switch (iMonth) {
    case 1: {
        return "January";
        break;
      }
    case 2: {
        return "February";
        break;
      }
    case 3: {
        return "March";
        break;
      }
    case 4: {
        return "April";
        break;
      }
    case 5: {
        return "May";
        break;
      }
    case 6: {
        return "June";
        break;
      }
    case 7: {
        return "July";
        break;
      }
    case 8: {
        return "August";
        break;
      }
    case 9: {
        return "September";
        break;
      }
    case 10: {
        return "October";
        break;
      }
    case 11: {
        return "November";
        break;
      }
    case 12: {
        return "December";
        break;
      }
  }
}

void displayDigit(int digit, int plce) {
  for (int i = 0; i < 8; i++) {
    lc.setRow(plce, i, num[digit][i]);
  }
}

void displayNumber(int number) {
  String tmpNumber = (String) number;
  int numberLength = tmpNumber.length();

  for (int i = 1; i <= (numLedMatrix - numberLength); i++) {
    tmpNumber = "0" + tmpNumber;
  }

  for (int i = 0; i <= (tmpNumber.length() - 1); i++) {
    displayDigit(tmpNumber.charAt(i) - '0', tmpNumber.length() - (i + 1));
  }
}


void displayTime() {
  myRTC.updateTime();
  String tmpTime = addZeroes(myRTC.hours, 2) + addZeroes(myRTC.minutes, 2);


  for (int i = 0; i <= (tmpTime.length() - 1); i++) {
    displayDigit(tmpTime.charAt(i) - '0', tmpTime.length() - (i + 1));
  }

  lc.setRow(1, 0, B10100);
  delay(1000);
  lc.setRow(1, 0, B00000);
  delay(1000);
  


}


