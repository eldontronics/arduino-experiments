// NOTES
// Facing the knob,
// GND - left
// VCC - right
// Output - center

//#include "FastLED.h"

//#define NUM_LEDS 8
//#define DATA_PIN 13
//#define CLOCK_PIN 13

// Define the array of leds
//CRGB leds[NUM_LEDS];

//int delayTime = 100;

int sensorPin = A0;

// Decalre the LEDs in array
int pinLED[] = { -1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};



void setup() {
  // put your setup code here, to run once:
 // Serial.begin(9600);
  //FastLED.addLeds<WS2812, DATA_PIN, GRB>(leds, NUM_LEDS);
  for (int iPin = 0; iPin < sizeof(pinLED); iPin++) {
    pinMode(pinLED[iPin], OUTPUT);
  }

}

void loop() {
  int sensorValue;
  int ledToLight = 0;
  int delayTime = 0;

  sensorValue = analogRead(sensorPin);
  ledToLight = map(sensorValue, 0, 1023, 0, 10);
  
  for (int pinNum = 0; pinNum <= sizeof(pinLED); pinNum++) {
    if (pinNum <= ledToLight) {
      digitalWrite(pinLED[pinNum], HIGH);
    }
    else {
      digitalWrite(pinLED[pinNum], LOW);
    }
  }

//  Serial.println(delayTime);

//    blinkLedColor(0, CRGB(255, 0 , 0), delayTime);
//    blinkLedColor(1, CRGB(255, 127 , 0), delayTime);
//    blinkLedColor(2, CRGB(255, 255, 0), delayTime);
//    blinkLedColor(3, CRGB(0, 255 , 0), delayTime);
//    blinkLedColor(4, CRGB(0, 0 , 255), delayTime);
//    blinkLedColor(5, CRGB(75 , 0, 130), delayTime);
//    blinkLedColor(6, CRGB(148, 0 , 211), delayTime);
//    blinkLedColor(7, CRGB(255, 255 , 255), delayTime);

}


//void blinkLedColor(int ledInd, CRGB ledColor, int delaySec) {
//  // Turn the LED on, then pause
//  leds[ledInd] = (CRGB) ledColor;
//  FastLED.show();
//  delay(delaySec);
//  // Now turn the LED off, then pause
//  leds[ledInd] = CRGB::Black;
//  FastLED.show();
//  delay(delaySec);
//
//}

