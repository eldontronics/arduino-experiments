//DFRobot.com
//Compatible with the Arduino IDE 1.0
//Library version:1.1
#include <Wire.h>
#include <LiquidCrystal_I2C.h>
#include <Arduino.h>
#include <TM1637Display.h>

#if defined(ARDUINO) && ARDUINO >= 100
#define printByte(args)  write(args);
#else
#define printByte(args)  print(args,BYTE);
#endif

// Module connection pins (Digital Pins)
#define CLK 2
#define DIO 3

// The amount of time (in milliseconds) between tests
#define TEST_DELAY   2000

uint8_t bell[8]  = {0x4,0xe,0xe,0xe,0x1f,0x0,0x4};
uint8_t note[8]  = {0x2,0x3,0x2,0xe,0x1e,0xc,0x0};
uint8_t clock[8] = {0x0,0xe,0x15,0x17,0x11,0xe,0x0};
uint8_t heart[8] = {0x0,0xa,0x1f,0x1f,0xe,0x4,0x0};
uint8_t duck[8]  = {0x0,0xc,0x1d,0xf,0xf,0x6,0x0};
uint8_t check[8] = {0x0,0x1,0x3,0x16,0x1c,0x8,0x0};
uint8_t cross[8] = {0x0,0x1b,0xe,0x4,0xe,0x1b,0x0};
uint8_t retarrow[8] = {  0x1,0x1,0x5,0x9,0x1f,0x8,0x4};

TM1637Display display(CLK, DIO);
LiquidCrystal_I2C lcd(0x3F,16,2);  // set the LCD address to 0x27 for a 16 chars and 2 line display
int ledPin = 11;


void setup()
{

  display.setBrightness(7);
  pinMode (11, OUTPUT);
  pinMode (3, OUTPUT);
  pinMode (8, OUTPUT);
  lcd.init();                      // initialize the lcd 
  lcd.noBacklight();
  delay(1000);
  lcd.backlight();
  
  lcd.createChar(0, bell);
  lcd.createChar(1, note);
  lcd.createChar(2, clock);
  lcd.createChar(3, heart);
  lcd.createChar(4, duck);
  lcd.createChar(5, check);
  lcd.createChar(6, cross);
  lcd.createChar(7, retarrow);
  lcd.home();
  lcd.setCursor(0, 0);
  lcd.print("I will be a");
  lcd.setCursor(0, 1);
  lcd.print("father soon! ");
  lcd.printByte(3);
  lcd.printByte(3);
  lcd.printByte(3);
  //displayKeyCodes();
     digitalWrite(11, HIGH);
     digitalWrite(13, HIGH);
}

// display all keycodes
void displayKeyCodes(void) {
  uint8_t i = 0;
  while (1) {
    lcd.clear();
    lcd.print("Codes 0x"); lcd.print(i, HEX);
    lcd.print("-0x"); lcd.print(i+16, HEX);
    lcd.setCursor(0, 1);
    for (int j=0; j<16; j++) {
      lcd.printByte(i+j);
    }
    i+=16;
    
    delay(4000);
  }
}

void loop()
{   
display.showNumberDec(1208, false, 4, 0); // number to be shown, show leading 0s, start, least significant (to show correctly, use 4,0)
//    for(int value = 0; value <= 9999; value++){
//      display.showNumberDec(value, false, 4, 0); // number to be shown, show leading 0s, start, least significant (to show correctly, use 4,0)
//      delay(10);      
//    }  
}


