#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include "I2Cdev.h"
#include "MPU6050.h"


bool willCalibrateMPU = false;

#define OLED_RESET 4
Adafruit_SSD1306 display(OLED_RESET);

#if (SSD1306_LCDHEIGHT != 64)
#error("Height incorrect, please fix Adafruit_SSD1306.h!");
#endif

// BEGIN: CALIBRATION CODES
// Arduino Wire library is required if I2Cdev I2CDEV_ARDUINO_WIRE implementation
// is used in I2Cdev.h
#if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
#include "Wire.h"
#endif

// class default I2C address is 0x68
// specific I2C addresses may be passed as a parameter here
// AD0 low = 0x68 (default for InvenSense evaluation board)
// AD0 high = 0x69

MPU6050 accelgyro;
//MPU6050 accelgyro(0x69); // <-- use for AD0 high

const char LBRACKET = '[';
const char RBRACKET = ']';
const char COMMA    = ',';
const char BLANK    = ' ';
const char PERIOD   = '.';

const int iAx = 0;
const int iAy = 1;
const int iAz = 2;
const int iGx = 3;
const int iGy = 4;
const int iGz = 5;

const int usDelay = 3150;   // empirical, to hold sampling to 200 Hz
const int NFast =  1000;    // the bigger, the better (but slower)
const int NSlow = 10000;    // ..
const int LinesBetweenHeaders = 5;

int LowValue[6];
int HighValue[6];
int Smoothed[6];
int LowOffset[6];
int HighOffset[6];
int Target[6];
int LinesOut;
int N;

void ForceHeader()
{
  LinesOut = 99;
}

void GetSmoothed()
{ int16_t RawValue[6];
  int i;
  long Sums[6];
  for (i = iAx; i <= iGz; i++)
  {
    Sums[i] = 0;
  }
  //    unsigned long Start = micros();

  for (i = 1; i <= N; i++)
  { // get sums
    accelgyro.getMotion6(&RawValue[iAx], &RawValue[iAy], &RawValue[iAz],
                         &RawValue[iGx], &RawValue[iGy], &RawValue[iGz]);
    if ((i % 500) == 0)
      //      Serial.print(PERIOD);
      delayMicroseconds(usDelay);
    for (int j = iAx; j <= iGz; j++)
      Sums[j] = Sums[j] + RawValue[j];
  } // get sums
  //    unsigned long usForN = micros() - Start;
  //    Serial.print(" reading at ");
  //    Serial.print(1000000/((usForN+N/2)/N));
  //    Serial.println(" Hz");
  for (i = iAx; i <= iGz; i++)
  {
    Smoothed[i] = (Sums[i] + N / 2) / N ;
  }
} // GetSmoothed

void Initialize()
{


  // join I2C bus (I2Cdev library doesn't do this automatically)
  //#if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
  //  Wire.begin();
  //#elif I2CDEV_IMPLEMENTATION == I2CDEV_BUILTIN_FASTWIRE
  //  Fastwire::setup(400, true);
  //#endif

  // initialize device
  Serial.println("Initializing I2C devices...");
  accelgyro.initialize();

  // verify connection
  Serial.println("Testing device connections...");
  Serial.println(accelgyro.testConnection() ? "MPU6050 connection successful" : "MPU6050 connection failed");

  if (accelgyro.testConnection()) {
    display.clearDisplay();
    display.display();
    display.setCursor(0, 0);
    display.setTextSize(1);
    display.setTextColor(WHITE);
    display.println("MPU6050:Connected...");
    display.display();
  }
  else {
    display.clearDisplay();
    display.display();
    display.setCursor(0, 0);
    display.setTextSize(1);
    display.setTextColor(WHITE);
    display.println("MPU6050: Not connected.");
    display.display();
    while (1) {}
  }



} // Initialize

void SetOffsets(int TheOffsets[6])
{ accelgyro.setXAccelOffset(TheOffsets [iAx]);
  accelgyro.setYAccelOffset(TheOffsets [iAy]);
  accelgyro.setZAccelOffset(TheOffsets [iAz]);
  accelgyro.setXGyroOffset (TheOffsets [iGx]);
  accelgyro.setYGyroOffset (TheOffsets [iGy]);
  accelgyro.setZGyroOffset (TheOffsets [iGz]);
} // SetOffsets

void ShowProgress()
{ if (LinesOut >= LinesBetweenHeaders)
  { // show header
    //  Serial.println("\tXAccel\t\t\tYAccel\t\t\t\tZAccel\t\t\tXGyro\t\t\tYGyro\t\t\tZGyro");
    LinesOut = 0;
  } // show header
  //Serial.print(BLANK);
  for (int i = iAx; i <= iGz; i++)
  {
    //    Serial.print(LBRACKET);
    //    Serial.print(LowOffset[i]),
    //                 Serial.print(COMMA);
    //    Serial.print(HighOffset[i]);
    //    Serial.print("] --> [");
    //    Serial.print(LowValue[i]);
    //    Serial.print(COMMA);
    //    Serial.print(HighValue[i]);
    if (i == iGz)
    {
      //      Serial.println(RBRACKET);
    }
    else
    {
      //      Serial.print("]\t");
    }
  }
  LinesOut++;
} // ShowProgress

void PullBracketsIn()
{ boolean AllBracketsNarrow;
  boolean StillWorking;
  int NewOffset[6];

  //Serial.println("\nclosing in:");
  AllBracketsNarrow = false;
  ForceHeader();
  StillWorking = true;
  while (StillWorking)
  { StillWorking = false;
    if (AllBracketsNarrow && (N == NFast))
    {
      SetAveraging(NSlow);
    }
    else
    {
      AllBracketsNarrow = true;  // tentative
    }
    for (int i = iAx; i <= iGz; i++)
    { if (HighOffset[i] <= (LowOffset[i] + 1))
      {
        NewOffset[i] = LowOffset[i];
      }
      else
      { // binary search
        StillWorking = true;
        NewOffset[i] = (LowOffset[i] + HighOffset[i]) / 2;
        if (HighOffset[i] > (LowOffset[i] + 10))
        {
          AllBracketsNarrow = false;
        }
      } // binary search
    }
    SetOffsets(NewOffset);
    GetSmoothed();
    for (int i = iAx; i <= iGz; i++)
    { // closing in
      if (Smoothed[i] > Target[i])
      { // use lower half
        HighOffset[i] = NewOffset[i];
        HighValue[i] = Smoothed[i];
      } // use lower half
      else
      { // use upper half
        LowOffset[i] = NewOffset[i];
        LowValue[i] = Smoothed[i];
      } // use upper half
    } // closing in
    ShowProgress();
  } // still working

} // PullBracketsIn

void PullBracketsOut()
{ boolean Done = false;
  int NextLowOffset[6];
  int NextHighOffset[6];

  //Serial.println("expanding:");
  ForceHeader();

  while (!Done)
  { Done = true;
    SetOffsets(LowOffset);
    GetSmoothed();
    for (int i = iAx; i <= iGz; i++)
    { // got low values
      LowValue[i] = Smoothed[i];
      if (LowValue[i] >= Target[i])
      { Done = false;
        NextLowOffset[i] = LowOffset[i] - 1000;
      }
      else
      {
        NextLowOffset[i] = LowOffset[i];
      }
    } // got low values

    SetOffsets(HighOffset);
    GetSmoothed();
    for (int i = iAx; i <= iGz; i++)
    { // got high values
      HighValue[i] = Smoothed[i];
      if (HighValue[i] <= Target[i])
      { Done = false;
        NextHighOffset[i] = HighOffset[i] + 1000;
      }
      else
      {
        NextHighOffset[i] = HighOffset[i];
      }
    } // got high values
    ShowProgress();
    for (int i = iAx; i <= iGz; i++)
    { LowOffset[i] = NextLowOffset[i];   // had to wait until ShowProgress done
      HighOffset[i] = NextHighOffset[i]; // ..
    }
  } // keep going
} // PullBracketsOut

void SetAveraging(int NewN)
{ N = NewN;
  //Serial.print("averaging ");
  //Serial.print(N);
  //Serial.println(" readings each time");
} // SetAveraging

// END: CALIBRATION CODES

const int MPU = 0x68; //I2C address of MPU

int GyX, GyY, GyZ;

float pitch = 0; // elevation value
float roll = 0; // rolling left, rolling right values
float yaw = 0; // turning left,, turnig right values

float v_pitch;
float v_roll;
float v_yaw;
float a_pitch;
float a_roll;
float a_yaw;

void setup()
{
  Serial.begin(9600);

  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);  // initialize with the I2C addr 0x3D (for the 128x64)
  display.clearDisplay();


  if (willCalibrateMPU) {
    display.setCursor(0, 0);
    display.setTextSize(1);
    display.setTextColor(WHITE);
    display.println("Calibration: Initialized.");
    display.display();


    Initialize();

    display.setCursor(0, 10);
    display.setTextSize(1);
    display.setTextColor(WHITE);
    display.println("Offsets:Calculating..");
    display.display();

    for (int i = iAx; i <= iGz; i++)
    { // set targets and initial guesses
      Target[i] = 0; // must fix for ZAccel
      HighOffset[i] = 0;
      LowOffset[i] = 0;
    } // set targets and initial guesses
    Target[iAz] = 16384;
    SetAveraging(NFast);

    PullBracketsOut();
    PullBracketsIn();

    display.setCursor(0, 20);
    display.setTextSize(1);
    display.setTextColor(WHITE);
    display.println("Calibration: Done!");
    display.display();

    Serial.println("-------------- done --------------");
    delay(5000);
  }

  Wire.begin();
  Wire.beginTransmission(MPU);
  Wire.write(0x6B); //power management register 1
  Wire.write(0);
  Wire.endTransmission(true);


}

void loop() {

  if (!willCalibrateMPU) {
    Wire.beginTransmission(MPU);
    Wire.write(0x43); //starts with MPU register 43(GYRO_XOUT_H)
    Wire.endTransmission(false);
    Wire.requestFrom(MPU, 6, true); //requests 6 registers

    GyX = Wire.read() << 8 | Wire.read();
    GyY = Wire.read() << 8 | Wire.read();
    GyZ = Wire.read() << 8 | Wire.read();

    v_pitch = (GyX / 131);

    if (v_pitch == -1) {
      v_pitch = 0;
    }

    v_roll = (GyY / 131);

    if (v_roll == 1) {
      v_roll = 0;
    }

    v_yaw = GyZ / 131;

    a_pitch = (v_pitch * 0.046);
    a_roll = (v_roll * 0.046);
    a_yaw = (v_yaw * 0.045);

    pitch = pitch + a_pitch;
    roll = roll + a_roll;
    yaw = yaw + a_yaw;

    display.clearDisplay();

    bool    isMoveLeft      = false;
    bool    isMoveRight     = false;
    bool    isMoveForward   = false;
    bool    isMoveBackward  = false;
    String  movementLR      = "";
    String  movementFB      = "";
    String  movement        = "";

    // LEFT or RIGHT
    if ( yaw < 0 ) {
      movementLR = "RIGHT";
    }
    else {
      movementLR = "LEFT";
    }

    // FORWARD or BACKWORD
    if ( roll < 0 ) {
      movementFB = "FORWARD";
    }
    else {
      movementFB = "BACKWARD";
    }

    if (abs(yaw) > abs(roll)) {
      movement = movementLR;
    }
    else {
      movement = movementFB;
    }




    Serial.print(" | pitch = ");
    Serial.print(pitch);
    display.setCursor(0, 0);
    display.setTextSize(1);
    display.setTextColor(WHITE);
    display.print("Pitch  : ");
    display.println(pitch);

    Serial.print(" | roll = ");
    Serial.print(roll);
    display.setCursor(0, 10);
    display.setTextSize(1);
    display.setTextColor(WHITE);
    display.print("F/B  :");
    display.println(roll);

    Serial.print(" | yaw = ");
    Serial.println(yaw);
    display.setCursor(0, 20);
    display.setTextSize(1);
    display.setTextColor(WHITE);
    display.print("L/R  :");
    display.println(yaw);

    display.setCursor(0, 40);
    display.setTextSize(1);
    display.setTextColor(WHITE);
    display.print("MOVEMENT  : ");
    display.println(movement);


    display.display();
  }
}
