/*
      8x8 LED Matrix MAX7219 Example 01
   by Dejan Nedelkovski, www.HowToMechatronics.com
   Based on the following library:
   GitHub | riyas-org/max7219  https://github.com/riyas-org/max7219
*/

#include <MaxMatrix.h>

int DIN = 7;   // DIN pin of MAX7219 module
int CLK = 6;   // CLK pin of MAX7219 module
int CS  = 5;    // CS pin of MAX7219 module
int maxInUse = 1;

MaxMatrix m(DIN, CS, CLK, maxInUse);

char smile01[] = {8, 8,
                  B11000011,
                  B01100110,
                  B00111100,
                  B10011001,
                  B11000011,
                  B01100110,
                  B00111100,
                  B00011000
                 };

char smile02[] = {8, 8,
                  B00011000,
                  B00111100,
                  B01100110,
                  B11000011,
                  B10011001,
                  B00111100,
                  B01100110,
                  B11000011
                 };
void setup() {
  m.init(); // MAX7219 initialization
  m.setIntensity(8); // initial led matrix intensity, 0-15

}

void loop() {

//  m.writeSprite(0, 0, smile01);
//  delay(10);
//
//  for (int i = 0; i < 8; i++) {
//    m.shiftRight(true, true);
//    delay(100);
//  }
//  m.clear();

  m.writeSprite(0, 0, smile02);
  delay(10);

  for (int i = 0; i < 8; i++) {
    m.shiftLeft(true, true);
    delay(100);
  }
  m.clear();
}
