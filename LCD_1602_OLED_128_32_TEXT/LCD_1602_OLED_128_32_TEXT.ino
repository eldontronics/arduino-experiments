/*********************************************************************
  This is an example for our Monochrome OLEDs based on SSD1306 drivers

  Pick one up today in the adafruit shop!
  ------> http://www.adafruit.com/category/63_98

  This example is for a 128x32 size display using I2C to communicate
  3 pins are required to interface (2 I2C and one reset)

  Adafruit invests time and resources providing this open source code,
  please support Adafruit and open-source hardware by purchasing
  products from Adafruit!

  Written by Limor Fried/Ladyada  for Adafruit Industries.
  BSD license, check license.txt for more information
  All text above, and the splash screen must be included in any redistribution
*********************************************************************/

#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <LiquidCrystal_I2C.h>

#define OLED_RESET 4
Adafruit_SSD1306 display(OLED_RESET);

#if (SSD1306_LCDHEIGHT != 32)
#error("Height incorrect, please fix Adafruit_SSD1306.h!");
#endif

LiquidCrystal_I2C lcd(0x3B, 16, 2);

void setup()   {
  Serial.begin(9600);

  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);
  display.clearDisplay();
  display.display();

  lcd.init();
  lcd.noBacklight();
  delay(100);
  lcd.backlight();
}


void loop() {

  lcd.clear();
  display.clearDisplay();
  display.setTextSize(1);
  display.setTextColor(WHITE);

  display.setCursor(0, 0);
  display.print("A0 : ");
  display.print(analogRead(A0));

  lcd.setCursor(0, 0);
  lcd.print("A0:");
  lcd.print(analogRead(A0));

  display.setCursor(0, 8);
  display.print("A1 : ");
  display.print(analogRead(A1));

  lcd.setCursor(9, 0);
  lcd.print("A1:");
  lcd.print(analogRead(A1));

  display.setCursor(0, 16);
  display.print("A2 : ");
  display.print(analogRead(A2));


  lcd.setCursor(0, 1);
  lcd.print("A2:");
  lcd.print(analogRead(A2));

  display.setCursor(0, 24);
  display.print("A3 : ");
  display.print(analogRead(A3));

  lcd.setCursor(9, 1);
  lcd.print("A3:");
  lcd.print(analogRead(A3));

  display.setCursor(60, 0);
  display.print("A4 : ");
  display.print(analogRead(A4));

  display.setCursor(60, 8);
  display.print("A5 : ");
  display.print(analogRead(A5));

  display.setTextSize(1);
  display.setCursor(60, 16);
  display.print("ARDUINO");
  display.setCursor(60, 24);
  display.print(" NANO");



  display.display();

  delay(100);
}



