/*
  Created by Rui Santos

  All the resources for this project:
  http://randomnerdtutorials.com/
*/

#include "LedControl.h"
// #include "binary.h"

/*
  DIN connects to pin 12
  CLK connects to pin 11
  CS connects to pin 10
*/

int pinDIN = 12;
int pinCLK = 11;
int pinCS  = 10;
int numLedMatrix = 2;
int matrixType = 1;

LedControl lc = LedControl(pinDIN, pinCLK, pinCS, numLedMatrix);

// delay time between faces
unsigned long delaytime = 250;


byte num[10][8] {
  {B00000000, B00000000, B00111110, B01010001, B01001001, B01000101, B00111110, B00000000}, // 0
  {B00000000, B00000000, B01000010, B01000010, B01111111, B01000000, B01000000, B00000000}, // 1
  {B00000000, B00000000, B01000010, B01100001, B01010001, B01001001, B01000110, B00000000}, // 2
  {B00000000, B00000000, B00100001, B01000001, B01000101, B01001011, B00110001, B00000000}, // 3
  {B00000000, B00000000, B00011000, B00010100, B00010010, B01111111, B00010000, B00000000}, // 4
  {B00000000, B00000000, B00100111, B01000101, B01000101, B01000101, B00111001, B00000000}, // 5
  {B00000000, B00000000, B00111100, B01001010, B01001001, B01001001, B00110000, B00000000}, // 6
  {B00000000, B00000000, B00000001, B01110001, B00001001, B00000101, B00000011, B00000000}, // 7
  {B00000000, B00000000, B00110110, B01001001, B01001001, B01001001, B00110110, B00000000}, // 8
  {B00000000, B00000000, B00000110, B01001001, B01001001, B00101001, B00011110, B00000000}  // 9
};

void setup() {

  Serial.begin(9600);

  // Initialize all Martices
  for (int i = 0; i <= numLedMatrix - 1; i++) {
    lc.shutdown(i, false);
    lc.setIntensity(i, 8);
    lc.clearDisplay(i);

  }

}

void displayDigit(int digit, int plce) {

  if (matrixType == 0) {// green board

    for (int i = 0; i < 8; i++) {
      lc.setRow(plce, i, num[digit][i]);
    }
  }
  else { // blue board
    for (int i = 7; i >= 0; i--) {
      lc.setRow(plce, i, reverse(num[digit][7 - i]));
    }
  }
}

void displayNumber(int number) {
  String tmpNumber = (String) number;
  int numberLength = tmpNumber.length();

  for (int i = 1; i <= (numLedMatrix - numberLength); i++) {
    tmpNumber = "0" + tmpNumber;
  }

  for (int i = 0; i <= (tmpNumber.length() - 1); i++) {
    displayDigit(tmpNumber.charAt(i) - '0', tmpNumber.length() - (i + 1));
  }


}

void loop() {
  for (int number = 0; number <= 99; number++) {
    displayNumber(number);
    delay(delaytime);
  }

}

void testBits(int plce, byte _bitsToShow) {
  lc.setRow(0, plce, reverse(_bitsToShow));
}

byte reverse(byte in) {
  byte out = 0;
  for (int i = 0; i < 8; i++)
    out |= ((in >> i) & 1) << (7 - i);
  return out;
}

