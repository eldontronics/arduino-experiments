#include "FastLED.h"

#define NUM_LEDS 24

#define DATA_PIN A1

CRGB leds[NUM_LEDS];

int delayInterval = 100;

void setup() {
  FastLED.addLeds<WS2812, DATA_PIN, GRB>(leds, NUM_LEDS);
  FastLED.setBrightness(100);
}

void loop() {

  for (int lightVar = 0; lightVar <= 255; lightVar+= 5) {
    for (int ledInd = 0; ledInd < NUM_LEDS; ledInd++) {
      leds[ledInd] = CRGB(0, lightVar, lightVar);
      FastLED.show();
      //      delay(delayInterval / 100);
    }
  }

  delay(5000);

  for (int lightVar = 255; lightVar >= 0; lightVar-=5) {
    for (int ledInd = 0; ledInd < NUM_LEDS; ledInd++) {
      leds[ledInd] = CRGB(0, lightVar, lightVar);
      FastLED.show();
      //    delay(delayInterval / 100);
    }
  }

  
}
