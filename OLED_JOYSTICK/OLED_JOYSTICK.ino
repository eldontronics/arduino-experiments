#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

int xAxis = A0;
int yAxis = A1;
int xValue = 0;
int yValue = 0;
int pressedPin = 2;
int notPressed = 0;

#define OLED_RESET 4
Adafruit_SSD1306 display(OLED_RESET);

#if (SSD1306_LCDHEIGHT != 64)
#error("Height incorrect, please fix Adafruit_SSD1306.h!");
#endif

void setup()   {                
  Serial.begin(9600);
  pinMode(xAxis, INPUT);
  pinMode(yAxis, INPUT);
  pinMode(pressedPin, INPUT_PULLUP);
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);  // initialize with the I2C addr 0x3D (for the 128x64)
  display.clearDisplay();
}


void loop() {
  xValue = analogRead(xAxis);
  yValue = analogRead(yAxis);
  notPressed = digitalRead(pressedPin);
  display.clearDisplay();
  display.setCursor(0,0);
  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.drawPixel(map(xValue, 0, 1023, 0, 127), map(yValue, 0, 1023, 0, 63), WHITE);
  display.display();  
  delay(1000);  
}

