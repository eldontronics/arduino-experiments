// DISTANCE SENSOR
int trigPin = 13;
int echoPin = 12;
long soundSpeed = 343; // meters per second


// WHEEL 1
int motor1Pin1 = 5;
int motor1Pin2 = 6;

// WHEEL 2
int motor2Pin1 = 9;
int motor2Pin2 = 10;


void setup() {
  // Distance Sensor
  Serial.begin (9600);
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);

  // Motors
  pinMode(motor1Pin1, OUTPUT);
  pinMode(motor1Pin2, OUTPUT);
  pinMode(motor2Pin1, OUTPUT);
  pinMode(motor2Pin2, OUTPUT);

}

void loop() {


  float duration, distance;
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW); 
  duration = pulseIn(echoPin, HIGH); // value is in microseconds
  duration = duration / 1000000; // convert microseconds to seconds
  distance = (duration/2) * soundSpeed;
  distance = distance * 100;
  //delay(100);

  if (distance <=7){
    // Backward
  //  Serial.print("Distance : ");
   // Serial.print(distance);
   // Serial.print(" cm. ");
   // Serial.println("Direction: backwards...");    
    digitalWrite(motor1Pin1, LOW); // FORWARD
    digitalWrite(motor1Pin2, HIGH); // BACKWARD
    digitalWrite(motor2Pin1, LOW); // FORWARD
    digitalWrite(motor2Pin2, HIGH); // BACKWARD    
    delay(500);

    // turn right
    digitalWrite(motor1Pin1, HIGH); // FORWARD
    digitalWrite(motor1Pin2, LOW); // BACKWARD
    digitalWrite(motor2Pin1, LOW); // FORWARD
    digitalWrite(motor2Pin2, LOW); // BACKWARD    
    delay(1000);
    
  }
  else{
    // Forward
    //Serial.print("Distance : ");
    //Serial.print(distance);
    //Serial.print(" cm. ");
    //Serial.println("Direction: forwards...");    
    digitalWrite(motor1Pin1, HIGH); // FORWARD
    digitalWrite(motor1Pin2, LOW); // BACKWARD
    digitalWrite(motor2Pin1, HIGH); // FORWARD
    digitalWrite(motor2Pin2, LOW); // BACKWARD    
    delay(10);
  }
// Serial.println("");
// Serial.println("");
 
}


////set the L293D’s three control pins to 9, 10, and 11 on the Arduino
//int enablePin = 11;
//int in1Pin = 10;
//int in2Pin = 9;
//
//void setup()
//{
// pinMode(enablePin, OUTPUT);
// pinMode(in1Pin, OUTPUT);
// pinMode(in2Pin, OUTPUT);
//}
//
//void loop()
//
//{
// analogWrite(enablePin, 255); // this sets the speed for the motor at 50
// digitalWrite(in1Pin, HIGH);
// digitalWrite(in2Pin, LOW);
// 
// // delay(2000); // delays for 2 seconds
////
//// analogWrite(enablePin, 255);
//// digitalWrite(in1Pin, LOW);
//// digitalWrite(in2Pin, HIGH);
//// delay(2000); // delays for 2 seconds
//}










































