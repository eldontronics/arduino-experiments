
// http://www.martyncurrey.com/hm-10-bluetooth-4ble-modules/#HM-10%20-%20AT%20commands

#include <SoftwareSerial.h>
#include <Servo.h>

Servo myservo;  // create servo object to control a servo
SoftwareSerial mySerial(7, 8); // RX, TX
// Connect HM10      Arduino Uno
//     Pin 1/TXD          Pin 7
//     Pin 2/RXD          Pin 8

void setup() {

  Serial.begin(9600);

  myservo.attach(9);  // attaches the servo on pin 9 to the servo object
  myservo.write(0);

  // If the baudrate of the HM-10 module has been updated,
  // you may need to change 9600 by another value
  // Once you have found the correct baudrate,
  // you can update it using AT+BAUDx command
  // e.g. AT+BAUD0 for 9600 bauds
  mySerial.begin(9600);
  // Test if module responds OK
  Serial.println("AT");
  mySerial.write("AT\r\n");

  // Resets the module
  Serial.println("AT+RESET");
  mySerial.write("AT+RESET\r\n");

  // Sets the BAUD rate to 9600
  Serial.println("AT+BAUD0");
  mySerial.write("AT+BAUD0\r\n");

  // Sets the module name to EldricMarcus
  Serial.println("AT+NAMEEldricMarcus");
  mySerial.write("AT+NAMEEldricMarcus\r\n");

  // Sets the module to SLAVE/PERIPHERAL
  Serial.println("AT+ROLE0");
  mySerial.write("AT+ROLE0\r\n");
}

void loop() {
  char c;
  if (Serial.available()) {
    c = Serial.read();
    mySerial.print(c);
  }
  
  if (mySerial.available()) {
    c = mySerial.read();
    Serial.print(c);

    if(c == '0'){
      turnOffSwitch();
    }
    else if(c == '1'){
      turnOnSwitch();
    }
    
  }
}

void turnOnSwitch() {
  for (int i = 0; i <= 180; i++) {
    myservo.write(i);
    delay(1 );
  }
}

void turnOffSwitch() {
  for (int i = 180; i >= 0; i--) {
    myservo.write(i);
    delay(1);
  }
}
