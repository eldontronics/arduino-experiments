// Serial communication thru Python
// and Arduino
// By Eldon B. Tenorio

#define redLED    9
#define yellowLED 8
#define blueLED   7
#define greenLED  6
 
String serialValue = "";

void setup()   {                
  Serial.begin(9600);
  pinMode(redLED,     OUTPUT);
  pinMode(yellowLED,  OUTPUT);
  pinMode(blueLED,    OUTPUT);
  pinMode(greenLED,   OUTPUT);
  
}

void loop() {
  // While serial is receiving characters
  while(Serial.available() > 0){
      // Append all received characters
      serialValue = serialValue + (char)Serial.read();
      delay(10);
  }

  // When there is data received
  if(serialValue != ""){
    if(serialValue == "clear") serialValue = "";

      if(serialValue == "red"){        
        digitalWrite(redLED,    HIGH);
        digitalWrite(yellowLED, LOW);
        digitalWrite(blueLED,   LOW);
        digitalWrite(greenLED,  LOW);
      }
      else if(serialValue == "yellow"){        
        digitalWrite(redLED,    LOW);
        digitalWrite(yellowLED, HIGH);
        digitalWrite(blueLED,   LOW);
        digitalWrite(greenLED,  LOW);
      } 
      else if(serialValue == "blue"){        
        digitalWrite(redLED,    LOW);
        digitalWrite(yellowLED, LOW);
        digitalWrite(blueLED,   HIGH);
        digitalWrite(greenLED,  LOW);
      }
      else if(serialValue == "green"){        
        digitalWrite(redLED,    LOW);
        digitalWrite(yellowLED, LOW);
        digitalWrite(blueLED,   LOW);
        digitalWrite(greenLED,  HIGH);
      }
      else if(serialValue == "all"){        
        digitalWrite(redLED,    HIGH);
        digitalWrite(yellowLED, HIGH);
        digitalWrite(blueLED,   HIGH);
        digitalWrite(greenLED,  HIGH);
      }
      else if(serialValue == "none"){        
        digitalWrite(redLED,    LOW);
        digitalWrite(yellowLED, LOW);
        digitalWrite(blueLED,   LOW);
        digitalWrite(greenLED,  LOW);
      }
    
    // Reset serial value
    serialValue ="";    
  }
}
