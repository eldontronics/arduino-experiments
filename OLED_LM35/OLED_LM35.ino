#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

float temp = 0.0;
int tempPin = A0;

#define OLED_RESET 4
Adafruit_SSD1306 display(OLED_RESET);

#if (SSD1306_LCDHEIGHT != 64)
#error("Height incorrect, please fix Adafruit_SSD1306.h!");
#endif

void setup()   {                
  Serial.begin(9600);
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);  // initialize with the I2C addr 0x3D (for the 128x64) 
  display.clearDisplay();
}


void loop() {
  display.clearDisplay();
  display.setCursor(0,0);
  display.setTextSize(1);
  display.setTextColor(WHITE);
  temp = analogRead(tempPin);
  temp = temp * 0.48828125;
  display.println("Temperature: ");
  display.setTextSize(2);
  display.print(temp);
  display.print("");
  display.write(248);
  display.println("C");
  display.setCursor(0,30);
  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.println("Date/Time: ");
  display.setTextSize(1);
  display.print("May 10, 2017 3:35PM");
  
  display.display();  
  delay(1000);  
}
