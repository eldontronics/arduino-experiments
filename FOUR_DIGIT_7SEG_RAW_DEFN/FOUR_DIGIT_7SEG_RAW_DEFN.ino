// COMMON ANODE 4-DIGIT 7-SEGMENT DISPLAY Arduino Code
// Written by : Eldon B. Tenorio

// Please to schematics of the 4-digit 7-segment display to determine the segment pins.

// Top pins:      D1  A   F   D2  D3  B
// Botton pins:   E   D   .   C   G   D4
//                ----------------------
// Top pins:      12  11  10  9   8   7
// Botton pins:   1   2   3   4   5   6


// Segment A            = Arduino Digital Pin 6 (Pin 11 of 4-digit 7-segment display)
// Segment B            = Arduino Digital Pin 7 (Pin  7 of 4-digit 7-segment display)
// Segment C            = Arduino Digital Pin 8 (Pin  4 of 4-digit 7-segment display)
// Segment D            = Arduino Digital Pin 9 (Pin  2 of 4-digit 7-segment display)
// Segment E            = Arduino Digital Pin 10 (Pin  1 of 4-digit 7-segment display)
// Segment F            = Arduino Digital Pin 11 (Pin 10 of 4-digit 7-segment display)
// Segment G            = Arduino Digital Pin 12 (Pin  5 of 4-digit 7-segment display)
// Decimal Point        = Arduino Digital Pin 13 (Pin  3 of 4-digit 7-segment display)
// Digit 1 (Thousands) = Arduino Digital Pin 2 (via 220-Ohm Resistor), Pin 12 of 4-digit 7-segment display
// Digit 2 (Hundreds)   = Arduino Digital Pin 3 (via 220-Ohm Resistor), Pin  9 of 4-digit 7-segment display
// Digit 3 (Tens)       = Arduino Digital Pin 4 (via 220-Ohm Resistor), Pin  8 of 4-digit 7-segment display
// Digit 4 (Ones)       = Arduino Digital Pin 5 (via 220-Ohm Resistor), Pin  6 of 4-digit 7-segment display

// Uncomment the line below if a common anode is used.
#define USECATHODE

#ifdef  USECATHODE
#define TURNONSEGMENT   1
#define TURNOFFSEGMENT  0
#else
#define TURNONSEGMENT   0
#define TURNOFFSEGMENT  1
#endif

int pinSegment[8] =   {6, 7, 8, 9, 10, 11, 12};
int dPoint        =   13;
int pinDigit[4]   =   {2, 3, 4, 5};
int segDelay      =   5;
int displayDelay  =   1000;

void setup() {
  Serial.begin(9600);

  pinMode(dPoint, OUTPUT);
  digitalWrite(dPoint, LOW);
  for (int i = 0; i < 8; i++) {
    pinMode(pinSegment[i], OUTPUT);
  }

  for (int i = 0; i < 4; i++) {
    pinMode(pinDigit[i], OUTPUT);
  }
}

void loop() {
  for (int num = 0; num <= 9999; num++) {
    displayNumber(num, 100, true);
  }
}

// Function     = displayNumber(int number, int delayTime, bool preceedZero)
// number       = the number to be displayed.
// delayTime    = the duration the number will display
// preceedZero  = zeroes will be prefixed once this is true.

void displayNumber(int number, int delayTime, bool preceedZero) {
  String strNumber = (String) number;
  char charNumber[4];
  int numZero = (4 - strNumber.length());
  char prefix = ' ';

  if (strNumber.length() < 4) {
    if (preceedZero) {
      prefix = '0';
    }
    for (int it = 1; it <= numZero; it++)
    {
      strNumber = prefix + strNumber;
    }
  }

  strNumber.toCharArray(charNumber, 5);

  for (int j = 1; j <= delayTime / 20; j++) {
    showDigit(charNumber[0], 0);
    showDigit(charNumber[1], 1);
    showDigit(charNumber[2], 2);
    showDigit(charNumber[3], 3);
  }
}

// Function     = showDigit(char digit, int placeholder)
// digit        = the digit to be displayed
// placeholder  = determines the position of the digit to be displayed.

void showDigit(char digit, int placeholder) {

  // 0 = ones
  // 1 = tens
  // 2 = hundreds
  // 3 = thousands

  lightDigit (placeholder);

  if (digit == '1') {
    digitalWrite(pinSegment[0], TURNOFFSEGMENT);
    digitalWrite(pinSegment[1], TURNONSEGMENT);
    digitalWrite(pinSegment[2], TURNONSEGMENT);
    digitalWrite(pinSegment[3], TURNOFFSEGMENT);
    digitalWrite(pinSegment[4], TURNOFFSEGMENT);
    digitalWrite(pinSegment[5], TURNOFFSEGMENT);
    digitalWrite(pinSegment[6], TURNOFFSEGMENT);
    delay(segDelay);
  } // 1

  if (digit == '2') {
    digitalWrite(pinSegment[0], TURNONSEGMENT);
    digitalWrite(pinSegment[1], TURNONSEGMENT);
    digitalWrite(pinSegment[2], TURNOFFSEGMENT);
    digitalWrite(pinSegment[3], TURNONSEGMENT);
    digitalWrite(pinSegment[4], TURNONSEGMENT);
    digitalWrite(pinSegment[5], TURNOFFSEGMENT);
    digitalWrite(pinSegment[6], TURNONSEGMENT);
    delay(segDelay);
  } // 2

  if (digit == '3') {
    digitalWrite(pinSegment[0], TURNONSEGMENT);
    digitalWrite(pinSegment[1], TURNONSEGMENT);
    digitalWrite(pinSegment[2], TURNONSEGMENT);
    digitalWrite(pinSegment[3], TURNONSEGMENT);
    digitalWrite(pinSegment[4], TURNOFFSEGMENT);
    digitalWrite(pinSegment[5], TURNOFFSEGMENT);
    digitalWrite(pinSegment[6], TURNONSEGMENT);
    delay(segDelay);
  } // 3

  if (digit == '4') {
    digitalWrite(pinSegment[0], TURNOFFSEGMENT);
    digitalWrite(pinSegment[1], TURNONSEGMENT);
    digitalWrite(pinSegment[2], TURNONSEGMENT);
    digitalWrite(pinSegment[3], TURNOFFSEGMENT);
    digitalWrite(pinSegment[4], TURNOFFSEGMENT);
    digitalWrite(pinSegment[5], TURNONSEGMENT);
    digitalWrite(pinSegment[6], TURNONSEGMENT);
    delay(segDelay);
  } // 4

  if (digit == '5') {
    digitalWrite(pinSegment[0], TURNONSEGMENT);
    digitalWrite(pinSegment[1], TURNOFFSEGMENT);
    digitalWrite(pinSegment[2], TURNONSEGMENT);
    digitalWrite(pinSegment[3], TURNONSEGMENT);
    digitalWrite(pinSegment[4], TURNOFFSEGMENT);
    digitalWrite(pinSegment[5], TURNONSEGMENT);
    digitalWrite(pinSegment[6], TURNONSEGMENT);
    delay(segDelay);
  } // 5

  if (digit == '6') {
    digitalWrite(pinSegment[0], TURNONSEGMENT);
    digitalWrite(pinSegment[1], TURNOFFSEGMENT);
    digitalWrite(pinSegment[2], TURNONSEGMENT);
    digitalWrite(pinSegment[3], TURNONSEGMENT);
    digitalWrite(pinSegment[4], TURNONSEGMENT);
    digitalWrite(pinSegment[5], TURNONSEGMENT);
    digitalWrite(pinSegment[6], TURNONSEGMENT);
    delay(segDelay);
  } // 6

  if (digit == '7') {
    digitalWrite(pinSegment[0], TURNONSEGMENT);
    digitalWrite(pinSegment[1], TURNONSEGMENT);
    digitalWrite(pinSegment[2], TURNONSEGMENT);
    digitalWrite(pinSegment[3], TURNOFFSEGMENT);
    digitalWrite(pinSegment[4], TURNOFFSEGMENT);
    digitalWrite(pinSegment[5], TURNOFFSEGMENT);
    digitalWrite(pinSegment[6], TURNOFFSEGMENT);
    delay(segDelay);
  } // 7


  if (digit == '8') {
    digitalWrite(pinSegment[0], TURNONSEGMENT);
    digitalWrite(pinSegment[1], TURNONSEGMENT);
    digitalWrite(pinSegment[2], TURNONSEGMENT);
    digitalWrite(pinSegment[3], TURNONSEGMENT);
    digitalWrite(pinSegment[4], TURNONSEGMENT);
    digitalWrite(pinSegment[5], TURNONSEGMENT);
    digitalWrite(pinSegment[6], TURNONSEGMENT);
    delay(segDelay);
  } // 8

  if (digit == '9') {
    digitalWrite(pinSegment[0], TURNONSEGMENT);
    digitalWrite(pinSegment[1], TURNONSEGMENT);
    digitalWrite(pinSegment[2], TURNONSEGMENT);
    digitalWrite(pinSegment[3], TURNONSEGMENT);
    digitalWrite(pinSegment[4], TURNOFFSEGMENT);
    digitalWrite(pinSegment[5], TURNONSEGMENT);
    digitalWrite(pinSegment[6], TURNONSEGMENT);
    delay(segDelay);
  } // 9

  if (digit == '0') {
    digitalWrite(pinSegment[0], TURNONSEGMENT);
    digitalWrite(pinSegment[1], TURNONSEGMENT);
    digitalWrite(pinSegment[2], TURNONSEGMENT);
    digitalWrite(pinSegment[3], TURNONSEGMENT);
    digitalWrite(pinSegment[4], TURNONSEGMENT);
    digitalWrite(pinSegment[5], TURNONSEGMENT);
    digitalWrite(pinSegment[6], TURNOFFSEGMENT);
    delay(segDelay);
  } // 0

  if (digit == ' ') {
    digitalWrite(pinSegment[0], TURNOFFSEGMENT);
    digitalWrite(pinSegment[1], TURNOFFSEGMENT);
    digitalWrite(pinSegment[2], TURNOFFSEGMENT);
    digitalWrite(pinSegment[3], TURNOFFSEGMENT);
    digitalWrite(pinSegment[4], TURNOFFSEGMENT);
    digitalWrite(pinSegment[5], TURNOFFSEGMENT);
    digitalWrite(pinSegment[6], TURNOFFSEGMENT);
    delay(segDelay);
  } // space

}

// function     = lightDigit(int placeholder)
// placeholder  = turns the digits place ON.
// placeholder values : 0 = Thousands place, 1 = Hundreds place, 2 = Tens place, 3 = Ones place.

void lightDigit(int placeholder) {

  if (placeholder == 0) {
    digitalWrite(pinDigit[0], B0); // thousands
    digitalWrite(pinDigit[1], B1); // hundreds
    digitalWrite(pinDigit[2], B1); // tens
    digitalWrite(pinDigit[3], B1); // ones
  }

  if (placeholder == 1) {
    digitalWrite(pinDigit[0], B1); // thousands
    digitalWrite(pinDigit[1], B0); // hundreds
    digitalWrite(pinDigit[2], B1); // tens
    digitalWrite(pinDigit[3], B1); // ones
  }

  if (placeholder == 2) {
    digitalWrite(pinDigit[0], B1); // thousands
    digitalWrite(pinDigit[1], B1); // hundreds
    digitalWrite(pinDigit[2], B0); // tens
    digitalWrite(pinDigit[3], B1); // ones
  }

  if (placeholder == 3) {
    digitalWrite(pinDigit[0], B1); // thousands
    digitalWrite(pinDigit[1], B1); // hundreds
    digitalWrite(pinDigit[2], B1); // tens
    digitalWrite(pinDigit[3], B0); // ones
  }
}
