#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <SoftwareSerial.h>

#define OLED_RESET 4
Adafruit_SSD1306 display1(OLED_RESET);

String nmeaStatement = "";

void setup()   {
  Serial.begin(9600);

  display1.begin(SSD1306_SWITCHCAPVCC, 0x3C);  // initialize with the I2C addr 0x3C (for the 128x32)
  display1.clearDisplay();
  display1.display();
  display1.setTextSize(1);
  display1.setTextColor(WHITE);
  display1.setCursor(0, 0);
  display1.print("Donduino v2.0");
  display1.setCursor(0, 10);
  display1.print("By Eldon B. Tenorio");
  display1.setCursor(0, 20);
  display1.print("ATmega328p");
  display1.setCursor(0, 30);
  display1.print("Arduino-compatible");
  display1.display();

}


void loop() {

}
