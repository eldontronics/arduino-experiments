
//Turn Samsung TV on/off repeatedly to test program. IR LED connected to pin 3.

#include <IRremote.h>

#define POWER         0x1818D02F
#define SAMSUNG_BITS  32 

IRsend irsend;

void setup()
{
  pinMode (3, OUTPUT);  //output as used in library
}

void loop() {

  Serial.println("Sending...");
  irsend.sendSAMSUNG(POWER, SAMSUNG_BITS); 
  Serial.println("Sent!");
  

  delay (5000);

}//end of loop

