#include "FastLED.h"
#define NUM_LEDS 24
#define DATA_PIN A1

CRGB leds[NUM_LEDS];

int delayInterval = 50;

void setup() {
  FastLED.addLeds<WS2812, DATA_PIN, GRB>(leds, NUM_LEDS);
}

void loop() {
  leds[0] = CRGB(255, 0, 0);
  leds[1] = CRGB(255, 0, 0);
  leds[2] = CRGB(255, 0, 0);
  leds[3] = CRGB(255, 0, 0);
  leds[4] = CRGB(255, 0, 0);
  leds[5] = CRGB(255, 0, 0);
  leds[6] = CRGB(255, 0, 0);
  leds[7] = CRGB(255, 0, 0);
  FastLED.show();
  delay(delayInterval);

//  leds[1] = CRGB(0, 255, 0);
//  FastLED.show();
//  delay(delayInterval);
//
//  leds[2] = CRGB(0, 0, 255);
//  FastLED.show();
//  delay(delayInterval);
//
//  leds[3] = CRGB(255, 0, 0);
//  FastLED.show();
//  delay(delayInterval);
//
//  leds[4] = CRGB(0, 255, 0);
//  FastLED.show();
//  delay(delayInterval);
//
//  leds[5] = CRGB(0, 0, 255);
//  FastLED.show();
//  delay(delayInterval);
//
//  leds[6] = CRGB(255, 0, 0);
//  FastLED.show();
//  delay(delayInterval);
//
//  leds[7] = CRGB(0, 255, 0);
//  FastLED.show();
//  delay(delayInterval);
//
//  // Turn off all lights
//  leds[0] = CRGB(0, 0, 0);
//  FastLED.show();
//  delay(delayInterval);
//
//  leds[1] = CRGB(0, 0, 0);
//  FastLED.show();
//  delay(delayInterval);
//
//  leds[2] = CRGB(0, 0, 0);
//  FastLED.show();
//  delay(delayInterval);
//
//  leds[3] = CRGB(0, 0, 0);
//  FastLED.show();
//  delay(delayInterval);
//
//  leds[4] = CRGB(0, 0, 0);
//  FastLED.show();
//  delay(delayInterval);
//
//  leds[5] = CRGB(0, 0, 0);
//  FastLED.show();
//  delay(delayInterval);
//
//  leds[6] = CRGB(0, 0, 0);
//  FastLED.show();
//  delay(delayInterval);
//
//  leds[7] = CRGB(0, 0, 0);
//  FastLED.show();
//  delay(delayInterval);

}
