/*
  Arduino Wireless Communication Tutorial
      Example 1 - Transmitter Code

  by Dejan Nedelkovski, www.HowToMechatronics.com

  Library: TMRh20/RF24, https://github.com/tmrh20/RF24/
*/

#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>

#include <LiquidCrystal_I2C.h>
LiquidCrystal_I2C lcd(0x3F, 16, 2); // set the LCD address to 0x27 for a 16 chars and 2 line display

RF24 radio(7, 8); // CNS, CE

const byte address[6] = "00001";

void setup() {

  lcd.init();
  lcd.noBacklight();
  delay(100);
  lcd.backlight();

  outputToLCD("Initializing", "Transmitter...");

  radio.begin();
  radio.openWritingPipe(address);
  radio.setPALevel(RF24_PA_MIN);
  radio.stopListening();
}

void loop() {
  char text[32];
  String number = (String) (random(1000, 10000));
  number.toCharArray(text, number.length());
  outputToLCD("Sending data:", text);
  radio.write(&text, sizeof(text));
  delay(1000);
}

void outputToLCD(String rowOneMessage, String rowTwoMessage) {
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print(rowOneMessage);
  lcd.setCursor(0, 1);
  lcd.print(rowTwoMessage);
}

