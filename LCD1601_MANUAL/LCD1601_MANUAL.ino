/*
  Arduino LCD Tutorial

  Crated by Dejan Nedelkovski,
  www.HowToMechatronics.com

*/

#include <LiquidCrystal.h> // includes the LiquidCrystal Library 
LiquidCrystal lcd(1, 2, 4, 5, 6, 7); // Creates an LC object. Parameters: (rs, enable, d4, d5, d6, d7)

void setup() {
  lcd.begin(16, 2); // Initializes the interface to the LCD screen, and specifies the dimensions (width and height) of the display }
  lcd.clear(); // Clears the display

}

void loop() {
  
  lcd.setCursor(0, 0); // Sets the location at which subsequent text written to the LCD will be displayed
  lcd.print("LCD1602 attached"); // Prints "Arduino" on the LCD

  lcd.setCursor(0, 1); // Sets the location at which subsequent text written to the LCD will be displayed
  lcd.print("to Pro Mini."); // Prints "Arduino" on the LCD

  

}
