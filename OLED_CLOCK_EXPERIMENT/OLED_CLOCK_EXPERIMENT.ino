#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define OLED_RESET 4

Adafruit_SSD1306 display(OLED_RESET);

#if (SSD1306_LCDHEIGHT != 64)
#error("Height incorrect, please fix Adafruit_SSD1306.h!");
#endif

// one sec = 6 deg = 0.10471967 rad
// 00 should be 1.570795

float degradsec   =  1.570795;
float degradmin   =  1.570795;
float degradhour  =  1.570795;

float px_sec;
float py_sec;

float px_min;
float py_min;

float px_hour;
float py_hour;

int seconds;
int minutes;
int hours;

void setup()   {

  Serial.begin(9600);
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);  // initialize with the I2C addr 0x3D (for the 128x64)
  display.clearDisplay();
  display.display();

  // Set the time here
  seconds = 0;
  minutes = 28;
  hours = 10;

  degradsec  = -1.570795 + (seconds * 0.10471967);
  degradmin  = -1.570795 + (minutes * 0.10471967) + (seconds / 60) * 0.10471967;
  degradhour = -1.570795 + (hours * 0.10471967 * 5) + ((minutes / 12) * 0.10471967);
}

void loop() {

  px_sec = ((display.width() / 2) + 20 * cos(degradsec));
  py_sec = ((display.height() / 2) + 20 * sin(degradsec));

  display.drawCircle(display.width() / 2, display.height() / 2, 21, WHITE);

  // Draw the seconds hand
  display.drawLine(display.width() / 2, display.height() / 2, px_sec + 1, py_sec + 1, WHITE);

  display.display();
  delay(100);
  display.clearDisplay();

  degradsec += 0.10471967;
  seconds++;

  if (seconds == 60) {
    seconds = 0;
    degradsec = -1.570795;
  }
}

