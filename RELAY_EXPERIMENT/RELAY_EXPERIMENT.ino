#include <IRremote.h>

int ledPin = 9;
int relayPin = 7;
int irReceivePin = 11;
int isTurnedOn = -1;

IRrecv irrecv(irReceivePin);

decode_results results;


void setup() {
  // put your setup code here, to run once:

  Serial.begin(9600);
  irrecv.enableIRIn(); // Start the receiver
    
  pinMode(ledPin, OUTPUT);
  pinMode(relayPin, OUTPUT);

  digitalWrite(ledPin, HIGH);
  digitalWrite(relayPin, LOW);
   
  
}

void loop() {
  if (irrecv.decode(&results)) {
    // Serial.println(results.value);
    if(results.value == 16623703){
      isTurnedOn = isTurnedOn * (-1);
    }

    if(isTurnedOn == 1){
      Serial.println("ON");
      digitalWrite(relayPin, HIGH);
    }
    else{
       Serial.println("OFF");
       digitalWrite(relayPin, LOW);
    }
  }
    irrecv.resume(); // Receive the next value
    delay(500);

}


