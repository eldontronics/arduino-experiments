// ROTARY ENCODR:
int pinA = 3;  // Connected to CLK on KY-040
int pinB = 4;  // Connected to DT on KY-040
int encoderPosCount = 0;
int pinALast;
int aVal;
boolean bCW;

// 10K POTENTIOMETER
int sensorValue;
int potValue = 0;
int delayTime = 0;
int sensorPin = A0;
int lastValue = 0;

void setup() {
  pinMode (pinA, INPUT);
  pinMode (pinB, INPUT);
  /* Read Pin A
    Whatever state it's in will reflect the last position
  */
  pinALast = digitalRead(pinA);
  Serial.begin (9600);
}

void loop() {
  aVal = digitalRead(pinA);

  if (aVal != pinALast) { // Means the knob is rotating
    // if the knob is rotating, we need to determine direction
    // We do that by reading pin B.
    if (digitalRead(pinB) != aVal) {  // Means pin A Changed first - We're Rotating Clockwise
      encoderPosCount += 1;
      bCW = true;
    } else {// Otherwise B changed first and we're moving CCW
      bCW = false;
      encoderPosCount -= 1;
    }

   // if (encoderPosCount > 180) encoderPosCount = 180;
   // if (encoderPosCount < 0) encoderPosCount = 0;


    Serial.println(encoderPosCount);

  }
  pinALast = aVal;


  sensorValue = analogRead(sensorPin);

  potValue = map(sensorValue, 0, 1023, 0, 180);
  if (potValue != lastValue) {
    Serial.println(potValue);
  }
  lastValue = potValue;
  
}

