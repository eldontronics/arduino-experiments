// Include the following libraries
#include <LiquidCrystal_I2C.h>
#include <Wire.h> 
#include <Keypad.h>

const byte numRows= 4; //number of rows on the keypad
const byte numCols= 4; //number of columns on the keypad

//keymap defines the key pressed according to the row and columns just as appears on the keypad
char keymap[numRows][numCols]= 
{
  {'1', '2', '3', 'A'}, 
  {'4', '5', '6', 'B'}, 
  {'7', '8', '9', 'C'},
  {'*', '0', '#', 'D'}
};

//Code that shows the the keypad connections to the arduino terminals
byte rowPins[numRows] = {9,8,7,6}; //Rows 0 to 3
byte colPins[numCols]= {5,4,3,2}; //Columns 0 to 3

//initializes an instance of the Keypad class
Keypad myKeypad= Keypad(makeKeymap(keymap), rowPins, colPins, numRows, numCols);

LiquidCrystal_I2C lcd(0x3F,16,2);  // Set the LCD I2C address : NOTE: Some LCD modules have 0x27, and some have 0x3F

char* stringInput;

void setup()
{   
  // Initialize the LCD
  lcd.begin(16,2);
  lcd.init();

  // Turn on and off the backlight just to verify that we are sending signals to the LCD module successfully
  lcd.backlight();
  delay(500);
  lcd.noBacklight();
  delay(500);
  lcd.backlight();

// For the keypad
  Serial.begin(9600);
  
}

void loop()
{

  char keypressed = myKeypad.getKey();
  
  if (keypressed != NO_KEY)
  {
    if (keypressed == 'C'){
      lcd.clear();
      lcd.setCursor(0,0);      
      stringInput = "";
    }
    else if (keypressed == '#'){
      
      if (stringInput == "0921"){
        lcd.clear();
        lcd.print("ACCESSED");
      }
      else{
        lcd.clear();
        lcd.print("DENIED");
        delay(4000);
        lcd.clear();
      }
      
      }
    else{
//        lcd.clear();
//        lcd.setCursor(0,0);
//        lcd.print("Hello, World!");  
//        delay(2000);        
          lcd.print(keypressed); 
          stringInput += (keypressed + "");
          Serial.print(stringInput);
      
    }
    
    // Serial.print(keypressed);
    
  }
  

}
