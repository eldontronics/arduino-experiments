/*
  Keyboard Message test

  For the Arduino Leonardo and Micro.

  Sends a text string when a button is pressed.

  The circuit:
   pushbutton attached from pin 4 to +5V
   10-kilohm resistor attached from pin 4 to ground

  created 24 Oct 2011
  modified 27 Mar 2012
  by Tom Igoe
  modified 11 Nov 2013
  by Scott Fitzgerald

  This example code is in the public domain.

  http://www.arduino.cc/en/Tutorial/KeyboardMessage
*/

#include "Keyboard.h"


String bitLockerCode = "Iloveyoumahalko14";
char bitLockerArray[49];

void setup() {
  // initialize control over the keyboard:
  Keyboard.begin();
  delay(1000);
  bitLockerCode.toCharArray(bitLockerArray, 49);

  // Press any key at LO
  Keyboard.press(KEY_BACKSPACE);
  Keyboard.release(KEY_BACKSPACE);

  delay(2000);
  for (int i = 0; i < sizeof(bitLockerArray); i++) {
    Keyboard.press(bitLockerArray[i]);
    Keyboard.release(bitLockerArray[i]);
    //    delay(50);
  }

  Keyboard.press(KEY_RETURN);
  Keyboard.release(KEY_RETURN);


}

void loop() {
}
//Iloveyoumahalko14



























