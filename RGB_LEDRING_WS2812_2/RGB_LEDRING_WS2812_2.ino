#include "FastLED.h"

#define NUM_LEDS 24

#define DATA_PIN A1

CRGB leds[NUM_LEDS];

int delayInterval = 100;

void setup() {
  FastLED.addLeds<WS2812, DATA_PIN, GRB>(leds, NUM_LEDS);
}

void loop() {

  leds[0] = CRGB(255, 0, 0);
  FastLED.show();
  delay(delayInterval);

  leds[1] = CRGB(0, 255, 0);
  FastLED.show();
  delay(delayInterval);

  leds[2] = CRGB(0, 0, 255);
  FastLED.show();
  delay(delayInterval);

  leds[3] = CRGB(255, 0, 0);
  FastLED.show();
  delay(delayInterval);

  leds[4] = CRGB(0, 255, 0);
  FastLED.show();
  delay(delayInterval);

  leds[5] = CRGB(0, 0, 255);
  FastLED.show();
  delay(delayInterval);

  leds[6] = CRGB(255, 0, 0);
  FastLED.show();
  delay(delayInterval);

  leds[7] = CRGB(0, 255, 0);
  FastLED.show();
  delay(delayInterval);


  // Turn off all lights
  leds[0] = CRGB(0, 0, 0);
  FastLED.show();
  delay(delayInterval);

  leds[1] = CRGB(0, 0, 0);
  FastLED.show();
  delay(delayInterval);

  leds[2] = CRGB(0, 0, 0);
  FastLED.show();
  delay(delayInterval);

  leds[3] = CRGB(0, 0, 0);
  FastLED.show();
  delay(delayInterval);

  leds[4] = CRGB(0, 0, 0);
  FastLED.show();
  delay(delayInterval);

  leds[5] = CRGB(0, 0, 0);
  FastLED.show();
  delay(delayInterval);

  leds[6] = CRGB(0, 0, 0);
  FastLED.show();
  delay(delayInterval);

  leds[7] = CRGB(0, 0, 0);
  FastLED.show();
  delay(delayInterval);

  for (int lightVar = 0; lightVar <= 255; lightVar++) {
    leds[0] = CRGB(lightVar, lightVar, lightVar);
    leds[1] = CRGB(lightVar, lightVar, lightVar);
    leds[2] = CRGB(lightVar, lightVar, lightVar);
    leds[3] = CRGB(lightVar, lightVar, lightVar);
    leds[4] = CRGB(lightVar, lightVar, lightVar);
    leds[5] = CRGB(lightVar, lightVar, lightVar);
    leds[6] = CRGB(lightVar, lightVar, lightVar);
    leds[7] = CRGB(lightVar, lightVar, lightVar);
    FastLED.show();
    delay(delayInterval / 100);
  }

  for (int lightVar = 255; lightVar >= 0; lightVar--) {
    leds[0] = CRGB(lightVar, lightVar, lightVar);
    leds[1] = CRGB(lightVar, lightVar, lightVar);
    leds[2] = CRGB(lightVar, lightVar, lightVar);
    leds[3] = CRGB(lightVar, lightVar, lightVar);
    leds[4] = CRGB(lightVar, lightVar, lightVar);
    leds[5] = CRGB(lightVar, lightVar, lightVar);
    leds[6] = CRGB(lightVar, lightVar, lightVar);
    leds[7] = CRGB(lightVar, lightVar, lightVar);
    FastLED.show();
    delay(delayInterval / 100);
  }


  for (int lightVar = 0; lightVar <= 255; lightVar++) {
    leds[0] = CRGB(lightVar, 0, 0);
    leds[1] = CRGB(lightVar, 0, 0);
    leds[2] = CRGB(lightVar, 0, 0);
    leds[3] = CRGB(lightVar, 0, 0);
    leds[4] = CRGB(lightVar, 0, 0);
    leds[5] = CRGB(lightVar, 0, 0);
    leds[6] = CRGB(lightVar, 0, 0);
    leds[7] = CRGB(lightVar, 0, 0);
    FastLED.show();
    delay(delayInterval / 100);
  }

  for (int lightVar = 255; lightVar >= 0; lightVar--) {
    leds[0] = CRGB(lightVar, 0, 0);
    leds[1] = CRGB(lightVar, 0, 0);
    leds[2] = CRGB(lightVar, 0, 0);
    leds[3] = CRGB(lightVar, 0, 0);
    leds[4] = CRGB(lightVar, 0, 0);
    leds[5] = CRGB(lightVar, 0, 0);
    leds[6] = CRGB(lightVar, 0, 0);
    leds[7] = CRGB(lightVar, 0, 0);
    FastLED.show();
    delay(delayInterval / 100);
  }

  for (int lightVar = 0; lightVar <= 255; lightVar++) {
    leds[0] = CRGB(0, lightVar, 0);
    leds[1] = CRGB(0, lightVar, 0);
    leds[2] = CRGB(0, lightVar, 0);
    leds[3] = CRGB(0, lightVar, 0);
    leds[4] = CRGB(0, lightVar, 0);
    leds[5] = CRGB(0, lightVar, 0);
    leds[6] = CRGB(0, lightVar, 0);
    leds[7] = CRGB(0, lightVar, 0);
    FastLED.show();
    delay(delayInterval / 100);
  }

  for (int lightVar = 255; lightVar >= 0; lightVar--) {
    leds[0] = CRGB(0, lightVar, 0);
    leds[1] = CRGB(0, lightVar, 0);
    leds[2] = CRGB(0, lightVar, 0);
    leds[3] = CRGB(0, lightVar, 0);
    leds[4] = CRGB(0, lightVar, 0);
    leds[5] = CRGB(0, lightVar, 0);
    leds[6] = CRGB(0, lightVar, 0);
    leds[7] = CRGB(0, lightVar, 0);
    FastLED.show();
    delay(delayInterval / 100);
  }

  for (int lightVar = 0; lightVar <= 255; lightVar++) {
    leds[0] = CRGB(0, 0, lightVar);
    leds[1] = CRGB(0, 0, lightVar);
    leds[2] = CRGB(0, 0, lightVar);
    leds[3] = CRGB(0, 0, lightVar);
    leds[4] = CRGB(0, 0, lightVar);
    leds[5] = CRGB(0, 0, lightVar);
    leds[6] = CRGB(0, 0, lightVar);
    leds[7] = CRGB(0, 0, lightVar);
    FastLED.show();
    delay(delayInterval / 100);
  }

  for (int lightVar = 255; lightVar >= 0; lightVar--) {
    leds[0] = CRGB(0, 0, lightVar);
    leds[1] = CRGB(0, 0, lightVar);
    leds[2] = CRGB(0, 0, lightVar);
    leds[3] = CRGB(0, 0, lightVar);
    leds[4] = CRGB(0, 0, lightVar);
    leds[5] = CRGB(0, 0, lightVar);
    leds[6] = CRGB(0, 0, lightVar);
    leds[7] = CRGB(0, 0, lightVar);
    FastLED.show();
    delay(delayInterval / 100);
  }


}
