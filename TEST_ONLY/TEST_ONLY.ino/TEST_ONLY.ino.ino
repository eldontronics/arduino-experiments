// ref u8glib: https://code.google.com/p/u8glib/
// To install u8glib on Arduino IDE: http://goo.gl/j3olBA
#include "U8glib.h"

U8GLIB_SSD1306_128X64 u8g(U8G_I2C_OPT_NONE | U8G_I2C_OPT_DEV_0);

const uint8_t bm[] PROGMEM = {
  0b11111111,
  0b11000011,
  0b11000011,
  0b11000011,
  0b11000011,
  0b11000011,
  0b11000011,
  0b11111111
};

static int WIDTH = 128;
static int HEIGHT = 64;

int x, y;

void setup(void) {
  x = 0;
  y = 0;
}

void loop(void) {

  u8g.firstPage();
  do {
    u8g.drawBitmapP( x, y, 1, 8, bm);
  } while ( u8g.nextPage() );

  delay(1);

  x += 8;
  if ( x >= WIDTH) {
    x = 0;
    y += 8;
    if ( y >= HEIGHT) {
      y = 0;
    }
  }
}
