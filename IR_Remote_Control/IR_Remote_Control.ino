/* Modified by Chris Targett
* Now includes more protocols
* Novemeber 2011

* IRremote: IRrecvDump - dump details of IR codes with IRrecv
* An IR detector/demodulator must be connected to the input RECV_PIN.
* Version 0.1 July, 2009
* Copyright 2009 Ken Shirriff
* http://arcfn.com
*
* Modified by Chris Targett to speed up the process of collecting
* IR (HEX and DEC) codes from a remote (to put into and .h file)
*
*/

#include <IRremote.h>

int RECV_PIN = 4;

IRrecv irrecv(RECV_PIN);

decode_results results;

void setup()
{
  Serial.begin(9600);
  irrecv.enableIRIn(); // Start the receiver
}

// Dumps out the decode_results structure.
// Call this after IRrecv::decode()
// void * to work around compiler issue
//void dump(void *v) {
//  decode_results *results = (decode_results *)v
void dump(decode_results *results) {
  int count = results->rawlen;
  if (results->decode_type == UNKNOWN) {
    Serial.print("Unknown encoding: ");
  }
    else if (results->decode_type == NEC) {
    Serial.print("Decoded NEC: ");
  }
  else if (results->decode_type == SONY) {
    Serial.print("Decoded SONY: ");
  }
  else if (results->decode_type == RC5) {
    Serial.print("Decoded RC5: ");
  }
  else if (results->decode_type == RC6) {
    Serial.print("Decoded RC6: ");
  }
  else if (results->decode_type == SAMSUNG) {
    Serial.print("Decoded SAMSUNG: ");
  }
  else if (results->decode_type == JVC) {
    Serial.print("Decoded JVC: ");
  }
  else if (results->decode_type == PANASONIC) {
    Serial.print("Decoded Panasonic: ");
  }
  Serial.print(results->value, HEX);
  Serial.print("(");
  Serial.print(results->bits, DEC);
  Serial.println(" bits)");
  Serial.print("#define Something_DEC ");
  Serial.println(results->value, DEC);
  Serial.print("#define Something_HEX ");
  Serial.println(results->value, HEX);
  Serial.print("Raw (");
  Serial.print(count, DEC);
  Serial.print("): ");
  for (int i = 0; i < count; i++) {
    if ((i % 2) == 1) {
      Serial.print(results->rawbuf[i]*USECPERTICK, DEC);
    }
    else {
      Serial.print(-(int)results->rawbuf[i]*USECPERTICK, DEC);
    }
    Serial.print(", ");
  }
  Serial.println("");
}

void loop() {
  if (irrecv.decode(&results)) {
    // dump(&results);
    // irrecv.resume(); // Receive the next value
    Serial.println(results.value);

    
  }
}



///*
// * IRremote: IRrecvDemo - demonstrates receiving IR codes with IRrecv
// * An IR detector/demodulator must be connected to the input RECV_PIN.
// * Version 0.1 July, 2009
// * Copyright 2009 Ken Shirriff
// * http://arcfn.com
// */
//
//#include <IRremote.h>
//
//int RECV_PIN = 11;
//int SEND_PIN = 3;
//int LED_PIN = 13;
//int ON_OFF = -1;
//
//IRrecv irrecv(RECV_PIN);
//IRsend irsend;
//
//decode_results results;
//
//void setup()
//{
//  Serial.begin(9600);
//  pinMode(LED_PIN, OUTPUT);
//  irrecv.enableIRIn(); // Start the receiver
//}
//
//void loop() {
//  if (irrecv.decode(&results)) {
//
//    
//    
//    // Serial.println(results.value, HEX);
////    if (results.value == 16623703){
////      ON_OFF = ON_OFF * (-1);
////    }
//
//  unsigned int irSignal[] = {0xB, 0x2, 0x4, 0xD, 0x7, 0xB, 0x8, 0x4};
//  irsend.sendRaw(irSignal, sizeof(irSignal) / sizeof(irSignal[0]), 38); //Note the approach used to automatically calculate the size of the array.
//  Serial.println(results.value, HEX);
//
////    if(ON_OFF == 1){
////      digitalWrite(LED_PIN, HIGH);
////    } 
////    else{
////      digitalWrite(LED_PIN, LOW);
////    }
//    
//    irrecv.resume(); // Receive the next value
//  }
//  delay(100);
//}
