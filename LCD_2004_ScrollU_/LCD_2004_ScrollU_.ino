//DFRobot.com
//Compatible with the Arduino IDE 1.0
//Library version:1.1
#include <Wire.h>
#include <LiquidCrystal_I2C.h>

struct News {
  int Index;
  String HeaderTitle;
};

LiquidCrystal_I2C lcd(0x3f, 20, 4); // set the LCD address to 0x27 for a 16 chars and 2 line display
String testText = "It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.";

void setup()
{
  Serial.begin(115200);
  lcd.init();  // initialize the lcd
  lcd.backlight();

}

void loop()
{
  scrollText("NEWS SOURCE", testText);
}


void scrollText_v2(String headerTitle, String headerDetail) {
  lcd.clear();

  News myNews[20];
  int newsIndex = 3;

  lcd.setCursor(0, 0);
  lcd.print(headerTitle);


  myNews[0].HeaderTitle =  "                    ";
  myNews[1].HeaderTitle =  "                    ";
  myNews[2].HeaderTitle =  "                    ";
  
  while (headerDetail.length() > 0) {
    myNews[newsIndex].HeaderTitle =  headerDetail.substring(0, 19);
    headerDetail = headerDetail.substring(19);
    newsIndex++;
  }

  // add additional 3 blank arrays
  myNews[newsIndex+1].HeaderTitle =  "                    ";
  myNews[newsIndex + 2].HeaderTitle =  "                    ";
  myNews[newsIndex + 3].HeaderTitle =  "                    ";

  for (int i = 0; i < newsIndex; i++) {
    lcd.setCursor(0, 3);
    lcd.print(myNews[2].HeaderTitle);
    lcd.setCursor(0, 2);
    lcd.print(myNews[1].HeaderTitle);
    lcd.setCursor(0, 1);
    lcd.print(myNews[0].HeaderTitle);

    for (int i = 0; i < newsIndex; i++) {
      myNews[i].HeaderTitle = myNews[i + 1].HeaderTitle;
    }

    delay(1000);

  }
}
