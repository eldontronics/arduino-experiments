#include <Arduino.h>
#include <TM1637Display.h>
#include <MyRealTimeClock.h>

MyRealTimeClock myRTC(5, 6, 7); // Assign Digital Pins CLK, DAT, RST

// Module connection pins (Digital Pins)
#define CLK 2
#define DIO 3

// The amount of time (in milliseconds) between tests
#define TEST_DELAY   2000

int toggleFlag = -1;

const uint8_t SEG_DONE[] = {
  SEG_B | SEG_C | SEG_D | SEG_E | SEG_G,           // d
  SEG_A | SEG_B | SEG_C | SEG_D | SEG_E | SEG_F,   // O
  SEG_C | SEG_E | SEG_G,                           // n
  SEG_A | SEG_D | SEG_E | SEG_F | SEG_G            // E
  };

const uint8_t SEG_ALL[] = {
  SEG_A | SEG_B | SEG_C | SEG_D | SEG_E | SEG_F | SEG_G,
  SEG_A | SEG_B | SEG_C | SEG_D | SEG_E | SEG_F | SEG_G,
  SEG_A | SEG_B | SEG_C | SEG_D | SEG_E | SEG_F | SEG_G,
  SEG_A | SEG_B | SEG_C | SEG_D | SEG_E | SEG_F | SEG_G
  };

const uint8_t SEG_HELP[] = {
  SEG_B | SEG_C | SEG_E | SEG_F | SEG_G,
  SEG_A | SEG_D | SEG_E | SEG_F | SEG_G,
  SEG_D | SEG_E | SEG_F,
  SEG_A | SEG_B | SEG_E | SEG_F | SEG_G
  };

TM1637Display display(CLK, DIO);

void setup()
{
  display.setBrightness(7);  
  Serial.begin(9600);
}

int _minute = 1;
int _second = -1;

void loop()
{
  myRTC.updateTime();
   _second++;
   toggleFlag *= -1;    
  if(_second == 60) { _minute++; }
  if(_second == 60) { _second = 0; };

  uint8_t segto;
  int value = 0;

  if(toggleFlag == 1){
    segto = 0x80 | display.encodeDigit( (value / 100 ) % 10);
  }
  else{
    segto = 0x80;
  }
  
  
  display.showNumberDecEx(myRTC.hours, segto, true, 2, 0); // number to be shown, show leading 0s, start, least significant (to show correctly, use 4,0)
  display.showNumberDecEx(myRTC.minutes, segto, true, 2, 2); // number to be shown, show leading 0s, start, least significant (to show correctly, use 4,0)
  delay(1000);
}
