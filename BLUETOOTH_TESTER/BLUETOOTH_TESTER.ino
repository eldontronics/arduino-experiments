#include <SoftwareSerial.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define OLED_RESET 4
Adafruit_SSD1306 display(OLED_RESET);

#define LOGO16_GLCD_HEIGHT 64
#define LOGO16_GLCD_WIDTH  128

#if (SSD1306_LCDHEIGHT != 64)
#error("Height incorrect, please fix Adafruit_SSD1306.h!");
#endif

SoftwareSerial bluetooth (10, 11); // D10 is TX on BLE, D11 is RX in BLE

int receivedCode = 0;
String receivedBluetoothString = "";

void setup() {
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);  // initialize with the I2C addr 0x3C (for the 128x32)
  display.clearDisplay();


  bluetooth.begin(9600);
  Serial.begin(9600);
  Serial.println("Waiting for code...");

  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(0, 0);
  display.println("Waiting for code...");
  display.display();
}

void loop() {

  while (bluetooth.available() > 0) {
    char receivedBluetoothChar = bluetooth.read();
    if (receivedBluetoothChar != '\n') {
      receivedBluetoothString += receivedBluetoothChar;
    }
    delay(10);
  }


  if (receivedBluetoothString != "") {
    if (receivedBluetoothString == "CLEAR") {
      Serial.println("Waiting for code...");
      display.clearDisplay();
      display.display();
    }
    else {
      display.clearDisplay();
      Serial.println(receivedBluetoothString);
      display.setTextSize(1);
      display.setTextColor(WHITE);
      display.setCursor(0, 0);
      display.println("Received Code :");
      display.setCursor(0, 10);
      display.println(receivedBluetoothString);
      display.display();
    }
    receivedBluetoothString = "";
  }
}

