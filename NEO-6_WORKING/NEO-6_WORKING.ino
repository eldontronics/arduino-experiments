#include <SoftwareSerial.h>
#include <Wire.h>
#include "SSD1306Ascii.h"
#include "SSD1306AsciiWire.h"

#define I2C_ADDRESS 0x3C
SSD1306AsciiWire oled;

const char gprmcSignal[6] = "$GPRMC";
const char gpzdaSignal[6] = "$GPZDA";
const int waitDelay     = 5;

const int redLed   = 12; // Indicator when GPS can't be determined
const int greenLed = 13; // Indicator when GPS location is successfully fetched

// Note: FOR WIRING NEO6M to Arduino:
// Pin 11 of the MCU should go to the TX pin of the Module,
// Pin 10 of the MCU should go to the RX pin of the module.
SoftwareSerial neo6mGPS(11, 10);

int nmeaIndex = -1;

char nmeaStatement[100];

void setup()
{
  Serial.begin(9600);
  neo6mGPS.begin(9600);

  pinMode(redLed, OUTPUT);
  pinMode(greenLed, OUTPUT);

  Wire.begin();
  oled.begin(&Adafruit128x32, I2C_ADDRESS);
  oled.set400kHz();
  oled.setFont(Adafruit5x7);

  oled.clear();
  oled.println("Initializing...");

  delay(2000);

}


void loop() {

  while (neo6mGPS.available() > 0) {

    char gpsData = (char)neo6mGPS.read();

    if (gpsData == '\n') {

      Serial.println(nmeaStatement);
      int countSignalChar = 0;

      for (int i = 0; i < sizeof(gprmcSignal); i++) {
        if (nmeaStatement[i] == gprmcSignal[i]) {
          countSignalChar++;
        }
      }

      int commaIndex = -1;
      int commaCount = 0;
      int commaUTCIndex = -1;
      int commaFlagIndex = -1;
      int commaLatIndex = -1;
      int commaLatDirectionIndex = -1;
      int commaLngIndex = -1;
      int commaLngDirectionIndex = -1;
      String gpsutc = "";
      String latitude = "";
      String longitude = "";

      if (countSignalChar == sizeof(gprmcSignal)) {
        // This is where the real treatment of data will be:

        for (int index = 0; index < sizeof(nmeaStatement); index++) {

          if (nmeaStatement[index] == ',') {
            commaCount++;
          }

          // Get the index of the desired value:
          if (commaCount == 1 && commaUTCIndex == -1) {
            commaUTCIndex = index;
          }
          else if (commaCount == 2 && commaFlagIndex == -1) {
            commaFlagIndex = index;
          }
          else if (commaCount == 3 && commaLatIndex == -1) {
            commaLatIndex = index;
          }
          else if (commaCount == 4 && commaLatDirectionIndex == -1) {
            commaLatDirectionIndex = index;
          }
          else if (commaCount == 5 && commaLngIndex == -1) {
            commaLngIndex = index;
          }
          else if (commaCount == 6 && commaLngDirectionIndex == -1) {
            commaLngDirectionIndex = index;
          }

        }

        if (nmeaStatement[commaFlagIndex + 1] == 'A') {
          // Show the coordinates here:

          digitalWrite(greenLed, HIGH);
          digitalWrite(redLed, LOW);

          // Get UTC
          int lengthUTC = commaFlagIndex - (commaUTCIndex + 1);
          Serial.println(nmeaStatement);
          Serial.println(lengthUTC);
          char utcValue[lengthUTC];
          for (int nmeaIndex = 0; nmeaIndex < lengthUTC; nmeaIndex++) {
            int _index = commaUTCIndex + nmeaIndex + 1;
            utcValue[nmeaIndex] = nmeaStatement[_index];
          }
          utcValue[lengthUTC] = '\0';
          gpsutc = (String)utcValue;

          // Get Latitude
          int lengthLatitude = commaLatDirectionIndex - commaLatIndex - 1;
          char latValue[lengthLatitude];
          for (int nmeaIndex = 0; nmeaIndex < lengthLatitude; nmeaIndex++) {
            int _index = commaLatIndex + nmeaIndex + 1;
            latValue[nmeaIndex] = nmeaStatement[_index];
          }
          latValue[lengthLatitude] = '\0';
          latitude = (String)latValue;

          // Get Longitude
          int lengthLongitude = commaLngDirectionIndex - commaLngIndex - 1;
          char lngValue[lengthLongitude];
          for (int nmeaIndex = 0; nmeaIndex < lengthLongitude; nmeaIndex++) {
            int _index = commaLngIndex + nmeaIndex + 1;
            lngValue[nmeaIndex] = nmeaStatement[_index];
          }
          lngValue[lengthLongitude] = '\0';
          longitude = (String)lngValue;

          Serial.print(latitude);
          Serial.println(nmeaStatement[commaLatDirectionIndex + 1]);
          Serial.print(longitude);
          Serial.println(nmeaStatement[commaLngDirectionIndex + 1]);

          // Get the Hour, Minute and Second of the NMEA statement
          String utcHR = gpsutc.substring(0, 2);
          String utcMN = gpsutc.substring(2, 4);
          String utcSC = gpsutc.substring(4, 9);;
          String trueUTC = utcHR + ":" + utcMN + ":" + utcSC;

          float latDec = latitude.substring(0, 2).toFloat();
          float latFlt = (latitude.substring(2).toFloat());
          float trueLatitude = ((latDec + (latFlt / 60.00)));

          float lngDec = longitude.substring(0, 3).toFloat();
          float lngFlt = (longitude.substring(3).toFloat());
          float trueLongitude = ((lngDec + (lngFlt / 60.00)));

          oled.clear();
          oled.setCursor(0, 0);
          oled.print("UTC:");
          oled.println(trueUTC);
          oled.print("Lat:");
          oled.print(trueLatitude, 6);
          oled.println(nmeaStatement[commaLatDirectionIndex + 1]);
          oled.print("Lon:");
          oled.print(trueLongitude, 6);
          oled.println(nmeaStatement[commaLngDirectionIndex + 1]);
          delay(5000);

        }
        else {
          oled.clear();
          oled.setCursor(0, 0);
          oled.println("Fetching GPS\ncoordinates...");
          oled.println("GPS location can't\nbe determined.");

          digitalWrite(greenLed, LOW);
          digitalWrite(redLed, HIGH);
          delay(5000);
        }
      }


      //      countSignalChar = 0;
      //      for (int i = 0; i < sizeof(gpzdaSignal); i++) {
      //        if (nmeaStatement[i] == gpzdaSignal[i]) {
      //          countSignalChar++;
      //        }
      //      }
      //
      //      if (countSignalChar == sizeof(gprmcSignal)) {
      //        oled.clear();
      //        oled.setCursor(0, 0);
      //        oled.println(nmeaStatement);
      //        delay(5000);
      //      }








      nmeaIndex = 0;
      memset(nmeaStatement, 0, sizeof(nmeaStatement));
    }
    else {
      nmeaStatement[nmeaIndex] = gpsData;
      nmeaIndex++;

    }
  }




}
