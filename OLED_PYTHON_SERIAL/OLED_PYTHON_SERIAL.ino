#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define OLED_RESET 4
Adafruit_SSD1306 display(OLED_RESET);

#if (SSD1306_LCDHEIGHT != 64)
#error("Height incorrect, please fix Adafruit_SSD1306.h!");
#endif

String serialValue = "";

String message[6] = {"", "", "", "", "", ""};
int line[6] = {0, 10, 20, 30, 40, 50};

void setup()   {                
  Serial.begin(9600);
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);
  display.clearDisplay();
  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(0,0);
  display.print("Program ready...");
  display.display();
}


void loop() {
  // While serial is receiving characters
  while(Serial.available() > 0){
      // Append all received characters
      serialValue = serialValue + (char)Serial.read();
      delay(10);
  }

  // When there is data received
  if(serialValue != ""){

    if(serialValue == "end"){
      display.clearDisplay();
      display.setCursor(0, 50);
      display.print("Program ended...");
      display.display();
      message[0] = "";       
      message[1] = "";       
      message[2] = "";       
      message[3] = "";       
      message[4] = "";       
      message[5] = "";
      return;
    }

     if(serialValue == "clear"){
      display.clearDisplay();
      display.display();
      serialValue = "";
      message[0] = "";       
      message[1] = "";       
      message[2] = "";       
      message[3] = "";       
      message[4] = "";       
      message[5] = "";
      return;
    }
   
    // Assign the previous message to the previous line
    message[0] = message[1];       
    message[1] = message[2];       
    message[2] = message[3];       
    message[3] = message[4];       
    message[4] = message[5];       

    // Assign the current message to the last message
    message[5] = "> " + serialValue;

    display.clearDisplay();
    display.setTextSize(1);
    display.setTextColor(WHITE);
  
    // output the current messages to the last line
    display.setCursor(0, line[5]); display.print(message[5]);

    // display the other messages
    display.setCursor(0, line[4]); display.print(message[4]);
    display.setCursor(0, line[3]); display.print(message[3]);
    display.setCursor(0, line[2]); display.print(message[2]);
    display.setCursor(0, line[1]); display.print(message[1]);
    display.setCursor(0, line[0]); display.print(message[0]);
    display.display();

    serialValue = "";
  }
}
