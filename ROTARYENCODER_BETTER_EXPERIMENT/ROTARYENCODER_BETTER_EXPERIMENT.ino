// Rotary Encoder Module
// GND            ->    Ground pin of Arduino
// VCC            ->    5V pin of Arduino
// Switch/Button  ->    Digital pin 2 (if you need to hook this to Arduino)
// DT/Output B    ->    Digital pin 7
// CLK/Output A   ->    Digital pin 6

#define OUTPUT_A    3
#define OUTPUT_B    4
#define SWITCH      2

int counter = 180;
int aState ;
int aLastState;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(OUTPUT_A, INPUT);
  pinMode(OUTPUT_B, INPUT);
  aLastState = digitalRead(OUTPUT_A);
}

void loop() {


  aState = digitalRead(OUTPUT_A);

  if (aState != aLastState) {
    if (digitalRead(OUTPUT_B) != aState) {
      counter += 1;
    }
    else {
      counter -= 1;
    }

    if (counter > 180) counter = 180;
    if (counter < 0) counter = 0;

  }

  Serial.println(counter);
  delay(20);

  aLastState = aState;

}
