#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define OLED_RESET 4
Adafruit_SSD1306 display(OLED_RESET);

#define LOGO16_GLCD_HEIGHT 64
#define LOGO16_GLCD_WIDTH  128

#if (SSD1306_LCDHEIGHT != 64)
#error("Height incorrect, please fix Adafruit_SSD1306.h!");
#endif



// If output is HIGH, then it is detecting white. Else, LOW (if Black);


int lineTrackingMod1Pin = 8;
int lineTrackingMod2Pin = 9;
int lineTrackingMod3Pin = 10;

int lineTrackingMod1Value = 0;
int lineTrackingMod2Value = 0;
int lineTrackingMod3Value = 0;

void setup()
{
  Serial.begin(9600);
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);

  pinMode(lineTrackingMod1Pin, INPUT);
  pinMode(lineTrackingMod2Pin, INPUT);
  pinMode(lineTrackingMod3Pin, INPUT);

}


void loop()
{

  lineTrackingMod1Value = digitalRead(lineTrackingMod1Pin);
  lineTrackingMod2Value = digitalRead(lineTrackingMod2Pin);
  lineTrackingMod3Value = digitalRead(lineTrackingMod3Pin);

  Serial.print("Line Tracker 1:"); Serial.println(lineTrackingMod1Value);
  Serial.print("Line Tracker 2:"); Serial.println(lineTrackingMod2Value);
  Serial.print("Line Tracker 3:"); Serial.println(lineTrackingMod3Value);
  Serial.println("");


  display.clearDisplay();

  display.setTextColor(WHITE);
  display.setCursor(0, 0);
  display.setTextSize(1);
  display.print("Has black color 1: ");
  display.setTextSize(2);

  if (lineTrackingMod1Value == LOW) {    
    display.print("O");
  }
  else {
    display.print("X");
  }


  display.setCursor(0, 20);
  display.setTextSize(1);
  display.print("Has black color 2: ");
  display.setTextSize(2);

  if (lineTrackingMod2Value == LOW) {    
    display.print("O");
  }
  else {
    display.print("X");
  }
  display.setCursor(0, 40);
  display.setTextSize(1);
  display.print("Has black color 3: ");
  display.setTextSize(2);
  
  if (lineTrackingMod3Value == LOW) {    
    display.print("O");
  }
  else {
    display.print("X");
  }
  
  display.display();

  delay(10);


}
