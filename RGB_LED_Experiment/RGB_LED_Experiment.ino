#include <IRremote.h>

int RECV_PIN = 4;

IRrecv irrecv(RECV_PIN);

decode_results results;

// NOTE: Longesrt lead is for GND
int redLED1Pin = A5;   // thru 220 ohm resistor
int greenLED1Pin = A4; // thru 220 ohm resistor
int blueLED1Pin = A3;   // thru 220 ohm resistor

int redLED2Pin = A2;   // thru 220 ohm resistor
int greenLED2Pin = A1; // thru 220 ohm resistor
int blueLED2Pin = A0;   // thru 220 ohm resistor

int redLED3Pin = 13;   // thru 220 ohm resistor
int greenLED3Pin = 12; // thru 220 ohm resistor
int blueLED3Pin = 11;   // thru 220 ohm resistor

int redLED4Pin = 10;   // thru 220 ohm resistor
int greenLED4Pin = 9; // thru 220 ohm resistor
int blueLED4Pin = 8;   // thru 220 ohm resistor

int redLED5Pin = 7;   // thru 220 ohm resistor
int greenLED5Pin = 6; // thru 220 ohm resistor
int blueLED5Pin = 5;   // thru 220 ohm resistor

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  irrecv.enableIRIn(); // Start the receiver
  
  pinMode(redLED1Pin, OUTPUT);
  pinMode(greenLED1Pin, OUTPUT);
  pinMode(blueLED1Pin, OUTPUT);

  pinMode(redLED2Pin, OUTPUT);
  pinMode(greenLED2Pin, OUTPUT);
  pinMode(blueLED2Pin, OUTPUT);

  pinMode(redLED3Pin, OUTPUT);
  pinMode(greenLED3Pin, OUTPUT);
  pinMode(blueLED3Pin, OUTPUT);

  pinMode(redLED4Pin, OUTPUT);
  pinMode(greenLED4Pin, OUTPUT);
  pinMode(blueLED4Pin, OUTPUT);

  pinMode(redLED5Pin, OUTPUT);
  pinMode(greenLED5Pin, OUTPUT);
  pinMode(blueLED5Pin, OUTPUT);  
}

void loop() {

  if (irrecv.decode(&results)) {
    // dump(&results);
    irrecv.resume(); // Receive the next value
    // Serial.println(results.value);   

    // 16580863 - RED
    if(results.value == 16580863){
        setRGBColor(redLED1Pin, greenLED1Pin, blueLED1Pin, 255, 0, 0);
        setRGBColor(redLED2Pin, greenLED2Pin, blueLED2Pin, 255, 0, 0);
        setRGBColor(redLED3Pin, greenLED3Pin, blueLED3Pin, 255, 0, 0);
        setRGBColor(redLED4Pin, greenLED4Pin, blueLED4Pin, 255, 0, 0);
        setRGBColor(redLED5Pin, greenLED5Pin, blueLED5Pin, 255, 0, 0);
    }

    // 16613503 - GREEN
    if (results.value == 16613503){
        setRGBColor(redLED1Pin, greenLED1Pin, blueLED1Pin, 0, 255, 0);
        setRGBColor(redLED2Pin, greenLED2Pin, blueLED2Pin, 0, 255, 0);
        setRGBColor(redLED3Pin, greenLED3Pin, blueLED3Pin, 0, 255, 0);
        setRGBColor(redLED4Pin, greenLED4Pin, blueLED4Pin, 0, 255, 0);
        setRGBColor(redLED5Pin, greenLED5Pin, blueLED5Pin, 0, 255, 0);      
    }
    
    // 16597183 - BLUE
    if (results.value == 16597183){
        setRGBColor(redLED1Pin, greenLED1Pin, blueLED1Pin, 0, 0, 255);
        setRGBColor(redLED2Pin, greenLED2Pin, blueLED2Pin, 0, 0, 255);
        setRGBColor(redLED3Pin, greenLED3Pin, blueLED3Pin, 0, 0, 255);
        setRGBColor(redLED4Pin, greenLED4Pin, blueLED4Pin, 0, 0, 255);
        setRGBColor(redLED5Pin, greenLED5Pin, blueLED5Pin, 0, 0, 255);      
    }


    
  }
  
  


}

void setRGBColor(int redLEDPin, int greenLEDPin, int blueLEDPin, int redValue, int greenValue, int blueValue){
  analogWrite(redLEDPin, redValue);
  analogWrite(greenLEDPin, greenValue);
  analogWrite(blueLEDPin, blueValue);
}

