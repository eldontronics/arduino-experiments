#include <SoftwareSerial.h>

SoftwareSerial mySerial(4, 2); // RX, TX of Arduino
String command = "";


void setup() {
  Serial.begin(9600);
  Serial.println("Type AT command!");
  mySerial.begin(9600);
}

void loop() {
  
  if(mySerial.available()){
    while(mySerial.available()){
      command += (char)mySerial.read();
    }

    Serial.print(command);
    command = "";    
  }

  if(Serial.available()){
    delay(10);
    mySerial.write(Serial.read());
  }

  
}
