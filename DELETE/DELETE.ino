String testValue1 = "fileinput:abcdefghji.xyz";
String testValue2 = "verse:II JN 123:456";

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
  Serial.println("Filename: " + testValue1.substring(0, 10)); // fileinput:
  Serial.println("Verse   : " + testValue2.substring(0, 6)); // fileinput:

  Serial.println("Filename: " + testValue1.substring(10)); // fileinput value
  Serial.println("Verse   : " + testValue2.substring(6)); // fileinput value
  delay(1000);
}
