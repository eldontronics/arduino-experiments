int ledPin = 11;

void setup() {
  // initialize digital pin LED_BUILTIN as an output.  
  Serial.begin(9600);
  pinMode(ledPin, OUTPUT);
}

void loop() {      
    blinkLED(ledPin, 5, true);  
}

void blinkLED(int ledPin, int times, bool isDelayAtEnd){
  for(int i = 1; i<= times; i++){
      digitalWrite(ledPin, HIGH);   // turn the LED on (HIGH is the voltage level)
      delay(100);                       // wait for a second
      digitalWrite(ledPin, LOW);   // turn the LED on (HIGH is the voltage level)
      delay(100);                       // wait for a second
  }

  if(isDelayAtEnd == true){
    delay(1000)  ;
  }
}


void longDelay(int ledPin, int waitms){
      digitalWrite(ledPin, HIGH);   // turn the LED on (HIGH is the voltage level)
      delay(waitms);                       // wait for a second
      digitalWrite(ledPin, LOW);   // turn the LED on (HIGH is the voltage level)
      delay(500);                       // wait for a second
}




