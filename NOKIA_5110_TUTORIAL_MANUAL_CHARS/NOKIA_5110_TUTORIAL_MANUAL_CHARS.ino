#define CE    7
#define RST   6
#define DC    5
#define DIN   4
#define CLK   3

byte characterArray [][6] = {
  {B00111110, B01010001, B01001001, B01000101, B00111110}, // 0
  {B01000010, B01000010, B01111111, B01000000, B01000000}, // 1
  {B01000010, B01100001, B01010001, B01001001, B01000110}, // 2
  {B00100001, B01000001, B01000101, B01001011, B00110001}, // 3
  {B00011000, B00010100, B00010010, B01111111, B00010000}, // 4
  {B00100111, B01000101, B01000101, B01000101, B00111001}, // 5
  {B00111100, B01001010, B01001001, B01001001, B00110000}, // 6
  {B00000001, B01110001, B00001001, B00000101, B00000011}, // 7
  {B00110110, B01001001, B01001001, B01001001, B00110110}, // 8
  {B00000110, B01001001, B01001001, B00101001, B00011110}, // 9
  {B00000000, B00011011, B00011011}, // :
  {B00000000, B01011011, B00111011}, // ;
  {}, // <
  {}, // =
  {}, // >
  {}, // ?
  {}, // @
  {B01111110, B00001001, B00001001, B00001001, B01111110}, // A
  {B01111111, B01001001, B01001001, B01001001, B00110110}, // B
  {B00111110, B01000001, B01000001, B01000001, B00100010}, // C
  {B01111111, B01000001, B01000001, B01000001, B00111110}, // D
  {B01111111, B01001001, B01001001, B01001001, B01000001}, // E
  {B01111111, B00001001, B00001001, B00001001, B00000001}, // F
  {B00111110, B01000001, B01000001, B01010001, B00110010}, // G
  {B01111111, B00001000, B00001000, B00001000, B01111111}, // H
  {B00000000, B01000001, B01111111, B01000001}, // I
  {B00100000, B01000000, B01000001, B00111111, B00000001}, // J
  {B01111111, B00001000, B00010100, B00100010, B01000001}, // K
  {B01111111, B01000000, B01000000, B01000000, B01000000}, // L
  {B01111111, B00000010, B00001100, B00000010, B01111111}, // M
  {B01111111, B00000100, B00001000, B00010000, B01111111}, // N
  {B00111110, B01000001, B01000001, B01000001, B00111110}, // O
  {B01111111, B00001001, B00001001, B00001001, B00000110}, // P
  {B00111110, B01000001, B01010001, B00100001, B01011110}, // Q
  {B01111111, B00001001, B00011001, B00101001, B01000110}, // R
  {B01000110, B01001001, B01001001, B01001001, B00110001}, // S
  {B00000001, B00000001, B01111111, B00000001, B00000001}, // T
  {B00111111, B01000000, B01000000, B01000000, B00111111}, // U
  {B00001111, B00110000, B01000000, B00110000, B00001111}, // V
  {B00111111, B01000000, B00111000, B01000000, B00111111}, // W
  {B01100011, B00010100, B00001000, B00010100, B01100011}, // X
  {B00000011, B00000100, B01111000, B00000100, B00000011}, // Y
  {B01100001, B01010001, B01001001, B01000101, B01000011}, // Z
};

void sendLCDCommand(byte dc, byte data)
{
  digitalWrite(DC, dc);
  digitalWrite(CE, LOW);
  shiftOut(DIN, CLK, MSBFIRST, data);
  digitalWrite(CE, HIGH);
}

void writeString(String message) {

  int msgSize = message.length();
  char msg[msgSize];

  message.toCharArray(msg, msgSize + 1);

  for (int indx = 0; indx < msgSize; indx++) {
    for (int byteIndex = 0; byteIndex < 5; byteIndex++) {
      sendLCDCommand(true, characterArray[(byte)msg[indx] - 48][byteIndex]);
    }
  }
}


void writeChars(char *charMsg) {
  while (*charMsg) {
    for (int byteIndex = 0; byteIndex < 5; byteIndex++) {
      sendLCDCommand(1, characterArray[((byte)*charMsg++) - 48][byteIndex]);
      sendLCDCommand(1, 0x00);
    }
       
  }
}


void clearLCD() {
  for (int i = 0; i < 504; i++) {
    sendLCDCommand(1, 0x00);
  }
}

void gotoXY(int x, int y) {
  sendLCDCommand(0, B10000000 | x); // x
  sendLCDCommand(0, B01000000 | y); // y
}

void setup() {

  Serial.begin(9600);

  pinMode(CE, OUTPUT);
  pinMode(RST, OUTPUT);
  pinMode(DC, OUTPUT);
  pinMode(DIN, OUTPUT);
  pinMode(CLK, OUTPUT);

  digitalWrite(RST, LOW);
  digitalWrite(RST, HIGH);

  sendLCDCommand(0, B00100001 );  // LCD Extended Commands.
  sendLCDCommand(0, B10110000 );  // Set LCD Vop (Contrast).
  sendLCDCommand(0, B00000100 );  // Set Temp coefficent.
  sendLCDCommand(0, B00010100 );  // LCD bias mode 1:48.
  sendLCDCommand(0, B00100000 );  // LCD Basic Commands.
  sendLCDCommand(0, B00001100 );  // LCD in normal mode.

  clearLCD();
  writeChars("E");

}

void loop() {
  //    gotoXY(30, 1);
  //    writeString("TEST1");
}

