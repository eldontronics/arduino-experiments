#include <SPI.h>
#include <SD.h>
#include <LiquidCrystal_I2C.h>

LiquidCrystal_I2C lcd(0x3F,16,2);  // set the LCD address to 0x27 for a 16 chars and 2 line display
File myFile;

String serialValue = "";
String previousValue = "";
String filename = "";


void setup()   {                
  Serial.begin(9600);
  lcd.init();
  lcd.noBacklight();
  delay(100);
  lcd.backlight();
  outputToLCD("System","ready...");
}

void loop() {
  // While serial is receiving characters
  while(Serial.available() > 0){
      // Append all received characters
      serialValue = serialValue + (char)Serial.read();
      delay(10);
  }

  // When there is data received
  if(serialValue != ""){

    if(serialValue.substring(0, 10) == "fileinput:"){
      filename = serialValue.substring(10); 
      serialValue = "";
      previousValue = "Log created.";

      // create the file here
      if (!SD.begin(4)) {
        outputToLCD("Initialization", "of card failed");
        return;
      }
      
    }
   
    if(serialValue == "clear" || serialValue == "end" ){
      lcd.clear();  
      serialValue ="";
    }
    else{ 

      if(serialValue.substring(0, 6) == "verse:"){
        serialValue = serialValue.substring(6);

        myFile = SD.open(filename, FILE_WRITE);
        
        if (myFile) {
          myFile.println(serialValue);        
        } 
        else {
          outputToLCD("Cannot write to", "SD Card");
          delay(2000);
        }
        
        myFile.close();
      }
      
      outputToLCD(previousValue, serialValue);
    } 
    previousValue = serialValue;
    serialValue ="";
    
  }
}

void outputToLCD(String rowOneMessage, String rowTwoMessage){
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print(rowOneMessage);
  lcd.setCursor(0, 1);
  lcd.print(rowTwoMessage);  
}

