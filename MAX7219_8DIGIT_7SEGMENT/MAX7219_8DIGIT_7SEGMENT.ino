#include "LedControl.h"
//#include <Wire.h>
#include <LiquidCrystal_I2C.h>
/*
  Now we need a LedControl to work with.
 ***** These pin numbers will probably not work with your hardware *****
  pin 12 is connected to the DataIn
  pin 11 is connected to the CLK
  pin 10 is connected to LOAD/CS
  We have only a single MAX72XX.
*/
LedControl lc = LedControl(12, 11, 10, 1);

LiquidCrystal_I2C lcd(0x3E, 16, 2); // set the LCD address to 0x27 for a 16 chars and 2 line display
LiquidCrystal_I2C lcd2(0x3F, 16, 2); // set the LCD address to 0x27 for a 16 chars and 2 line display

/* we always wait a bit between updates of the display */
unsigned long delaytime = 10;
int maxDigitLength = 8;
void setup() {
  // initialize the lcd
  lcd.init();
  lcd2.init();
  // Print a message to the LCD.
  lcd.backlight();
  lcd2.backlight();
  /*
    The MAX72XX is in power-saving mode on startup,
    we have to do a wakeup call
  */
  Serial.begin(9600);
  lc.shutdown(0, false);
  /* Set the brightness to a medium values */
  lc.setIntensity(0, 8);
  /* and clear the display */
  lc.clearDisplay(0);
}

void loop() {
  for (long i = 0; i <= 12345678; i++) {
    displayNumber(i, true);
    displayToLCD(lcd, "Counter x 2", (String)(i * 2));
    displayToLCD(lcd2, "Counter x 3", (String) (i * 3));
    delay(delaytime);
  }
  delay(5000);
  lc.clearDisplay(0);
}

void displayNumber(long number, bool appendZeroes) {
  String testNo = (String) number;
  String prefixZeroes = "";
  int numberLength = testNo.length();
  if (appendZeroes) {
    for (int i = 1; i <= maxDigitLength - numberLength; i++) {
      prefixZeroes.concat("0");
    }
    testNo = prefixZeroes + testNo;
  }
  numberLength =  testNo.length();
  for (int i = numberLength - 1; i >= 0; i--) {
    lc.setDigit(0, i, testNo.substring((numberLength - i), (numberLength - (i + 1))).toInt(), false);
  }
}

void displayToLCD(LiquidCrystal_I2C mLCD, String topRowText, String bottomRowText) {
  mLCD.setCursor(0, 0);
  mLCD.print(topRowText);
  mLCD.setCursor(0, 1);
  mLCD.print(bottomRowText);
}
