//We always have to include the library
#include "LedControl.h"

/*
 Now we need a LedControl to work with.
 ***** These pin numbers will probably not work with your hardware *****
 pin 12 is connected to the DataIn 
 pin 11 is connected to the CLK 
 pin 10 is connected to LOAD / CS
 We have only a single MAX72XX.
 */
LedControl lc=LedControl(12, 11, 10, 1);

int xAxis = A0;
int yAxis = A1;
int xValue = 0;
int yValue = 0;
int pressedPin = 2;
int notPressed = 0;

/* we always wait a bit between updates of the display */
unsigned long delaytime=2000;
byte heart[8] ={B01100110,B11111111,B11111111,B11111111,B01111110,B00111100,B00011000,B00000000};
byte i[8]     ={B11111111,B11111111,B00011000,B00011000,B00011000,B00011000,B11111111,B11111111};
byte u[8]     ={B11000011,B11000011,B11000011,B11000011,B11000011,B11000011,B11111111,B01111110};
byte dot1[8]  ={B00000000,B00000000,B00000000,B00000000,B00000000,B00000000,B11000000,B11000000};
byte dot2[8]  ={B00000000,B00000000,B00000000,B00000000,B00000000,B00000000,B00011000,B00011000};
byte dot3[8]  ={B00000000,B00000000,B00000000,B00000000,B00000000,B00000000,B00000011,B00000011};


void setup() {
  /*
   The MAX72XX is in power-saving mode on startup,
   we have to do a wakeup call
   */

  Serial.begin(9600);
  pinMode(xAxis, INPUT);
  pinMode(yAxis, INPUT);
  pinMode(pressedPin, INPUT_PULLUP);

  lc.shutdown(0,false);
  /* Set the brightness to a medium values */
  lc.setIntensity(0,8);
  /* and clear the display */
  lc.clearDisplay(0);
}

void loop() { 
//  xValue = constrain(map(analogRead(yAxis), 0, 1023, 0, 7), 0, 7);
//  yValue = constrain(map(analogRead(xAxis), 0, 1023, 0, 7), 0, 7);
//  notPressed = digitalRead(pressedPin);
//
//  Serial.print(xValue);
//  Serial.print(",");
//  Serial.println(yValue);
//  
//  lc.setLed(0, xValue, yValue, true);
// 
 
  lc.clearDisplay(0);  

displayCharacter(heart, 8);
 
}

void displayCharacter(byte displayChar[], int elementSize){  
  for(int i = 0; i <= elementSize - 1; i++){
    lc.setRow(0, i, displayChar[i]);  
  }  
}

