#include <dht.h>
#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <LiquidCrystal_I2C.h>

#define OLED_RESET 4
Adafruit_SSD1306 display(OLED_RESET);
LiquidCrystal_I2C lcd(0x3F,16,2);  // set the LCD address to 0x27 for a 16 chars and 2 line display
dht DHT;

#define DHT11_PIN 8
#if (SSD1306_LCDHEIGHT != 64)
#error("Height incorrect, please fix Adafruit_SSD1306.h!");
#endif

void setup()   {                
  Serial.begin(9600);
  lcd.init();
  lcd.noBacklight();
  delay(100);
  lcd.backlight();

  // by default, we'll generate the high voltage from the 3.3v line internally! (neat!)
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);  // initialize with the I2C addr 0x3D (for the 128x64)
  display.clearDisplay();

    //  DO NOT DELETE ----------------------
    //  display.setTextSize(1.5);
    //  display.setTextColor(WHITE);
    //  display.setCursor(30,0);
    //  display.println("I LOVE YOU");
    //  display.println("");
    //  display.setTextSize(2);
    //  display.println("  Mamang");
    //  display.println("   and");
    //  display.println("  Tatay!");
    //  display.display();
    // outputToLCD(" Bro. Efren and","   Sis. Vilma");

}


void loop() {
  int chk = DHT.read11(DHT11_PIN);
  display.clearDisplay();
  display.setTextSize(1.5);
  display.setTextColor(WHITE);
  display.setCursor(0,0);
  display.println("Temperature : ");
  display.setTextSize(3);
  display.print(DHT.temperature);
  display.println(" C");
  display.setTextSize(1.5);
  display.println("Humidity : ");
  display.setTextSize(3);
  display.print(DHT.humidity);
  display.println(" %");
  display.display();
  delay(1000);
}


void outputToLCD(String rowOneMessage, String rowTwoMessage){
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print(rowOneMessage);
  lcd.setCursor(0, 1);
  lcd.print(rowTwoMessage);  
}

