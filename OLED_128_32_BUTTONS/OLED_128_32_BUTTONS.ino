/*********************************************************************
  This is an example for our Monochrome OLEDs based on SSD1306 drivers

  Pick one up today in the adafruit shop!
  ------> http://www.adafruit.com/category/63_98

  This example is for a 128x32 size display using I2C to communicate
  3 pins are required to interface (2 I2C and one reset)

  Adafruit invests time and resources providing this open source code,
  please support Adafruit and open-source hardware by purchasing
  products from Adafruit!

  Written by Limor Fried/Ladyada  for Adafruit Industries.
  BSD license, check license.txt for more information
  All text above, and the splash screen must be included in any redistribution
*********************************************************************/

#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define OLED_RESET 4
Adafruit_SSD1306 display(OLED_RESET);

//#if (SSD1306_LCDHEIGHT != 32)
//#error("Height incorrect, please fix Adafruit_SSD1306.h!");
//#endif

int redButton   = 5;
int greenButton = 6;
int blueButton  = 7;

int isRedButtonClicked = -1;
int isGreenButtonClicked = -1;
int isBlueButtonClicked = -1;

void setup()   {
  Serial.begin(9600);

  pinMode(redButton, INPUT);
  pinMode(greenButton, INPUT);
  pinMode(blueButton, INPUT);

  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);  
  display.clearDisplay();
  display.display();
}


void loop() {

  if (digitalRead(redButton) == HIGH)   isRedButtonClicked    = isRedButtonClicked * -1;
  if (digitalRead(greenButton) == HIGH) isGreenButtonClicked  = isGreenButtonClicked * -1;
  if (digitalRead(blueButton) == HIGH)  isBlueButtonClicked   = isBlueButtonClicked * -1;

  display.clearDisplay();
  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(0, 0);
  display.print("Red Button   : ");

  if (isRedButtonClicked == 1) {
    display.print("ON");
  }
  else {
    display.print("OFF");
  }

  display.setCursor(0, 10);
  display.print("Green Button : ");

  if (isGreenButtonClicked == 1) {
    display.print("ON");
  }
  else {
    display.print("OFF");
  }

  display.setCursor(0, 20);
  display.print("Blue Button  : ");

  if (isBlueButtonClicked == 1) {
    display.print("ON");
  }
  else {
    display.print("OFF");
  }

  display.display();

  delay(100);
}



