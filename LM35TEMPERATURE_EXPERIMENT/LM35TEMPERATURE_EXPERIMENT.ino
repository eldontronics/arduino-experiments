float temp = 0.0;
int tempPin = A0;


void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:

  temp = analogRead(tempPin);
  temp = temp * 0.48828125;

  Serial.print("Temperature= ");
  Serial.print(temp);
  Serial.print(" *C");
  Serial.println("");
  delay(1000);
}
