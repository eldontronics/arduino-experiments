int pirPin = 9;
int ledPin = 13;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(pirPin, INPUT);
  pinMode(ledPin, OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
 int pirValue = digitalRead(pirPin);

 if( pirValue == HIGH){
  digitalWrite(ledPin, HIGH);
 }
 else{
  digitalWrite(ledPin, LOW);
 }
 Serial.println(pirValue);
 delay(10);

  
}
