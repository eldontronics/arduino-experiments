// This example shows how to connect to Cayenne using an ESP8266 and send/receive sample data.
// Make sure you install the ESP8266 Board Package via the Arduino IDE Board Manager and select the correct ESP8266 board before compiling.

//#define CAYENNE_DEBUG
#define CAYENNE_PRINT Serial
#include <CayenneMQTTESP8266.h>

#include <SimpleTimer.h>    // Download from https://github.com/jfturcot/SimpleTimer
#include <SimpleDHT.h>      // Download from https://github.com/adafruit/DHT-sensor-library

// WiFi network info.
char ssid[] = "Eldric";
char wifiPassword[] = "daomingerdec8";

// Cayenne authentication info. This should be obtained from the Cayenne Dashboard.
char username[] = "520f7c90-a413-11e7-8c02-137ff2c4ffef";
char password[] = "fea63778081bfc423c632e17a96062bfc63c9a82";
char clientID[] = "80a340b0-a5ac-11e7-b0e9-e9adcff3788e";

// DHT11 Pin
int pinDHT11 = 2;
SimpleDHT11 dht11;

// Sound Pin
int soundPin = 3;

// Relay Pin
int relayPin = 1;

// Timer
SimpleTimer timer;

void setup() {
  Serial.begin(9600);
  Cayenne.begin(username, password, clientID, ssid, wifiPassword);
  pinMode(relayPin, OUTPUT); // Relay
  digitalWrite(relayPin, HIGH);
  pinMode(soundPin, INPUT); // Sound
  timer.setInterval(500L, transmitData);
}

void loop() {

  Cayenne.loop();
  timer.run();
}


CAYENNE_IN(1) {
  if (getValue.asInt() == 1) { // NOTE: Channel = Virtual Pin
    digitalWrite(1, LOW);
  }
  else {
    digitalWrite(1, HIGH);
  }
}

void transmitData()
{
  byte temperature = 0;
  byte humidity = 0;
  int err = SimpleDHTErrSuccess;

  if ((err = dht11.read(pinDHT11, &temperature, &humidity, NULL)) != SimpleDHTErrSuccess) {
    Cayenne.virtualWrite(V4, 0);
    Cayenne.virtualWrite(V2, 0);
  }
  else {
    Cayenne.virtualWrite(V4, (int)temperature);
    Cayenne.virtualWrite(V2, (int)humidity);
  }

  if (digitalRead(soundPin) == HIGH) {
    Cayenne.virtualWrite(V3, HIGH);
  }
  else {
    Cayenne.virtualWrite(V3, LOW);
  }
}


