
/* This sketch sends a packet with random data to another radio and waits for
   the packet to be sent back.  It prints out the random data and the received data, which should be the same.
*/

#include <rf24g.h>
// We must instantiate the RF24_G object outside of the setup function so it is available in the loop function
RF24_G test;

#include <LiquidCrystal_I2C.h>
LiquidCrystal_I2C lcd(0x3F, 16, 2); // set the LCD address to 0x27 for a 16 chars and 2 line display


void setup() {

  lcd.init();                      // initialize the lcd
  lcd.backlight();
  lcd.setCursor(0, 0);
  lcd.print("Initializing");
  lcd.setCursor(0, 1);
  lcd.print("LCD 16x2...");
  delay(2000);

  Serial.begin(9600);
  // create the RF24G object with an address of 4, using pins 7 and 8
  test = RF24_G(4, 7, 8);

}

void loop() {
  // create a random number
  uint8_t randNumber = random(300);
  // create a variable to store the received number
  int actual;
  // declare the sender packet variable
  packet sender;
  // declare the receiver packet variable
  packet receiver;
  // set the destination of the packet to address 1
  sender.setAddress(1);
  // write the payload to the packet
  sender.addPayload(&randNumber, sizeof(int));
  // print out the original payload
  Serial.print("original number:");
  Serial.println(randNumber);

  outputToLCD("Sending Data:", (String)randNumber, 100);
  // send the packet, if it is successful try to read back the packet

  if (test.write(&sender) == true) {
    // wait until a packet is received
    while (test.available() != true);
    // copy the packet into the receiver object
    test.read(&receiver);
    // copy the payload into the actual value
    receiver.readPayload(&actual, sizeof(int));
    // print out the actual value received
    Serial.print("received number:");
    Serial.println(actual);
    Serial.println("");
  }

}

void outputToLCD(String message1, String message2, int delayValue) {
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print(message1);
  lcd.setCursor(0, 1);
  lcd.print(message2);
  delay(delayValue);
}

