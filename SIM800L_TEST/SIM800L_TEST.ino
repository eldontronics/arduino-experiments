#include <SoftwareSerial.h>

String tempSerial, tempGSM;

SoftwareSerial gsm(10, 11); // RX, TX of Arduino



void setup() {



  Serial.begin(9600); //Set Serial Monitor Baudrate

  Serial.println("SIM800L Test");

  gsm.begin(9600); // Set GSM Baudrate



}



void loop() {



  //Read from GSM and Print on Serial Monitor

  if (gsm.available())

  {

    tempGSM = gsm.readString(); // Read from GSM and Store it in this variable.
    Serial.println(tempGSM);    //Print the string from GSM to Serial Monitor.
    Serial.println("Received!");
    

  }



  //Write to GSM from user given Serial Monitor Inputs(AT Commands)

  if (Serial.available())

  {

    tempSerial = Serial.readString(); //Read from Serial Monitor and store it in this variable.
    gsm.println(tempSerial);   //Send the Serial Monitor Strings(AT Commands) to GSM.

  }

}
