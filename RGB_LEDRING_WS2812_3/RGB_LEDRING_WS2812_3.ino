#include "FastLED.h"

#define NUM_LEDS 24

#define DATA_PIN A1

CRGB leds[NUM_LEDS];

int delayInterval = 50;

void setup() {
  FastLED.addLeds<WS2812, DATA_PIN, GRB>(leds, NUM_LEDS);
}

void loop() {
  for (int i = 0; i < NUM_LEDS; i++) {
    leds[i] = CRGB(0, 0, 0);
    FastLED.show();
  }

    
}
