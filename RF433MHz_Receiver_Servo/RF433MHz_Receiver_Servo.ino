// ask_receiver.pde
// -*- mode: C++ -*-
// Simple example of how to use RadioHead to receive messages
// with a simple ASK transmitter in a very simple way.
// Implements a simplex (one-way) receiver with an Rx-B1 module

#include <RH_ASK.h>
#include <SPI.h> // Not actualy used but needed to compile
#include <Servo.h>

Servo baseServo;
Servo nodServo;

RH_ASK driver;
String receivedMsg = "";

void setup()
{
  Serial.begin(9600);  // Debugging only
  baseServo.attach(7);
  baseServo.write(0);
  nodServo.attach(6);
  nodServo.write(0);

  delay(200);
  baseServo.write(180);
  nodServo.write(180);
  delay(2000);
  baseServo.write(0);
  nodServo.write(0);
  delay(1000);

  if (!driver.init())
    Serial.println("init failed");
}

void loop()
{
  uint8_t buf[RH_ASK_MAX_MESSAGE_LEN];
  uint8_t buflen = sizeof(buf);

  if (driver.recv(buf, &buflen)) // Non-blocking
  {
    int i;

    Serial.print("Received data: ");
    for (int bufIndex = 0; bufIndex < buflen; bufIndex++) {
      receivedMsg += (char)(buf[bufIndex]);
    }

    int baseAngle = 0;
    int nodAngle = 0;

    int sepIndex =  receivedMsg.indexOf(",");
    baseAngle = receivedMsg.substring(0, sepIndex).toInt();
    nodAngle = receivedMsg.substring(sepIndex + 1).toInt();

    
    Serial.print(baseAngle);
    Serial.print(", ");
    Serial.println(nodAngle);

    
    baseServo.write(baseAngle);
    nodServo.write(nodAngle);
    
    //    int angle = receivedMsg.toInt();
    //    receiverServo.write(angle);

  }
  else {
    receivedMsg = "";
  }

}
