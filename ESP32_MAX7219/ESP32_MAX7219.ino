#include <SPI.h>
#include <Adafruit_GFX.h>
#include <Max72xxPanel.h>    //  max7219 library

int pinCS = 5; // Attach CS (D5) to this pin, DIN to MOSI (D23) and CLK to SCK (D18)

Max72xxPanel matrix = Max72xxPanel(pinCS, 1, 1);

const int _LEFT  = 0;
const int _RIGHT = 1;


void setup() {
  matrix.setIntensity(5); // Set brightness between 0 and 15
  matrix.fillScreen(0);
}

void loop() {
  matrix.fillScreen(0);
  turn(_LEFT);
  delay(500);
  matrix.fillScreen(0);
  turn(_RIGHT);
  delay(500);
}


void turn(int _direction) {

  int _maxIndex = 7;

  if (_direction == _RIGHT) {
    _maxIndex = 0;
  }


  matrix.drawPixel(abs(_maxIndex - 7), 3, 1);
  matrix.drawPixel(abs(_maxIndex - 7), 4, 1);
  matrix.drawPixel(abs(_maxIndex - 6), 2, 1);
  matrix.drawPixel(abs(_maxIndex - 6), 3, 1);
  matrix.drawPixel(abs(_maxIndex - 6), 4, 1);
  matrix.drawPixel(abs(_maxIndex - 6), 5, 1);
  matrix.drawPixel(abs(_maxIndex - 5), 1, 1);
  matrix.drawPixel(abs(_maxIndex - 5), 2, 1);
  matrix.drawPixel(abs(_maxIndex - 5), 5, 1);
  matrix.drawPixel(abs(_maxIndex - 5), 6, 1);
  matrix.drawPixel(abs(_maxIndex - 4), 0, 1);
  matrix.drawPixel(abs(_maxIndex - 4), 1, 1);
  matrix.drawPixel(abs(_maxIndex - 4), 6, 1);
  matrix.drawPixel(abs(_maxIndex - 4), 7, 1);
  matrix.drawPixel(abs(_maxIndex - 3), 0, 1);
  matrix.drawPixel(abs(_maxIndex - 3), 7, 1);
  matrix.drawPixel(abs(_maxIndex - 2), 3, 1);
  matrix.drawPixel(abs(_maxIndex - 2), 4, 1);
  matrix.drawPixel(abs(_maxIndex - 1), 2, 1);
  matrix.drawPixel(abs(_maxIndex - 1), 3, 1);
  matrix.drawPixel(abs(_maxIndex - 1), 4, 1);
  matrix.drawPixel(abs(_maxIndex - 1), 5, 1);
  matrix.drawPixel(abs(_maxIndex - 0), 1, 1);
  matrix.drawPixel(abs(_maxIndex - 0), 2, 1);
  matrix.drawPixel(abs(_maxIndex - 0), 5, 1);
  matrix.drawPixel(abs(_maxIndex - 0), 6, 1);
  matrix.write();
}
