#include <Servo.h>

Servo testServo1;
Servo testServo2;
Servo testServo3;
Servo testServo4;
Servo testServo5;

int servoPin1 = 9;
int servoPin2 = 10;
int servoPin3 = 11;
int servoPin4 = 12;
int servoPin5 = 13;

int flexFinger1 = A0;
int flexFinger2 = A1;
int flexFinger3 = A2;
int flexFinger4 = A3;
int flexFinger5 = A4;

int flexPosition1 = 0;
int flexPosition2 = 0;
int flexPosition3 = 0;
int flexPosition4 = 0;
int flexPosition5 = 0;

void setup() {
  // put your setup code here, to run once:

  Serial.begin(9600);
  
  testServo1.attach(servoPin1);
  testServo2.attach(servoPin2);
  testServo3.attach(servoPin3);
  testServo4.attach(servoPin4);
  testServo5.attach(servoPin5);

  testServo1.write(flexPosition1);
  testServo2.write(flexPosition2);
  testServo3.write(flexPosition3);
  testServo4.write(flexPosition4);
  testServo5.write(flexPosition5);
}

void loop() {

//  flexPosition1 = map(analogRead(flexFinger1), 0, 115, 0, 180);
//  flexPosition2 = map(analogRead(flexFinger2), 0, 35, 0, 180);
//  flexPosition3 = map(analogRead(flexFinger3), 0, 140, 0, 180);
//  flexPosition4 = map(analogRead(flexFinger4), 0, 115, 0, 180);
//  flexPosition5 = map(analogRead(flexFinger5), 0, 145, 0, 180);

  testServo1.write(0);
  delay(500);
  testServo1.write(180);
  delay(500);
  
  testServo2.write(0);
  delay(500);
  testServo2.write(180);
  delay(500);
  
  testServo3.write(0);
  delay(500);
  testServo3.write(180);
  delay(500);
  
  testServo4.write(0);
  delay(500);
  testServo4.write(180);
  delay(500);
  
  testServo5.write(0);
  delay(500);
  testServo5.write(180);
  delay(500);
  
//  testServo2.write(flexPosition2);
//  testServo3.write(flexPosition3);
//  testServo4.write(flexPosition4);
//  testServo5.write(flexPosition5);




}
