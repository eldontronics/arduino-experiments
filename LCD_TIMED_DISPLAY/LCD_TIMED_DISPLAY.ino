#include <LiquidCrystal_I2C.h>
LiquidCrystal_I2C lcd(0x3F,16,2);  // set the LCD address to 0x27 for a 16 chars and 2 line display



void setup()
{
  lcd.init();                      // initialize the lcd 
  lcd.backlight();
  lcd.setCursor(0, 0);
  lcd.print("Initializing");
  lcd.setCursor(0, 1);
  lcd.print("LCD 16x2...");
  delay(2000);
  
}
void loop()
{
  outputToLCD("Thanks be to GOD","Happy Pasalamat!", 3000);

  outputToLCD("Salamat sa DIOS,","Oh, Amang Banal!", 3000);

  outputToLCD("Maligayang","Pagpapasalamat", 3000);
}

void outputToLCD(String message1, String message2, int delayValue){
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print(message1);
  lcd.setCursor(0, 1);
  lcd.print(message2);
  delay(delayValue);
}


