// ask_transmitter.pde
// -*- mode: C++ -*-
// Simple example of how to use RadioHead to transmit messages
// with a simple ASK transmitter in a very simple way.
// Implements a simplex (one-way) transmitter with an TX-C1 module

#include <RH_ASK.h>
#include <SPI.h> // Not actually used but needed to compile

#define potBasePin A0
#define potNodPin A1

int potBaseValue = 0;
int potNodValue = 0;

int angleBase = 0;
int angleNod = 0;

RH_ASK driver;

void setup()
{
  Serial.begin(9600);    // Debugging only

  if (!driver.init())
    Serial.println("init failed");
}

void loop()
{

  potBaseValue = analogRead(potBasePin);
  potNodValue = analogRead(potNodPin);

  angleBase = map(potBaseValue, 0, 1023, 0, 180);
  angleNod = map(potNodValue, 0, 1023, 0, 180);

  String mData = ((String)angleBase) + "," + ((String)angleNod);
  char msg[20];
  mData.toCharArray(msg, sizeof(msg));
  Serial.print("Sending data: ");
  Serial.println(mData);
  driver.send((uint8_t *)msg, strlen(msg));
  driver.waitPacketSent();

  delay(50);

}
