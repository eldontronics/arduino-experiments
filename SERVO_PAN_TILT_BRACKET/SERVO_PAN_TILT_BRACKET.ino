#include <Servo.h>


Servo baseServo;
Servo tiltServo;
int baseServoPin = 8;
int tiltServoPin = 9;
int servoDelay = 10;

int vibrationPin = A0;
int vibrationReading = 0;


void setup() {
  // put your setup code here, to run once:

  Serial.begin(9600);

  baseServo.attach(baseServoPin);
  tiltServo.attach(tiltServoPin);

  baseServo.write(0);
  tiltServo.write(0);

  delay(2000);

}

void loop() {
  pan_0_to_90();
  tilt();
  pan_90_to_180();
  tilt();
  pan_180_to_90();
  tilt();
  pan_90_to_0();
  tilt();

  //  vibrationReading = map(analogRead(vibrationPin), 22, 1023, 0, 100);
  //  Serial.println(vibrationReading);
  //  delay(200);

}

void tilt() {

  for (int i = 0; i <= 180; i++) {
    tiltServo.write(i);
    delay(servoDelay);
  }

  for (int i = 180; i >= 0; i--) {
    tiltServo.write(i);
    delay(servoDelay);
  }
}

void pan_0_to_90() {
  for (int i = 0; i <= 90; i++) {
    baseServo.write(i);
    delay(servoDelay);
  }
}

void pan_90_to_180() {
  for (int i = 90; i <= 180; i++) {
    baseServo.write(i);
    delay(servoDelay);
  }
}

void pan_180_to_90() {
  for (int i = 180; i >= 90; i--) {
    baseServo.write(i);
    delay(servoDelay);
  }
}

void pan_90_to_0() {
  for (int i = 90; i >= 0; i--) {
    baseServo.write(i);
    delay(servoDelay);
  }
}


