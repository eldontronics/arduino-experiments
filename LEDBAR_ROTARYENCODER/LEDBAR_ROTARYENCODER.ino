int val = 0;
int encoder0PinA = 11; // CLK pin on Rotary Encoder
int encoder0PinB = 12; // DT pin on Rotary Encoder
int encoder0Pos = 0;
int encoder0PinALast = LOW;
int n = LOW;

void setup() {
  // put your setup code here, to run once:


  Serial.begin(9600);

  for(int i = 2; i <=10; i++){
    pinMode(i, OUTPUT);  
  }
  
  pinMode(encoder0PinA, INPUT);
  pinMode(encoder0PinB, INPUT);
}

void loop() {

  int value = 0;
  n = digitalRead(encoder0PinA);
  if( (encoder0PinALast == LOW) && (n = HIGH) ){
    if(digitalRead(encoder0PinB) == LOW){
      encoder0Pos--;
    }
    else{
      encoder0Pos++;
    }

    if( encoder0Pos > 110 ) encoder0Pos = 110;
    if( encoder0Pos < 0 ) encoder0Pos = 0;

    value = constrain((encoder0Pos / 10), 1, 11);
    turnOffAllLED();
    
    for(int ledPin = 2; ledPin <= value; ledPin++){
      digitalWrite(ledPin, HIGH);
    }
    
    Serial.println(encoder0Pos);
    Serial.println(value);
    Serial.println("----");
    
    
  }
  encoder0PinALast = n;
  
}

void turnOffAllLED(){
    for(int ledPin = 2; ledPin <= 10; ledPin++){
      digitalWrite(ledPin, LOW);
    }
}

