int pinLED[10] = {2, 3, 4, 5, 6, 7, 8, 9, 10, 11};

void setup() {
  for (int led = 0; led < 10; led++) {
    pinMode(pinLED[led], OUTPUT);
  }
}

void loop() {

  int rndNum = random(10);

  // put your main code here, to run repeatedly:
  for (int led = 0; led <= rndNum; led++) {

    digitalWrite(pinLED[led], HIGH);
    delay(10);
  }

  for (int led = (10 - 1); led >= 0; led--) {
    digitalWrite(pinLED[led], LOW);
  }

  //  for (int led = (10 - 1); led >= 0; led--) {
  //    digitalWrite(pinLED[led], HIGH);
  //    delay(100);
  //  }
  //
  //  for (int led = 0; led < 10; led++) {
  //    digitalWrite(pinLED[led], LOW);
  //    delay(100);
  //  }

}
