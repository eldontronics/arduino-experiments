#include <LiquidCrystal_I2C.h>
LiquidCrystal_I2C lcd(0x3E, 16, 2); // set the LCD address to 0x27 for a 16 chars and 2 line display


int topLeftLDR      = A0;
int topRightLDR     = A1;
int bottomLeftLDR   = A2;
int bottomRightLDR  = A3;

int topLeftLDRValue     = 0;
int topRightLDRValue    = 0;
int bottomLeftLDRValue  = 0;
int bottomRightLDRValue  = 0;

void setup() {
  Serial.begin(9600);
  lcd.init();
  lcd.clear();
  lcd.noBacklight();
  delay(300);
  lcd.backlight();
}

void loop() {
  lcd.clear();
  Serial.print("Top Left : ");
  topLeftLDRValue = map(analogRead(topLeftLDR), 0, 1023, 0, 100);
  lcd.setCursor(0, 0);
  lcd.print(topLeftLDRValue);
  Serial.println(topLeftLDRValue);

//  Serial.print("Top Right : ");
//  topRightLDRValue = map(analogRead(topRightLDR), 0, 1023, 0, 100);
//  lcd.setCursor(10, 0);
//  lcd.print(topRightLDRValue);
//  Serial.println(topRightLDRValue);
//
//  Serial.print("Bottom Left : ");
//  bottomLeftLDRValue = map(analogRead(bottomLeftLDR), 0, 1023, 0, 100);
//  lcd.setCursor(0, 1);
//  lcd.print(bottomLeftLDRValue);
//  Serial.println(bottomLeftLDRValue);
//
//  Serial.print("Bottom Right : ");
//  bottomRightLDRValue = map(analogRead(bottomRightLDR), 0, 1023, 0, 100);
//  lcd.setCursor(10, 1);
//  lcd.print(bottomRightLDRValue);
//  Serial.println(bottomRightLDRValue);

  Serial.println("-----------------------------");
  Serial.println("");
  delay(500);
}
