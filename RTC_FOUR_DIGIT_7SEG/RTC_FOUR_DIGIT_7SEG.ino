#include <TM1637Display.h>
#include <MyRealTimeClock.h>

// Module connection pins (Digital Pins)
#define CLK 2
#define DIO 3

bool toggleColon;
TM1637Display display(CLK, DIO);
MyRealTimeClock myRTC(6, 7, 8); // Assign Digital Pins CLK, DAT, RST

void setup()
{
  display.setBrightness(5);  
  Serial.begin(9600);
  // Second 00 | Minute 59 | Hour 10 | Day of Week 05 |Day 12 |  Month 07 | Year 2015 - Just in case you want to set the time manually
  // myRTC.setDS1302Time(00, 16, 9, 07, 13, 05, 2017);
}

void loop()
{  
  myRTC.updateTime();
  uint8_t segto;
  segto = 0x80 | display.encodeDigit(myRTC.hours % 10);
  
  int hours = myRTC.hours;
  if(hours > 12) hours -= 12;

  if(toggleColon){
    display.showNumberDecEx(hours, &segto ,true, 2, 0);
    toggleColon = false;     
  }
  else{
    display.showNumberDec(hours ,true, 2, 0);  
    toggleColon = true;   
  }
  
  display.showNumberDec(myRTC.minutes, true, 2, 2);
  delay(1000);

}
