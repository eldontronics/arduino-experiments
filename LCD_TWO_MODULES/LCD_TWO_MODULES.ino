#include <LiquidCrystal_I2C.h>

LiquidCrystal_I2C lcd1(0x3E, 16, 2); // set the LCD address to 0x3E for a 16 chars and 2 line display
LiquidCrystal_I2C lcd2(0x3F, 16, 2); // set the LCD address to 0x3F for a 16 chars and 2 line display

void setup()
{
  lcd1.init();                      // initialize the lcd1
  lcd2.init();                      // initialize the lcd2

  lcd1.backlight();
  lcd2.backlight();

  lcd1.setCursor(0, 0);
  lcd2.setCursor(0, 0);

  lcd1.print("Initializing");
  lcd2.print("Initializing");

  lcd1.setCursor(0, 1);
  lcd2.setCursor(0, 1);

  lcd1.print("LCD 1 16x2...");
  lcd2.print("LCD 2 16x2...");


  delay(2000);

}
void loop()
{
  outputToLCD1("Salamat sa DIOS", "Happy Pasalamat!", 0);
  outputToLCD2("Eldon", "Tenorio", 3000);

  outputToLCD1("Salamat, Ama!,", "Oh, Amang Banal!", 0);
  outputToLCD2("Mary Rose", "Tenorio", 3000);

  outputToLCD1("Maligayang", "Pagpapasalamat", 0);
  outputToLCD2("Eldric Marcus", "Tenorio", 3000);

}

void outputToLCD1(String message1, String message2, int delayValue) {
  lcd1.clear();
  lcd1.setCursor(0, 0);
  lcd1.print(message1);
  lcd1.setCursor(0, 1);
  lcd1.print(message2);
  delay(delayValue);
}

void outputToLCD2(String message1, String message2, int delayValue) {
  lcd2.clear();
  lcd2.setCursor(0, 0);
  lcd2.print(message1);
  lcd2.setCursor(0, 1);
  lcd2.print(message2);
  delay(delayValue);
}

