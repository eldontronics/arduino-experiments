/**
 *  Attaching trimpot to Arduino
 *  Author: Eldon B. Tenorio
 */
int mappedValue = 0;
int potValue    = 0;
int analogPin   = 0;

void setup() {      
Serial.begin(9600);
}

void loop() {
  // Raw values range from 0 - 1023
  potValue = analogRead(analogPin);

  // Display the raw value to the serial monitor    
  Serial.print("Raw value :");
  Serial.println(potValue);

  // Let's try mapping the raw values to 0 - 180
  mappedValue = map(potValue, 0, 1023, 0, 180);

  // Display the mapped value to the serial monitor
  Serial.print("Mapped value :");
  Serial.println(mappedValue);  
}



