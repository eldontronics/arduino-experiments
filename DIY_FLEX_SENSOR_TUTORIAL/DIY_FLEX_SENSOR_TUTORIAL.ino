int flexValue = 0;
int flexPin = A0;
int flexAngle = 0;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
  flexValue = analogRead(flexPin);
  Serial.println(flexValue);

  // Sample flex value mapping
  flexAngle = map(flexValue, 10, 143, 0, 180);
  Serial.println(flexAngle);

  delay(500);  
}


















