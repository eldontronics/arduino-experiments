/*********
  Rui Santos
  Complete project details at http://randomnerdtutorials.com
  Libraries can be downloaded here: https://github.com/esp8266/Arduino/tree/master/libraries
*********/

#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>

MDNSResponder mdns;

// Replace with your network credentials
const char* ssid = "Eldric";
const char* password = "daomingerdec8";

ESP8266WebServer server(80);

String webPage = "";

int blueLED = 1;
int redLED  = 2;

void setup(void) {

  webPage += "<h1 style=\"font-family:Arial;\">ESP8266 ESP-01 Web Server</h1>";
  webPage += "<p style=\"font-family:Arial;\">Red LED<a href=\"RedLEDOn\"><button>ON</button></a>&nbsp;<a href=\"RedLEDOff\"><button>OFF</button></a></p>";
  webPage += "<p style=\"font-family:Arial;\">Blue LED<a href=\"BlueLEDOn\"><button>ON</button></a>&nbsp;<a href=\"BlueLEDOff\"><button>OFF</button></a></p>";

  delay(1000);
  Serial.begin(115200);
  WiFi.begin(ssid, password);
  Serial.println("");

  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  if (mdns.begin("esp8266", WiFi.localIP())) {
    Serial.println("MDNS responder started");
  }

  delay(2000);
  server.on("/", []() {
    server.send(200, "text/html", webPage);
  });

  // LEDs
  server.on("/RedLEDOn", []() {
    server.send(200, "text/html", webPage);
    digitalWrite(redLED, HIGH);
    delay(1000);
  });
  server.on("/RedLEDOff", []() {
    server.send(200, "text/html", webPage);
    digitalWrite(redLED, LOW);
    delay(1000);
  });
  server.on("/BlueLEDOn", []() {
    server.send(200, "text/html", webPage);
    digitalWrite(blueLED, HIGH);
    delay(1000);
  });
  server.on("/BlueLEDOff", []() {
    server.send(200, "text/html", webPage);
    digitalWrite(blueLED, LOW);
    delay(1000);
  });

  server.begin();
  Serial.println("HTTP server started");


  delay(10000);

  // preparing GPIOs
  pinMode(redLED, OUTPUT);
  pinMode(blueLED, OUTPUT);

  digitalWrite(1, LOW);
  digitalWrite(2, LOW);

}

void loop(void) {
  server.handleClient();
}
