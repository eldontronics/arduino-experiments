// This example shows how to connect to Cayenne using an ESP8266 and send/receive sample data.
// Make sure you install the ESP8266 Board Package via the Arduino IDE Board Manager and select the correct ESP8266 board before compiling.

#include <SimpleDHT.h>

SimpleDHT11 dht11;

//#define CAYENNE_DEBUG
#define CAYENNE_PRINT Serial
#include <CayenneMQTTESP8266.h>

// WiFi network info.
char ssid[] = "Eldric";
char wifiPassword[] = "daomingerdec8";

// Cayenne authentication info. This should be obtained from the Cayenne Dashboard.
char username[] = "032651b0-8d81-11e7-9727-55550d1a07e7";
char password[] = "8d1d37cd312702aeaba4bf701156485c0cea70a0";
char clientID[] = "5a6676d0-ebd7-11e7-939f-b354e6d68235";

unsigned long lastMillis = 0;

int wVibrationPin = 12;
int wSoundPin     = 13;
int wDHT11Pin     = 14;
int wLightPin     =  0;
int wPIRPin       = 16;
int wPiezoPin     = 15;

long  wVibrationLevel   = 0;
long  wSoundLevel       = 0;
float wTemperatureLevel = 0;
float wHumidityLevel    = 0;
int   wLightLevel       = 0;
int   wMotionDetected   = 0;

void setup() {
  Serial.begin(9600);
  Cayenne.begin(username, password, clientID, ssid, wifiPassword);

  pinMode(wVibrationPin, INPUT);
  pinMode(wSoundPin, INPUT);
  pinMode(wPiezoPin, OUTPUT);

}

const int pitchLow = 200;
const int pitchHigh = 1000;
int pitchStep = 10;
int currentPitch = pitchLow;


void loop() {
  Cayenne.loop();

  if (millis() - lastMillis > 100) {
    lastMillis = millis();

    wVibrationLevel = GetVibrationReading();
    wSoundLevel = GetSoundReading();
    wLightLevel = map(GetLightReading(), 0, 1024, 0, 100);
    GetDHTReading();
    wMotionDetected = GetMotionReading();

    Serial.print("Vibration level     : ");
    Serial.println(wVibrationLevel);
    Serial.print("Sound level         : ");
    Serial.println(wSoundLevel);
    Serial.print("Light               : ");
    Serial.println(wLightLevel);
    Serial.print("Motion Detected     : ");
    Serial.println(wMotionDetected == 1);
    Serial.print("Temperature         : ");
    Serial.println(wTemperatureLevel);
    Serial.print("Humidity            : ");
    Serial.println(wHumidityLevel);
    Serial.println("---------------");

    Cayenne.virtualWrite(0, wVibrationLevel);
    Cayenne.virtualWrite(1, wSoundLevel);
    Cayenne.virtualWrite(2, wTemperatureLevel);
    Cayenne.virtualWrite(3, wHumidityLevel);
    Cayenne.virtualWrite(4, wLightLevel);
    Cayenne.virtualWrite(5, (wMotionDetected == 1));
    digitalWrite(wPiezoPin, (wMotionDetected == 1));

  }



}


long GetVibrationReading() {
  delay(10);
  long measurement = pulseIn (wVibrationPin, HIGH); //wait for the pin to get HIGH and returns measurement
  return measurement;
}

long GetSoundReading() {
  delay(10);
  long measurement = pulseIn (wSoundPin, HIGH); //wait for the pin to get HIGH and returns measurement
  return measurement;
}

int GetLightReading() {
  delay(10);
  int measurement = analogRead(wLightPin);
  return measurement;
}

bool GetMotionReading() {
  bool measurement = digitalRead(wPIRPin); //wait for the pin to get HIGH and returns measurement
  return measurement;
}

void GetDHTReading()
{
  delay(10);
  byte temperature = 0;
  byte humidity = 0;
  int err = SimpleDHTErrSuccess;

  if ((err = dht11.read(wDHT11Pin, &temperature, &humidity, NULL)) != SimpleDHTErrSuccess) {
    wTemperatureLevel   = 0.00;
    wHumidityLevel      = 0.00;
  }
  else {
    wTemperatureLevel   = (float)temperature;
    wHumidityLevel      = (int)humidity;
  }
}

//Default function for processing actuator commands from the Cayenne Dashboard.
//You can also use functions for specific channels, e.g CAYENNE_IN(1) for channel 1 commands.
CAYENNE_IN_DEFAULT()
{
  CAYENNE_LOG("CAYENNE_IN_DEFAULT(%u) - %s, %s", request.channel, getValue.getId(), getValue.asString());
  //Process message here. If there is an error set an error message using getValue.setError(), e.g getValue.setError("Error message");

  if (request.channel == 6) {
    if (getValue.asInt() == 1) {
      // digitalWrite(wPiezoPin, HIGH);
      SoundAlarm();
    }
    else {
      digitalWrite(wPiezoPin, LOW);
    }
  }

}

void SoundAlarm() {
  //  for (int i = 0; i <= 10; i++) {
  //    tone(wPiezoPin, 3000, 1000);
  //    delay(100);
  //    tone(wPiezoPin, 1000, 1000);
  //    delay(100);
  //  }

  tone(wPiezoPin, currentPitch, 10);
  currentPitch += pitchStep;
  if (currentPitch >= pitchHigh) {
    pitchStep = -pitchStep;
  }
  else if (currentPitch <= pitchLow) {
    pitchStep = -pitchStep;
  }
  delay(10);

}

