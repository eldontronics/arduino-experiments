
#include <SoftwareSerial.h>
#include <LiquidCrystal_I2C.h>
LiquidCrystal_I2C lcd(0x3B, 16, 2); // set the LCD address to 0x27 for a 16 chars and 2 line display

SoftwareSerial mySerial(3, 4); // D3 - Connect to BT TX, D4 - Connect to BT RX
int countData = 0;


void setup()
{
  lcd.init(); // initialize the lcd
  lcd.noBacklight();
  delay(1000);
  lcd.backlight();

  mySerial.begin(38400);
  Serial.begin(9600);

  lcd.setCursor(0, 0);
  lcd.print("Initializing");
  lcd.setCursor(0, 1);
  lcd.print("Master BT...");
 
  delay(1000);

}

void loop()
{

  String data = (String) countData;
  byte dataByte[data.length()];
  data.getBytes(dataByte, data.length() + 1);
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Sending data:");
  lcd.setCursor(0, 1);
  lcd.print(data);
  mySerial.write(dataByte, data.length() + 1);
  countData++;
  delay(200);
}
