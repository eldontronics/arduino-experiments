#include <SoftwareSerial.h>
#include <LiquidCrystal_I2C.h>
LiquidCrystal_I2C lcd(0x3F, 16, 2); // set the LCD address to 0x27 for a 16 chars and 2 line display

SoftwareSerial bluetooth (3, 4); // D3 - Connect to BT TX, D4 - Connect to BT RX

int receivedCode = 0;
String receivedBluetoothString = "";

void setup() {
  lcd.init(); // initialize the lcd
  lcd.noBacklight();
  delay(1000);
  lcd.backlight();

  bluetooth.begin(9600);
  Serial.begin(9600);
  Serial.println("Waiting for code...");

  lcd.setCursor(0, 0);
  lcd.print("Initializing");
  lcd.setCursor(0, 1);
  lcd.print("Slave BT...");

  delay(2000);

  lcd.setCursor(0, 0);
  lcd.print("Slave bluetooth");
  lcd.setCursor(0, 1);
  lcd.print("is now ready...");

}

void loop() {

  while (bluetooth.available() > 0) {
    char receivedBluetoothChar = bluetooth.read();
    if (receivedBluetoothChar != '\n') {
      receivedBluetoothString += receivedBluetoothChar;
    }
    delay(20);
  }


  if (receivedBluetoothString != "") {
    if (receivedBluetoothString == "CLEAR") {
      Serial.println("Waiting for code...");
      lcd.clear();
    }
    else {
      receivedBluetoothString.remove(receivedBluetoothString.length()-1);
      Serial.print(receivedBluetoothString);
      Serial.print(", ");
      Serial.println(receivedBluetoothString.length());
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print("Received Data:");
      lcd.setCursor(0, 1);
      lcd.print( receivedBluetoothString);
    }
    receivedBluetoothString = "";
  }
}

