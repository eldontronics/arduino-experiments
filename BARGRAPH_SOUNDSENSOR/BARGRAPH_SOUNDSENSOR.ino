int soundSensor = A0;

int pinLED[10] = {2, 3, 4, 5, 6, 7, 8, 9, 10, 11};

void setup() {
  //  Serial.begin(9600);
  for (int led = 0; led < 10; led++) {
    pinMode(pinLED[led], OUTPUT);
  }
}

void loop() {

  int sensorValue = analogRead(soundSensor);
  int treatedValue = map(sensorValue, 0, 1023, 0, 9);

  for (int led = 0; led <= treatedValue; led++) {
    digitalWrite(pinLED[led], HIGH);
    delay(5);
  }

  for (int led = 9; led >= 0; led--) {
    digitalWrite(pinLED[led], LOW);
  }



}
