// Analog Pin A5 for the Probe Antenae
int antenaPin = A5;
int antenaValue = 0;
int mappedValue = 0;

// My first 7-segment display experiment
int a = 2;  //For displaying segment "a"
int b = 3;  //For displaying segment "b"
int c = 4;  //For displaying segment "c"
int d = 5;  //For displaying segment "d"
int e = 6;  //For displaying segment "e"
int f = 8;  //For displaying segment "f"
int g = 9;  //For displaying segment "g"
int dot = 10; // For displaying the dot;

void setup() {

  Serial.begin(9600);
  
//  pinMode(a, OUTPUT);  // segment a
//  pinMode(b, OUTPUT);  // segment b
//  pinMode(c, OUTPUT);  // segment c
//  pinMode(d, OUTPUT);  // segment d
//  pinMode(e, OUTPUT);  // segment e
//  pinMode(f, OUTPUT);  // segment f
//  pinMode(g, OUTPUT);  // segment g
//  pinMode(dot, OUTPUT);  // dot
}

void loop() {
  antenaValue = analogRead(antenaPin);
  mappedValue = map(antenaValue, 0, 60, 0, 9);  
  displayNumber(mappedValue);
  Serial.print("Raw value: ");
  Serial.println(antenaValue);
//  Serial.print("Mapped value: ");
//  Serial.println(mappedValue);
  Serial.println("");
   
  delay(500);
}

void charDot(){
  digitalWrite(dot,HIGH);
  delay(200);
  digitalWrite(dot,LOW);
  delay(200);
}

void digitOne(){
  digitalWrite(a,LOW);
  digitalWrite(b,HIGH);
  digitalWrite(c,HIGH);
  digitalWrite(d,LOW);
  digitalWrite(e,LOW);
  digitalWrite(f,LOW);
  digitalWrite(g,LOW);
  digitalWrite(dot,LOW);
}

void digitTwo(){
  digitalWrite(a,HIGH);
  digitalWrite(b,HIGH);
  digitalWrite(c,LOW);
  digitalWrite(d,HIGH);
  digitalWrite(e,HIGH);
  digitalWrite(f,LOW);
  digitalWrite(g,HIGH);
  digitalWrite(dot,LOW);
}

void digitThree(){
  digitalWrite(a,HIGH);
  digitalWrite(b,HIGH);
  digitalWrite(c,HIGH);
  digitalWrite(d,HIGH);
  digitalWrite(e,LOW);
  digitalWrite(f,LOW);
  digitalWrite(g,HIGH);
  digitalWrite(dot,LOW);
}

void digitFour(){
  digitalWrite(a,LOW);
  digitalWrite(b,HIGH);
  digitalWrite(c,HIGH);
  digitalWrite(d,LOW);
  digitalWrite(e,LOW);
  digitalWrite(f,HIGH);
  digitalWrite(g,HIGH);
  digitalWrite(dot,LOW);
}

void digitFive(){
  digitalWrite(a,HIGH);
  digitalWrite(b,LOW);
  digitalWrite(c,HIGH);
  digitalWrite(d,HIGH);
  digitalWrite(e,LOW);
  digitalWrite(f,HIGH);
  digitalWrite(g,HIGH);
  digitalWrite(dot,LOW);
}

void digitSix(){
  digitalWrite(a,HIGH);
  digitalWrite(b,LOW);
  digitalWrite(c,HIGH);
  digitalWrite(d,HIGH);
  digitalWrite(e,HIGH);
  digitalWrite(f,HIGH);
  digitalWrite(g,HIGH);
  digitalWrite(dot,LOW);
}

void digitSeven(){
  digitalWrite(a,HIGH);
  digitalWrite(b,HIGH);
  digitalWrite(c,HIGH);
  digitalWrite(d,LOW);
  digitalWrite(e,LOW);
  digitalWrite(f,LOW);
  digitalWrite(g,LOW);
  digitalWrite(dot,LOW);
}

void digitEight(){
  digitalWrite(a,HIGH);
  digitalWrite(b,HIGH);
  digitalWrite(c,HIGH);
  digitalWrite(d,HIGH);
  digitalWrite(e,HIGH);
  digitalWrite(f,HIGH);
  digitalWrite(g,HIGH);
  digitalWrite(dot,LOW);
}

void digitNine(){
  digitalWrite(a,HIGH);
  digitalWrite(b,HIGH);
  digitalWrite(c,HIGH);
  digitalWrite(d,HIGH);
  digitalWrite(e,LOW);
  digitalWrite(f,HIGH);
  digitalWrite(g,HIGH);
  digitalWrite(dot,LOW);
}

void digitZero(){
  digitalWrite(a,HIGH);
  digitalWrite(b,HIGH);
  digitalWrite(c,HIGH);
  digitalWrite(d,HIGH);
  digitalWrite(e,HIGH);
  digitalWrite(f,HIGH);
  digitalWrite(g,LOW);
  digitalWrite(dot,LOW);
}

void turnOffAllSegments(){
  // turn all segments off
  digitalWrite(a,LOW);
  digitalWrite(b,LOW);
  digitalWrite(c,LOW);
  digitalWrite(d,LOW);
  digitalWrite(e,LOW);
  digitalWrite(f,LOW);
  digitalWrite(g,LOW);
  digitalWrite(dot,LOW);  
}

void displayNumber(int number){
  if (number == 0) { digitZero(); }
  if (number == 1) { digitOne(); }
  if (number == 2) { digitTwo(); }
  if (number == 3) { digitThree(); }
  if (number == 4) { digitFour(); }
  if (number == 5) { digitFive(); }
  if (number == 6) { digitSix(); }
  if (number == 7) { digitSeven(); }
  if (number == 8) { digitEight(); }
  if (number == 9) { digitNine(); }
}


