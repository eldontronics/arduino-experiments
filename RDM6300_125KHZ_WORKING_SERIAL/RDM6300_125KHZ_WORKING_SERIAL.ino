#include <SoftwareSerial.h>
SoftwareSerial RFID(2, 3); // RX of module connected to Pin 3, and TX of module connected to Pin 2

int i;
int dataCount = 0;
int data[1][14] = {{2, 53, 48, 48, 48, 53, 48, 67, 66, 49, 50, 68, 57, 3}};
int currentCard[14] = {1,2,3,4,5,6,7,8,9,0,1,2,3,4};
bool isCardCompleted = false;

void setup()
{
  RFID.begin(9600); // start serial to RFID reader
  Serial.begin(9600); // start serial to PC
  Serial.println("RDM6300 Ready...");
  
}
void loop()
{
  while (RFID.available() > 0)
  {    
      if (dataCount <= 14) {
        i = RFID.read();
        currentCard[dataCount] = (int)i;            
        dataCount++;        
      }
      else {                
        isCardCompleted = true;
        RFID.flush();
        dataCount = 0;      
      }    
  }

  if(isCardCompleted){
    for(int indx=0; indx<14; indx++){
      Serial.print(currentCard[indx], DEC);
      Serial.print(" ");
    }
    Serial.println("");
  isCardCompleted = false;
  }
  
}
