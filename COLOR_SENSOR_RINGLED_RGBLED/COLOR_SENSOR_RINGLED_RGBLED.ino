#include "FastLED.h"

#define NUM_LEDS 8
#define DATA_PIN 7


CRGB leds[NUM_LEDS];

int delayInterval = 50;

const  int s0   = 8 ;
const  int s1   = 9 ;
const  int s2   = 12 ;
const  int s3   = 11 ;
const  int out  = 10 ;

// Pin the leds
int redPin    = A0; // The only lead at the side of the longer lead (GND)
int greenPin  = A1; // The lead closest to othe red pin and GND pin
int bluePin   = A2; // the farthest from the GND pin

// Variables that store the value of the colors
int redValue    = 0 ;
int greenValue  = 0 ;
int blueValue   = 0 ;

void setup ()
{
  FastLED.addLeds<WS2812, DATA_PIN, GRB>(leds, NUM_LEDS);

  pinMode (s0, OUTPUT);
  pinMode (s1, OUTPUT);
  pinMode (s2, OUTPUT);
  pinMode (s3, OUTPUT);
  pinMode (out, INPUT);

  pinMode(redPin, OUTPUT);
  pinMode(greenPin, OUTPUT);
  pinMode(bluePin, OUTPUT);

  Serial.begin ( 9600 );
  digitalWrite (s0, HIGH);
  digitalWrite (s1, HIGH);
}

void loop ()
{
  color (); // Call the routine le colors
  // Shows the serial monitor the values ​​detected

  redValue = constrain(map(redValue, 7, 57, 255, 0), 0, 255);
  greenValue = constrain(map(greenValue, 7, 62, 255, 0), 0, 255);
  blueValue = constrain(map(blueValue, 5, 47, 255, 0), 0, 255);

  showRingLEDColors(redValue, greenValue, blueValue);

  Serial.print ("Red : ");
  Serial.print (redValue);
  Serial.print (", Green : ");
  Serial.print (greenValue);
  Serial.print (", Blue : ");
  Serial.println (blueValue);
  delay(20);

}

void color ()
{
  // Routine that reads the value of the colors
  digitalWrite (s2, LOW);
  digitalWrite (s3, LOW);
  // Count OUT, Pred, RED
  redValue = pulseIn (out, digitalRead (out) == HIGH ? LOW : HIGH);
  digitalWrite (s3, HIGH);
  // Count OUT, pBlue, BLUE
  blueValue = pulseIn (out, digitalRead (out) == HIGH ? LOW : HIGH);
  digitalWrite (s2, HIGH);
  // Count OUT, pGreen, GREEN
  greenValue = pulseIn (out, digitalRead (out) == HIGH ? LOW : HIGH);
}

void setColor(int redColorValue, int greenColorValue, int blueColorValue)
{
#ifdef COMMON_ANODE
  redColorValue = 255 - redColorValue;
  greenColorValue = 255 - greenColorValue;
  blueColorValue = 255 - blueColorValue;
#endif

  analogWrite(redPin, redColorValue);
  analogWrite(greenPin, greenColorValue);
  analogWrite(bluePin, blueColorValue);
}

void showRingLEDColors(int redColorValue, int greenColorValue, int blueColorValue) {
  for (int i = 0; i < NUM_LEDS; i++) {
    leds[i] = CRGB(redColorValue, greenColorValue, blueColorValue);
  }
  FastLED.show();
  delay(delayInterval);
}

