// This example shows how to connect to Cayenne using an ESP8266 and send/receive sample data.
// Make sure you install the ESP8266 Board Package via the Arduino IDE Board Manager and select the correct ESP8266 board before compiling.

//#define CAYENNE_DEBUG
#define CAYENNE_PRINT Serial
#include <CayenneMQTTESP8266.h>

// WiFi network info.
char ssid[] = "Eldric";
char wifiPassword[] = "daomingerdec8";

// Cayenne authentication info. This should be obtained from the Cayenne Dashboard.
char username[] = "032651b0-8d81-11e7-9727-55550d1a07e7";
char password[] = "8d1d37cd312702aeaba4bf701156485c0cea70a0";
char clientID[] = "6091ba10-c553-11e7-98e1-8369df76aa6d";

int receivedCode = 3;

// LEFT MOTOR WHEEL CONTROL PINS
int pin8 = 4;
int pin9 = 5;

// RIGHT MOTOR WHEEL CONTROL PINS
int pin10 = 7;
int pin11 = 8;

void setup() {

  // Initialize the Serial Monitor
  Serial.begin(9600);

  // LEFT WHEEL OUTPUT
  pinMode(pin8, OUTPUT);
  pinMode(pin9, OUTPUT);

  // RIGHT WHEEL OUTPUT
  //  pinMode(pin10, OUTPUT);
  //  pinMode(pin11, OUTPUT);

  Cayenne.begin(username, password, clientID, ssid, wifiPassword);

}

void loop() {
  Cayenne.loop();
}


CAYENNE_IN(0) {
  Serial.println(getValue.asInt());
  receivedCode = getValue.asInt();
  
//     if (receivedCode == 3) {
//      Serial.println("Forward");
//      moveForward();
//    }
//    if (receivedCode == 4) {
//      Serial.println("Backward");
//      moveBackward();
//    }
//  
//    if (receivedCode == 1) {
//      Serial.println("Left");
//      moveLeftward();
//    }
//    if (receivedCode == 2) {
//      Serial.println("Right");
//      moveRightward();
//    }
//    if (receivedCode == 5) {
//      Serial.println("Stop");
//      stopMovement();
//    }

    delay(10);
  
}

// MOVE CAR FORWARD
void moveForward() {
  moveLeftWheelForward();
  moveRightWheelForward();
}

// MOVE CAR BACKWARD
void moveBackward() {
  moveLeftWheelBackward();
  moveRightWheelBackward();
}

// TURN CAR TO THE LEFT
void moveLeftward() {
  haltLeftWheel();
  moveRightWheelForward();
}

// TURN CAR TO THE RIGHT
void moveRightward() {
  haltRightWheel();
  moveLeftWheelForward();
}

// STOP BOTH WHEELS
void stopMovement() {
  haltLeftWheel();
  haltRightWheel();
}

// Move left wheel forward
void moveLeftWheelForward() {
  digitalWrite(pin8, HIGH);
  digitalWrite(pin9, LOW);
}

// Move left wheel backward
void moveLeftWheelBackward() {
  digitalWrite(pin9, HIGH);
  digitalWrite(pin8, LOW);
}

// Move right wheel forward
void moveRightWheelForward() {
  digitalWrite(pin10, HIGH);
  digitalWrite(pin11, LOW);
}

// Move right wheel backward
void moveRightWheelBackward() {
  digitalWrite(pin10, LOW);
  digitalWrite(pin11, HIGH);
}

// Stop left wheel
void haltLeftWheel() {
  digitalWrite(pin8, LOW);
  digitalWrite(pin9, LOW);
}

// Stop right wheel
void haltRightWheel() {
  digitalWrite(pin10, LOW);
  digitalWrite(pin11, LOW);
}


