// This example shows how to connect to Cayenne using an ESP8266 and send/receive sample data.
// Make sure you install the ESP8266 Board Package via the Arduino IDE Board Manager and select the correct ESP8266 board before compiling.

//#define CAYENNE_DEBUG
#define CAYENNE_PRINT Serial
#include <CayenneMQTTESP8266.h>

// WiFi network info.
char ssid[] = "Eldric";
char wifiPassword[] = "daomingerdec8";

// Cayenne authentication info. This should be obtained from the Cayenne Dashboard.
char username[] = "032651b0-8d81-11e7-9727-55550d1a07e7";
char password[] = "8d1d37cd312702aeaba4bf701156485c0cea70a0";
char clientID[] = "617947d0-c53b-11e7-b67f-67bba9556416";

void setup() {
  Serial.begin(9600);
  pinMode(14, OUTPUT);
  Cayenne.begin(username, password, clientID, ssid, wifiPassword);

}

void loop() {
  Cayenne.loop();
}

CAYENNE_IN(14) {
  CAYENNE_LOG("CAYENNE_IN_DEFAULT(%u) - %s, %s", request.channel, getValue.getId(), getValue.asString());
}

