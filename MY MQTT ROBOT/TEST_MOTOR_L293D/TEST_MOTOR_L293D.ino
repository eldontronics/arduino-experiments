#include <SoftwareSerial.h>

// SoftwareSerial bluetooth (3, 4); // Arduino D3 is TX on BLE, Arduino D4 is RX in BLE
int receivedCode = 3;

// LEFT MOTOR WHEEL CONTROL PINS
int pin8 = 2;
int pin9 = 3;

// RIGHT MOTOR WHEEL CONTROL PINS
int pin10 = 7;
int pin11 = 8;

void setup() {

  // Initialize the Serial Monitor
  Serial.begin(9600);

  // LEFT WHEEL OUTPUT
  pinMode(pin8, OUTPUT);
  pinMode(pin9, OUTPUT);

  // RIGHT WHEEL OUTPUT
  pinMode(pin10, OUTPUT);
  pinMode(pin11, OUTPUT);

  Serial.println("Waiting for bluetooth command...");

}

void loop() {
  analogWrite(pin8, 1023);
  analogWrite(pin9, 500);

  delay(5);

  //  if (receivedCode == 3) {
  //    Serial.println("Forward");
  //    moveForward();
  //  }
  //  if (receivedCode == 4) {
  //    Serial.println("Backward");
  //    moveForward();
  //  }
  //
  //  if (receivedCode == 1) {
  //    Serial.println("Left");
  //    moveLeftward();
  //  }
  //  if (receivedCode == 2) {
  //    Serial.println("Right");
  //    moveRightward();
  //  }
  //  if (receivedCode == 5) {
  //    Serial.println("Stop");
  //    stopMovement();
  //  }


}

// MOVE CAR FORWARD
void moveForward() {
  moveLeftWheelForward();
  moveRightWheelForward();
}

// MOVE CAR BACKWARD
void moveBackward() {
  moveLeftWheelBackward();
  moveRightWheelBackward();
}

// TURN CAR TO THE LEFT
void moveLeftward() {
  haltLeftWheel();
  moveRightWheelForward();
}

// TURN CAR TO THE RIGHT
void moveRightward() {
  haltRightWheel();
  moveLeftWheelForward();
}

// STOP BOTH WHEELS
void stopMovement() {
  haltLeftWheel();
  haltRightWheel();
}

// Move left wheel forward
void moveLeftWheelForward() {
  digitalWrite(pin8, HIGH);
  digitalWrite(pin9, LOW);
}

// Move left wheel backward
void moveLeftWheelBackward() {
  digitalWrite(pin9, HIGH);
  digitalWrite(pin8, LOW);
}

// Move right wheel forward
void moveRightWheelForward() {
  digitalWrite(pin10, HIGH);
  digitalWrite(pin11, LOW);
}

// Move right wheel backward
void moveRightWheelBackward() {
  digitalWrite(pin10, LOW);
  digitalWrite(pin11, HIGH);
}

// Stop left wheel
void haltLeftWheel() {
  digitalWrite(pin8, LOW);
  digitalWrite(pin9, LOW);
}

// Stop right wheel
void haltRightWheel() {
  digitalWrite(pin10, LOW);
  digitalWrite(pin11, LOW);
}


