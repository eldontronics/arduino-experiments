/*
 * IRremote: IRrecvDemo - demonstrates receiving IR codes with IRrecv
 * An IR detector/demodulator must be connected to the input RECV_PIN.
 * Version 0.1 July, 2009
 * Copyright 2009 Ken Shirriff
 * http://arcfn.com
 */
#include <LiquidCrystal_I2C.h>
#include <IRremote.h>


// GND is -
// VCC + (middle pin)
// S is atteched to Pin 11 on Arduino
int RECV_PIN = 11;



IRrecv irrecv(RECV_PIN);
LiquidCrystal_I2C lcd(0x3F,16,2);

decode_results results;

void setup()
{
  Serial.begin(9600);
  lcd.init(); // initialize the lcd 
  irrecv.enableIRIn(); // Start the receiver

  lcd.backlight();
  lcd.home();
  lcd.setCursor(0, 0);
  lcd.print("Waiting for");
  lcd.setCursor(0, 1);
  lcd.print("infrared codes..");

}

void loop() {
  if (irrecv.decode(&results)) {

    if(results.value != 1253111734){
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print("Code:");
      lcd.print(results.value);
      lcd.setCursor(0, 1);
      lcd.print("Protocol:");
      lcd.print(results.decode_type);
      Serial.println(results.value); // , HEX      
      
    }

    irrecv.resume(); // Receive the next value
    
    
  }
  delay(100);
}
