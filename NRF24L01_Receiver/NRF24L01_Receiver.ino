/*
  Arduino Wireless Communication Tutorial
        Example 1 - Receiver Code

  by Dejan Nedelkovski, www.HowToMechatronics.com

  Library: TMRh20/RF24, https://github.com/tmrh20/RF24/
*/
#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>

#include <LiquidCrystal_I2C.h>
LiquidCrystal_I2C lcd(0x3B, 16, 2); // set the LCD address to 0x27 for a 16 chars and 2 line display

RF24 radio(7, 8); // CNS, CE
const byte address[6] = "00001";

String previousData = "";

void setup() {


  lcd.init();
  lcd.noBacklight();
  delay(100);
  lcd.backlight();

  outputToLCD("Initializing", "Receiver...");
  delay(2000);
  Serial.begin(9600);
  radio.begin();
  radio.openReadingPipe(0, address);
  radio.setPALevel(RF24_PA_MIN);
  radio.startListening();
}
void loop() {
  if (radio.available()) {
    char text[32] = "";
    radio.read(&text, sizeof(text));
    previousData = text;

    Serial.print("Received data : ");
    Serial.println(text);

    // Display Data
    outputToLCD("Received Data:", text);
    delay(250);
  }
  else {

//    outputToLCD("Previous data:", previousData);
//     delay(50);
  }
}

void outputToLCD(String rowOneMessage, String rowTwoMessage) {
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print(rowOneMessage);
  lcd.setCursor(0, 1);
  lcd.print(rowTwoMessage);
}

