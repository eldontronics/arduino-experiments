/*
  Software serial multple serial test

  Receives from the hardware serial, sends to software serial.
  Receives from software serial, sends to hardware serial.

  The circuit:
   RX is digital pin 10 (connect to TX of other device)
   TX is digital pin 11 (connect to RX of other device)

  Note:
  Not all pins on the Mega and Mega 2560 support change interrupts,
  so only the following can be used for RX:
  10, 11, 12, 13, 50, 51, 52, 53, 62, 63, 64, 65, 66, 67, 68, 69

  Not all pins on the Leonardo and Micro support change interrupts,
  so only the following can be used for RX:
  8, 9, 10, 11, 14 (MISO), 15 (SCK), 16 (MOSI).

  created back in the mists of time
  modified 25 May 2012
  by Tom Igoe
  based on Mikal Hart's example

  This example code is in the public domain.

*/
#include <SoftwareSerial.h>

#include <Wire.h>
#include "SSD1306Ascii.h"
#include "SSD1306AsciiWire.h"

#define I2C_ADDRESS 0x3C
SSD1306AsciiWire oled;

SoftwareSerial mySerial(8, 9); // RX, TX

int count = 0;

void setup() {
  // Open serial communications and wait for port to open:
  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only

  }

  Wire.begin();
  oled.begin(&Adafruit128x64, I2C_ADDRESS);
  oled.set400kHz();
  oled.setFont(Adafruit5x7);

  oled.clear();
  oled.print("Module now ready.\nSend some AT commands\nfrom the Serial monitor.");
  Serial.println("Send SIM800L AT Command:");
  delay(2000);
  oled.clear();

  // set the data rate for the SoftwareSerial port
  mySerial.begin(9600);


  mySerial.write("AT\r\n");
  mySerial.write("AT+CMGF=1\r\n");
  mySerial.write("AT+CMGF=1\r\n");
  
  
}


void loop() {



  while (mySerial.available()) {


    char ch = (char)mySerial.read();

    if (ch == '\n') {
      count++;
      Serial.println(count);
    }

    if (count > 8) {
      delay(2000);
      oled.clear();
      count = 0;
    }


    oled.print(ch);
    Serial.print(ch);
    delay(30);

    if (ch == '~') {
      oled.clear();
      Serial.println("OLED Cleared..!");
    }
  }

  while (Serial.available()) {
    mySerial.write(Serial.read());
  }

}
