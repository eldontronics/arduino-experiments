/**
   Module pinout to ESP32
   ----------------------

   LCD 20x04 Display
      SCL - Connect to Pin D22
      SDA - Connect to Pin D21
      VCC - Connect to Pin VIN
      GND - Connect to Pin GND

   0.96 80x160 OLED Display
      GND - Connect to Pin GND
      VCC - Connect to Pin VIN
      SCL - Connect to Pin D18
      SDA - Connect to Pin D23
      RES - Connect to Pin D13
      DC  - Connect to Pin D12
      CS  - Connect to Pin D14
      BLK - Not connected

   0.96 64x128 OLED Display
      SCL - Connect to Pin D22
      SDA - Connect to Pin D21
      VCC - Connect to Pin VIN
      GND - Connect to Pin GND

   SC Card Modile
      GND - Not connected
      3V3 - Not connected
      5V  - Connect to Pin VIN
      CS  - Connect to Pin D27
      MOSI- Connect to Pin D23
      SCK - Connect to Pin D18
      MISO- Connect to Pin D19
      GND - Connect to Pin GND

   Next News Source Button
      One leg connected to Pin VIN (right)
      One leg connected to Pin D35 <-> 10K Resistor <-> Pin GND

   Reset Button
      One leg connected to Pin VIN (right)
      One leg connected to Pin D34 <-> 10K Resistor <-> Pin GND

   Next News Item Button
      One leg connected to Pin VIN (right)
      One leg connected to Pin D32 <-> 10K Resistor <-> Pin GND


 **/

#include <LiquidCrystal_I2C.h>
#include <SD.h>
#include <SPI.h>
#include <WiFi.h>
#include <WiFiMulti.h>
#include <HTTPClient.h>
#include <Wire.h>
#include <ACROBOTIC_SSD1306.h>

#include "LCD_Driver.h"
#include "GUI_Paint.h"
#include "image.h"

#define BUFFPIXEL   64

WiFiMulti wifiMulti;
LiquidCrystal_I2C lcd(0x3F, 20, 4);

struct Button {
  const uint8_t PIN;
  uint32_t numberKeyPresses;
  bool pressed;
};

struct PubDateTime {
  String Date;
  String Time;
};

Button sourceButton   = {35, 0, false};
Button titleButton    = {32, 0, false};
Button restartButton  = {34, 0, false};

PubDateTime newsPubDateTime;

bool isLogoAlreadyShown = false;

void isrRestart()
{

  static unsigned long previousInterruptTime = 0;
  unsigned long currentInterruptTime = millis();

  if (currentInterruptTime - previousInterruptTime > 200)
  {
    restartButton.pressed = true;
  }
  previousInterruptTime = currentInterruptTime;
}

void isrSource()
{

  static unsigned long previousInterruptTime = 0;
  unsigned long currentInterruptTime = millis();

  if (currentInterruptTime - previousInterruptTime > 200)
  {
    sourceButton.pressed = true;
  }
  previousInterruptTime = currentInterruptTime;
}


void isrTitle()
{
  static unsigned long previousInterruptTime = 0;
  unsigned long currentInterruptTime = millis();

  if (currentInterruptTime - previousInterruptTime > 200)
  {
    titleButton.pressed = true;
  }
  previousInterruptTime = currentInterruptTime;
}

String rssNewsFeedURL[5] = {
  "https://www.inquirer.net/fullfeed",
  "https://www.philstar.com/rss/headlines",
  "http://manilastandard.net/feed/news",
  "https://www.manilatimes.net/rss-feed/",
  "http://www.senate.gov.ph/rss/rss_news.aspx"
};

String rssNewsImage[5] = {
  "/inquirer.bmp",
  "/philstar.bmp",
  "/manilast.bmp",
  "/manilatm.bmp",
  "/senaterp.bmp"
};


String rssURL = "";
const char* ca = \
                 "-----BEGIN CERTIFICATE-----\n" \
                 "MIIEkjCCA3qgAwIBAgIQCgFBQgAAAVOFc2oLheynCDANBgkqhkiG9w0BAQsFADA/\n" \
                 "MSQwIgYDVQQKExtEaWdpdGFsIFNpZ25hdHVyZSBUcnVzdCBDby4xFzAVBgNVBAMT\n" \
                 "DkRTVCBSb290IENBIFgzMB4XDTE2MDMxNzE2NDA0NloXDTIxMDMxNzE2NDA0Nlow\n" \
                 "SjELMAkGA1UEBhMCVVMxFjAUBgNVBAoTDUxldCdzIEVuY3J5cHQxIzAhBgNVBAMT\n" \
                 "GkxldCdzIEVuY3J5cHQgQXV0aG9yaXR5IFgzMIIBIjANBgkqhkiG9w0BAQEFAAOC\n" \
                 "AQ8AMIIBCgKCAQEAnNMM8FrlLke3cl03g7NoYzDq1zUmGSXhvb418XCSL7e4S0EF\n" \
                 "q6meNQhY7LEqxGiHC6PjdeTm86dicbp5gWAf15Gan/PQeGdxyGkOlZHP/uaZ6WA8\n" \
                 "SMx+yk13EiSdRxta67nsHjcAHJyse6cF6s5K671B5TaYucv9bTyWaN8jKkKQDIZ0\n" \
                 "Z8h/pZq4UmEUEz9l6YKHy9v6Dlb2honzhT+Xhq+w3Brvaw2VFn3EK6BlspkENnWA\n" \
                 "a6xK8xuQSXgvopZPKiAlKQTGdMDQMc2PMTiVFrqoM7hD8bEfwzB/onkxEz0tNvjj\n" \
                 "/PIzark5McWvxI0NHWQWM6r6hCm21AvA2H3DkwIDAQABo4IBfTCCAXkwEgYDVR0T\n" \
                 "AQH/BAgwBgEB/wIBADAOBgNVHQ8BAf8EBAMCAYYwfwYIKwYBBQUHAQEEczBxMDIG\n" \
                 "CCsGAQUFBzABhiZodHRwOi8vaXNyZy50cnVzdGlkLm9jc3AuaWRlbnRydXN0LmNv\n" \
                 "bTA7BggrBgEFBQcwAoYvaHR0cDovL2FwcHMuaWRlbnRydXN0LmNvbS9yb290cy9k\n" \
                 "c3Ryb290Y2F4My5wN2MwHwYDVR0jBBgwFoAUxKexpHsscfrb4UuQdf/EFWCFiRAw\n" \
                 "VAYDVR0gBE0wSzAIBgZngQwBAgEwPwYLKwYBBAGC3xMBAQEwMDAuBggrBgEFBQcC\n" \
                 "ARYiaHR0cDovL2Nwcy5yb290LXgxLmxldHNlbmNyeXB0Lm9yZzA8BgNVHR8ENTAz\n" \
                 "MDGgL6AthitodHRwOi8vY3JsLmlkZW50cnVzdC5jb20vRFNUUk9PVENBWDNDUkwu\n" \
                 "Y3JsMB0GA1UdDgQWBBSoSmpjBH3duubRObemRWXv86jsoTANBgkqhkiG9w0BAQsF\n" \
                 "AAOCAQEA3TPXEfNjWDjdGBX7CVW+dla5cEilaUcne8IkCJLxWh9KEik3JHRRHGJo\n" \
                 "uM2VcGfl96S8TihRzZvoroed6ti6WqEBmtzw3Wodatg+VyOeph4EYpr/1wXKtx8/\n" \
                 "wApIvJSwtmVi4MFU5aMqrSDE6ea73Mj2tcMyo5jMd6jmeWUHK8so/joWUoHOUgwu\n" \
                 "X4Po1QYz+3dszkDqMp4fklxBwXRsW10KXzPMTZ+sOPAveyxindmjkW8lGy+QsRlG\n" \
                 "PfZ+G6Z6h7mjem0Y+iWlkYcV4PIWL1iwBi8saCbGS5jN2p8M+X+Q7UNKEkROb3N6\n" \
                 "KOqkqm57TH2H3eDJAkSnh6/DNFu0Qg==\n" \
                 "-----END CERTIFICATE-----\n";


void setup()
{
  Serial.begin(115200);
  pinMode(sourceButton.PIN, INPUT_PULLUP);
  pinMode(titleButton.PIN, INPUT_PULLUP);
  pinMode(restartButton.PIN, INPUT_PULLUP);

  attachInterrupt(sourceButton.PIN, isrSource, RISING);
  attachInterrupt(titleButton.PIN, isrTitle, RISING);
  attachInterrupt(restartButton.PIN, isrRestart, RISING);

  lcd.init();
  lcd.backlight();
  lcd.clear();

  Wire.begin();
  oled.init();                      // Initialze SSD1306 OLED display
  oled.clearDisplay();              // Clear screen
  oled.setTextXY(0, 0);             // Set cursor position, start of line 0
  oled.putString("ELDRIC MARCUS");
  oled.setTextXY(1, 0);             // Set cursor position, start of line 1
  oled.putString("ESP32 NewsTicker");

  Config_Init();
  LCD_Init();
  LCD_Clear(BLACK);
  LCD_SetBacklight(100);

  Paint_NewImage(LCD_WIDTH, LCD_HEIGHT, 0, BLACK);
  Paint_Clear(BLACK);
  Paint_SetRotate(180);

  Serial.print("Wait");
  for (uint8_t t = 4; t > 0; t--) {
    Serial.printf(".");
    Serial.flush();
    delay(500);
  }

  Serial.println();

  Paint_DrawString_EN(0, 0, "Initializing SD card...", &Font8, BLACK, GREEN);

  if (!SD.begin(DEV_SD_CS)) {
    Paint_DrawString_EN(0, 10, "SD Card Failed.", &Font8, BLACK, RED);
    return;
  }

  // Reading wifi password creds
  oled.clearDisplay();              // Clear screen
  oled.setTextXY(0, 0);             // Set cursor position, start of line 0
  oled.putString("Fetcing wifi credentials...");

  oled.setTextXY(2, 0);
  if (SD.exists("/wificred.txt")) {
    oled.putString("Reading file...");

    File dataFile = SD.open("/wificred.txt");
    String creds = "";

    String credsArr[2];
    int arrIndex = 0;
    // if the file is available, write to it:
    if (dataFile)
    {
      while (dataFile.available())
      {
        creds = creds + (char)dataFile.read();       
      }
      dataFile.close();
    }

    oled.setTextXY(3, 0);
    // Serial.println(creds);
    oled.putString(creds);



  } else {
    oled.putString("File not found!");
  }

  Serial.println();

  Paint_DrawString_EN(0, 10, "SD Card Initialization OK.", &Font8, BLACK, BLUE);
  delay(1000);

  wifiMulti.addAP("Eldric", "daomingerdec8");
  Paint_DrawString_EN(0, 0, "Connected",  &Font24, BLACK, WHITE);
  Paint_DrawString_EN(0, 20, "Please wait...", &Font12, BLACK, BLUE);
  lcd.setCursor(0, 0);
  lcd.print("Fetching news...");

}


void loop() {

  for (int rssURLIndex = 0; rssURLIndex < 5; rssURLIndex++) {
    isLogoAlreadyShown = false;

    String line = "";

    rssURL = rssNewsFeedURL[rssURLIndex];

    if ((wifiMulti.run() == WL_CONNECTED)) {

      HTTPClient http;

      if (rssURL.indexOf("https://") >= 0) {
        http.begin(rssURL, ca); //HTTPS
      }
      else {
        http.begin(rssURL); //HTTP
      }

      int httpCode = http.GET();

      // httpCode will be negative on error
      if (httpCode > 0) {
        if (httpCode == HTTP_CODE_OK) {
          String payload = http.getString();
          line = payload;
          //Serialprintln(payload);
        }
      } else {
        Serial.printf("[HTTP] GET... failed, error: %s\n", http.errorToString(httpCode).c_str());
        http.end();
        return;
      }

      http.end();

      line.trim();

      // GET the source here:
      int startSourceIndex = line.indexOf("<title>");
      int endSourceIndex = line.indexOf("</title>") + String("</title").length() + 1;

      String sourceHeader = (line.substring(startSourceIndex, endSourceIndex));

      Serial.print("Source : ");

      // Replace some characters here:
      sourceHeader.replace("<![CDATA[", "'");
      sourceHeader.replace("]]>", "'");
      sourceHeader.replace("<title>", "");
      sourceHeader.replace("</title>", "");
      sourceHeader.replace("&gt;", ">");
      sourceHeader.replace("&mdash;", "-");
      sourceHeader.replace("&#x2018;", "'");
      sourceHeader.replace("&lsquo;", "'");
      sourceHeader.replace("&#x2019;", "'");
      sourceHeader.replace("&#8217;", "'");
      sourceHeader.replace("&#8216;", "'");
      sourceHeader.replace("&rsquo;", "'");
      sourceHeader.replace("-", "");
      sourceHeader.replace("–", "-");
      sourceHeader.replace("—", "-");
      sourceHeader.replace("'", "");
      sourceHeader.replace("  ", " ");
      sourceHeader.replace("&amp;", "&");
      sourceHeader.replace("&#241;", "ñ");
      sourceHeader.trim();

      Serial.println(sourceHeader);
      Serial.println();

      // Remove excess data up to the first <item> tag.
      line = line.substring(line.indexOf("<item>"));
      int startIndex = -1;
      int endIndex = -1;



      while (line.length() > 0) {

        if (!isLogoAlreadyShown) {
          // Display the logo of the source news
          isLogoAlreadyShown = true;
          Paint_Clear(BLACK);
          displayImage(rssNewsImage[rssURLIndex]);
        }

        startIndex = line.indexOf("<item>");
        endIndex = line.indexOf("</item>") + String("</item>").length() + 1;

        if (startIndex >= 0 && endIndex >= 0) {

          int startTitle = -1;
          int endTitle = -1;

          int pubDateTimeStart =  line.indexOf("<pubDate>") +  String("<pubDate>").length();
          int pubDateTimeEnd = line.indexOf("</pubDate>");

          Serial.println(line.substring(pubDateTimeStart, pubDateTimeEnd));
          GetPubDateTime(line.substring(pubDateTimeStart, pubDateTimeEnd));

          startTitle = line.indexOf("<title>");
          endTitle = line.indexOf("</title>");

          String newsRawTitle = line.substring(startTitle, endTitle);

          newsRawTitle.replace("<title><![CDATA[", "");
          newsRawTitle.replace("]]>", "");
          newsRawTitle.replace("<title>", "");

          // Replace some characters here:
          newsRawTitle.replace("&mdash;", "-");
          newsRawTitle.replace("&#x2018;", "'");
          newsRawTitle.replace("&lsquo;", "'");
          newsRawTitle.replace("&#x2019;", "'");
          newsRawTitle.replace("&rsquo;", "'");
          newsRawTitle.replace("&#8217;", "'");
          newsRawTitle.replace("&#8216;", "'");
          newsRawTitle.replace("&#8212;", "-");
          newsRawTitle.replace("&#8211;", "-");
          newsRawTitle.replace("‘", "'");
          newsRawTitle.replace("’", "'");
          newsRawTitle.replace("&amp;", "&");
          newsRawTitle.replace("&quot;", "\"");
          newsRawTitle.replace("&#039;", "'");
          newsRawTitle.replace("&#8230;", "...");
          newsRawTitle.replace("—", "-");
          newsRawTitle.replace("–", "-");
          newsRawTitle.replace("&#241;", "ñ");

          Serial.println(newsRawTitle);

          rssURL.replace("https://", "");
          rssURL.replace("http://", "");
          rssURL.replace("www.", "");
          rssURL = rssURL.substring(0, rssURL.indexOf("/"));
          rssURL.toUpperCase();

          oled.clearDisplay();
          oled.setTextXY(0, 0);
          oled.putString("News Date/Time");
          oled.setTextXY(2, 0);
          oled.putString(newsPubDateTime.Date);
          oled.setTextXY(3, 0);
          oled.putString(newsPubDateTime.Time);

          lcd.clear();
          lcd.setCursor(0, 0);
          showNewsItemLCD(newsRawTitle);

          // Exit from scrollText from loop method
          if (sourceButton.pressed) {
            sourceButton.pressed = false;
            break;
          }

          line = line.substring(endIndex - 1);
        }
        else {
          line = "";
        }
      }
    }
  }
}

void displayNews(String headerTitle, String headerDetail, PubDateTime pubDateTime) {
  char __newsSource[100];
  headerTitle.toCharArray(__newsSource, sizeof(__newsSource));
  char __newsItem[100];
  headerDetail.toCharArray(__newsItem, sizeof(__newsItem));

  Paint_DrawString_EN(0, 0, __newsSource, &Font12, BLACK, BLUE);
  Paint_DrawString_EN(0, 10, __newsItem, &Font12, BLACK, WHITE);

  for (int i = 1; i <= 100; i++) {

    if (titleButton.pressed) {
      Serial.println("Change news item...");
      Paint_Clear(BLACK);
      Paint_DrawString_EN(0, 0, "Changing", &Font12, BLACK, BLUE);
      Paint_DrawString_EN(0, 20, "News", &Font24, BLACK, WHITE);
      Paint_DrawString_EN(0, 40, "Item...", &Font24, BLACK, WHITE);

      delay(100);
      titleButton.pressed = false;
      return;
    }

    if (sourceButton.pressed) {
      Paint_Clear(BLACK);
      Paint_DrawString_EN(0, 0, "Changing", &Font12, BLACK, BLUE);
      Paint_DrawString_EN(0, 20, "News", &Font24, BLACK, WHITE);
      Paint_DrawString_EN(0, 40, "Source...", &Font24, BLACK, WHITE);
      delay(100);
      return;
    }

    if (restartButton.pressed) {
      Paint_Clear(BLACK);
      Paint_DrawString_EN(0, 0,  "Restarting", &Font12, BLACK, RED);
      Paint_DrawString_EN(0, 20, "ESP32...", &Font24, BLACK, RED);
      delay(2000);
      restartButton.pressed = false;
      ESP.restart();
    }

    delay(100);
  }
}

void displayImage(String Imgilename) {
  // Draw the image pixel by pixel starting from 0,0
  bmpDraw(Imgilename, 0, 0);
}


void GetPubDateTime(String dateTime) {
  // Split the datetime value to date and time respectively. 4th space splits the two
  int indx = -1;

  int cnt = 0;
  for (int i = 0; i < dateTime.length(); i++) {
    if (dateTime.charAt(i) == ' ') {
      cnt++;
    }

    if (cnt == 4) {
      indx = i;
      break;
    }

  }

  newsPubDateTime.Date = dateTime.substring(0, indx);
  newsPubDateTime.Time = dateTime.substring(indx + 1);

}

void bmpDraw(String filename, uint8_t x, uint16_t y) {

  File     bmpFile;
  int      bmpWidth, bmpHeight;   // W+H in pixels
  uint8_t  bmpDepth;              // Bit depth (currently must be 24)
  uint32_t bmpImageoffset;        // Start of image data in file
  uint32_t rowSize;               // Not always = bmpWidth; may have padding
  uint8_t  sdbuffer[3 * BUFFPIXEL]; // pixel buffer (R+G+B per pixel)
  uint8_t  buffidx = sizeof(sdbuffer); // Current position in sdbuffer
  boolean  goodBmp = false;       // Set to true on valid header parse
  boolean  flip    = true;        // BMP is stored bottom-to-top
  int      w, h, row, col;
  uint8_t  r, g, b;
  uint32_t pos = 0, startTime = millis();

  //  if ((x >= tft.width()) || (y >= tft.height())) return;

  Serial.println();
  Serial.print(F("Loading image '"));
  Serial.print(filename);
  Serial.println('\'');

  // Open requested file on SD card
  if ((bmpFile = SD.open(filename)) == NULL) {
    Serial.print(F("File not found"));
    return;
  }

  // Parse BMP header
  if (read16(bmpFile) == 0x4D42) { // BMP signature
    Serial.print(F("File size: ")); Serial.println(read32(bmpFile));
    (void)read32(bmpFile); // Read & ignore creator bytes
    bmpImageoffset = read32(bmpFile); // Start of image data
    Serial.print(F("Image Offset: ")); Serial.println(bmpImageoffset, DEC);
    // Read DIB header
    Serial.print(F("Header size: ")); Serial.println(read32(bmpFile));

    bmpWidth  = read32(bmpFile);
    bmpHeight = read32(bmpFile);


    if (read16(bmpFile) == 1) { // # planes -- must be '1'
      bmpDepth = read16(bmpFile); // bits per pixel
      Serial.print(F("Bit Depth: ")); Serial.println(bmpDepth);
      if ((bmpDepth == 24) && (read32(bmpFile) == 0)) { // 0 = uncompressed

        goodBmp = true; // Supported BMP format -- proceed!
        Serial.print(F("Image size: "));
        Serial.print(bmpWidth);
        Serial.print('x');
        Serial.println(bmpHeight);

        // BMP rows are padded (if needed) to 4-byte boundary
        rowSize = (bmpWidth * 3 + 3) & ~3;

        // If bmpHeight is negative, image is in top-down order.
        // This is not canon but has been observed in the wild.
        if (bmpHeight < 0) {
          bmpHeight = -bmpHeight;
          flip      = false;
        }

        // Crop area to be loaded
        w = bmpWidth;
        h = bmpHeight;

        for (row = 0; row < h; row++) { // For each scanline...

          if (flip) {
            pos = bmpImageoffset + (bmpHeight - 1 - row) * rowSize;
          }
          else {
            pos = bmpImageoffset + row * rowSize;
          }
          if (bmpFile.position() != pos) { // Need seek?
            bmpFile.seek(pos);
            buffidx = sizeof(sdbuffer); // Force buffer reload
          }

          for (col = 0; col < w; col++) { // For each pixel...
            // Time to read more pixel data?
            if (buffidx >= sizeof(sdbuffer)) { // Indeed
              bmpFile.read(sdbuffer, sizeof(sdbuffer));
              buffidx = 0; // Set index to beginning
            }

            // Convert pixel from BMP to TFT format, push to display
            b = sdbuffer[buffidx++];
            g = sdbuffer[buffidx++];
            r = sdbuffer[buffidx++];

            // I added this as well to render the correct color onto the display
            // - Eldon
            uint16_t pixelColor = (((31 * (r + 4)) / 255) << 11) |
                                  (((63 * (g + 2)) / 255) << 5) |
                                  ((31 * (b + 4)) / 255);

            // Switch to get the correct orientation
            // - Eldon
            Paint_SetPixel(y + col, x + row, pixelColor);

          } // end pixel
        } // end scanline

        Serial.print(F("Loaded in "));
        Serial.print(millis() - startTime);
        Serial.println(" ms");
      } // end goodBmp

      Serial.print(r); Serial.print(",");
      Serial.print(g); Serial.print(",");
      Serial.println(b);
    }
  }

  bmpFile.close();
  if (!goodBmp) Serial.println(F("BMP format not recognized."));
}


// These read 16- and 32-bit types from the SD card file.
// BMP data is stored little-endian, Arduino is little-endian too.
// May need to reverse subscript order if porting elsewhere.

uint16_t read16(File f) {
  uint16_t result;
  ((uint8_t *)&result)[0] = f.read(); // LSB
  ((uint8_t *)&result)[1] = f.read(); // MSB
  return result;
}

uint32_t read32(File f) {
  uint32_t result;
  ((uint8_t *)&result)[0] = f.read(); // LSB
  ((uint8_t *)&result)[1] = f.read();
  ((uint8_t *)&result)[2] = f.read();
  ((uint8_t *)&result)[3] = f.read(); // MSB
  return result;
}


void printDirectory(File dir, int numTabs) {
  while (true) {

    File entry =  dir.openNextFile();
    if (! entry) {
      // no more files
      break;
    }
    for (uint8_t i = 0; i < numTabs; i++) {
      Serial.print('\t');
    }
    Serial.print(entry.name());
    if (entry.isDirectory()) {
      Serial.println("/");
      printDirectory(entry, numTabs + 1);
    } else {
      // files have sizes, directories do not
      Serial.print("\t\t");
      Serial.println(entry.size(), DEC);
    }
    entry.close();
  }
}


void showNewsItemLCD(String newsItem) {
  String lcdNews1 = newsItem.substring(0, 20);
  String lcdNews2 = newsItem.substring(20, 40);
  String lcdNews3 = newsItem.substring(40, 60);
  String lcdNews4 = newsItem.substring(60, 80);

  lcdNews1.trim();
  lcdNews2.trim();
  lcdNews3.trim();
  lcdNews4.trim();

  lcd.setCursor(0, 0);
  lcd.print(lcdNews1);
  lcd.setCursor(0, 1);
  lcd.print(lcdNews2);
  lcd.setCursor(0, 2);
  lcd.print(lcdNews3);
  lcd.setCursor(0, 3);
  lcd.print(lcdNews4);

  for (int i = 1; i <= 100; i++) {

    if (titleButton.pressed) {
      isLogoAlreadyShown = false;
      Serial.println("Change news item...");
      Paint_Clear(BLACK);
      Paint_DrawString_EN(0, 0, "Changing", &Font12, BLACK, BLUE);
      Paint_DrawString_EN(0, 20, "News", &Font24, BLACK, WHITE);
      Paint_DrawString_EN(0, 40, "Item...", &Font24, BLACK, WHITE);

      delay(100);
      titleButton.pressed = false;
      return;
    }

    if (sourceButton.pressed) {
      Paint_Clear(BLACK);
      Paint_DrawString_EN(0, 0, "Changing", &Font12, BLACK, BLUE);
      Paint_DrawString_EN(0, 20, "News", &Font24, BLACK, WHITE);
      Paint_DrawString_EN(0, 40, "Source...", &Font24, BLACK, WHITE);
      delay(100);
      return;
    }

    if (restartButton.pressed) {
      Paint_Clear(BLACK);
      Paint_DrawString_EN(0, 0,  "Restarting", &Font12, BLACK, RED);
      Paint_DrawString_EN(0, 20, "ESP32...", &Font24, BLACK, RED);
      delay(2000);
      restartButton.pressed = false;
      ESP.restart();
    }

    delay(100);
  }

}
