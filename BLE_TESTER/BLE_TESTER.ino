#include <SoftwareSerial.h>

SoftwareSerial bluetooth (3, 4); // D3 is TX on BLE, D4 is RX in BLE

String receivedBluetoothString = "";
boolean commandFinished = false;

void setup() {
  bluetooth.begin(9600);
  Serial.begin(9600);
  pinMode(13, OUTPUT);
  Serial.println("Waiting for code...");
}

void loop() {

  while (bluetooth.available() > 0) {
    char receivedBluetoothChar = bluetooth.read();
    if (receivedBluetoothChar != '\n') {
      receivedBluetoothString += receivedBluetoothChar;
    }
    delay(10); // 10

  }

  if (receivedBluetoothString != "") {
    if (receivedBluetoothString == "CLEAR") {
      Serial.println("Waiting for code...");
    }
    else {

      if (receivedBluetoothString.indexOf("Red") > 0) {
        // assign Red Value
        receivedBluetoothString.remove(0, 3);
        Serial.print("Red Value : ");
        Serial.println(receivedBluetoothString);
        bluetooth.write("So sent Red");

      }

      if (receivedBluetoothString.indexOf("Green") > 0) {
        // assign Green Value
        receivedBluetoothString.remove(0, 5);
        Serial.print("Green Value : ");
        Serial.println(receivedBluetoothString);
        bluetooth.write("So sent Green");
      }

      if (receivedBluetoothString.indexOf("Blue") > 0) {
        // assign Blue Value
        receivedBluetoothString.remove(0, 4);
        Serial.print("Blue Value : ");
        Serial.println(receivedBluetoothString);
        bluetooth.write("So sent Blue");
      }


      Serial.println(receivedBluetoothString);

      if (receivedBluetoothString == "LEDON") {
        digitalWrite(13, HIGH);
      }
      else if (receivedBluetoothString == "LEDOFF") {
        digitalWrite(13, LOW);
      }


    }
    receivedBluetoothString = "";
  }

}
