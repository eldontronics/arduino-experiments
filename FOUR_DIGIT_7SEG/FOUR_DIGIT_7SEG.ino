#include <Arduino.h>
#include <TM1637Display.h>

int analogPin = A0;
int ledPIN = 11;
int minute = 0;
int second = -1;

// Module connection pins (Digital Pins)
#define CLK 2
#define DIO 3

// The amount of time (in milliseconds) between tests
#define TEST_DELAY   2000

const uint8_t SEG_DONE[] = {
  SEG_B | SEG_C | SEG_D | SEG_E | SEG_G,           // d
  SEG_A | SEG_B | SEG_C | SEG_D | SEG_E | SEG_F,   // O
  SEG_C | SEG_E | SEG_G,                           // n
  SEG_A | SEG_D | SEG_E | SEG_F | SEG_G            // E
};

const uint8_t SEG_ALL[] = {
  SEG_A | SEG_B | SEG_C | SEG_D | SEG_E | SEG_F | SEG_G,
  SEG_A | SEG_B | SEG_C | SEG_D | SEG_E | SEG_F | SEG_G,
  SEG_A | SEG_B | SEG_C | SEG_D | SEG_E | SEG_F | SEG_G,
  SEG_A | SEG_B | SEG_C | SEG_D | SEG_E | SEG_F | SEG_G
};

const uint8_t SEG_HELP[] = {
  SEG_B | SEG_C | SEG_E | SEG_F | SEG_G,
  SEG_A | SEG_D | SEG_E | SEG_F | SEG_G,
  SEG_D | SEG_E | SEG_F,
  SEG_A | SEG_B | SEG_E | SEG_F | SEG_G
};

TM1637Display display(CLK, DIO);

void setup()
{
  display.setBrightness(7);
  Serial.begin(9600);
  display.showNumberDec(0143, false, 4, 0); // number to be shown, show leading 0s, start, least significant (to show correctly, use 4,0)

}

void loop()
{

  //    digitalWrite(ledPIN, HIGH);
  //    for(int value = 0; value <= 9999; value++){
  //      display.showNumberDec(value, false, 4, 0); // number to be shown, show leading 0s, start, least significant (to show correctly, use 4,0)
  //      delay(10);
  //    }
  //

  //  for(int sec = 0; sec <=59; sec++){
  //      display.showNumberDec(sec, true, 2, 2); // number to be shown, show leading 0s, start, least significant (to show correctly, use 4,0)
  //      delay(1000);
  //  }
  //
  //      display.showNumberDec(20, true, 2, 0); // number to be shown, show leading 0s, start, least significant (to show correctly, use 4,0)
  //      display.showNumberDec(17, true, 2, 2); // number to be shown, show leading 0s, start, least significant (to show correctly, use 4,0)

}
