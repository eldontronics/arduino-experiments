/*********
  Rui Santos
  Complete project details at http://randomnerdtutorials.com
*********/

#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>

MDNSResponder mdns;

// Replace with your network credentials
const char* ssid = "Eldric";
const char* password = "daomingerdec8";

ESP8266WebServer server(80);

String webPage = "";

int redLED = D5;
int blueLED = D6;
int greenLED = D7;

void setup(void) {
  webPage += "<h1  style=\"font-family:Arial;\">NodeMCU Web Server</h1>";
  webPage += "<p style=\"font-family:Arial;\">Red LED    <a href=\"RedLEDOn\"><button>ON</button></a>&nbsp;<a href=\"RedLEDOff\"><button>OFF</button></a></p>";
  webPage += "<p style=\"font-family:Arial;\">Blue LED   <a href=\"BlueLEDOn\"><button>ON</button></a>&nbsp;<a href=\"BlueLEDOff\"><button>OFF</button></a></p>";
  webPage += "<p style=\"font-family:Arial;\">Green LED  <a href=\"GreenLEDOn\"><button>ON</button></a>&nbsp;<a href=\"GreenLEDOff\"><button>OFF</button></a></p>";

  // preparing GPIOs
  pinMode(redLED, OUTPUT);
  pinMode(blueLED, OUTPUT);
  pinMode(greenLED, OUTPUT);

  digitalWrite(redLED, LOW);
  digitalWrite(blueLED, LOW);
  digitalWrite(greenLED, LOW);

  delay(1000);
  Serial.begin(115200);
  WiFi.begin(ssid, password);
  Serial.println("");

  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  if (mdns.begin("esp8266", WiFi.localIP())) {
    Serial.println("MDNS responder started");
  }

  server.on("/", []() {
    server.send(200, "text/html", webPage);
  });

  // LEDs
  server.on("/RedLEDOn", []() {
    server.send(200, "text/html", webPage);
    digitalWrite(redLED, HIGH);
    delay(1000);
  });
  server.on("/RedLEDOff", []() {
    server.send(200, "text/html", webPage);
    digitalWrite(redLED, LOW);
    delay(1000);
  });
  server.on("/BlueLEDOn", []() {
    server.send(200, "text/html", webPage);
    digitalWrite(blueLED, HIGH);
    delay(1000);
  });
  server.on("/BlueLEDOff", []() {
    server.send(200, "text/html", webPage);
    digitalWrite(blueLED, LOW);
    delay(1000);
  });
  server.on("/GreenLEDOn", []() {
    server.send(200, "text/html", webPage);
    digitalWrite(greenLED, HIGH);
    delay(1000);
  });
  server.on("/GreenLEDOff", []() {
    server.send(200, "text/html", webPage);
    digitalWrite(greenLED, LOW);
    delay(1000);
  });

  server.begin();
  Serial.println("HTTP server started");
}

void loop(void) {
  server.handleClient();
}
