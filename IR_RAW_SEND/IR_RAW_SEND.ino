/*
 * IRremote: IRsendRawDemo - demonstrates sending IR codes with sendRaw
 * An IR LED must be connected to Arduino PWM pin 3.
 * Version 0.1 July, 2009
 * Copyright 2009 Ken Shirriff
 * http://arcfn.com
 *
 * IRsendRawDemo - added by AnalysIR (via www.AnalysIR.com), 24 August 2015
 *
 * This example shows how to send a RAW signal using the IRremote library.
 * The example signal is actually a 32 bit NEC signal.
 * Remote Control button: LGTV Power On/Off. 
 * Hex Value: 0x20DF10EF, 32 bits
 * 
 * It is more efficient to use the sendNEC function to send NEC signals. 
 * Use of sendRaw here, serves only as an example of using the function.
 * 
 */


#include <IRremote.h>

IRsend irsend;

int LEDPin = 13;

void setup()
{
  Serial.begin(9600);
  pinMode(LEDPin, OUTPUT);
}

void loop() {

  int khz = 60; // 38kHz carrier frequency for the NEC protocol
  unsigned int dataON[]  = {4400, 600, 1600, 600, 500, 600, 1600, 600, 1600, 600, 500, 600, 500, 600, 1550, 650, 450, 650, 450, 650, 1550, 650, 450, 650, 450, 650, 1550, 650, 1550, 600, 500, 600, 1600, 600, 500, 600, 500, 600, 550, 550, 1600, 600, 1600, 600, 1600, 600, 1600, 600, 1600, 600, 1550, 650, 1550, 650, 1550, 650, 450, 650, 450, 650, 450, 600, 500, 600, 500, 600, 500, 600, 1600, 600, 500, 600, 550, 550, 1600, 600, 500, 600, 500, 600, 500, 600, 1600, 600, 500, 600, 1550, 650, 1550, 650, 450, 650, 1550, 650, 1550, 650, 1600, 550}; //AnalysIR Batch Export (IRremote) - RAW
  unsigned int dataOFF[] = {4400, 650, 1550, 650, 450, 600, 1600, 600, 1600, 600, 500, 600, 500, 600, 1600, 600, 500, 600, 500, 600, 1600, 600, 500, 600, 500, 600, 1600, 600, 1550, 650, 450, 650, 1550, 650, 450, 650, 1550, 650, 1550, 600, 1600, 600, 1600, 600, 500, 600, 1600, 600, 1600, 600, 1600, 600, 500, 600, 500, 600, 500, 600, 500, 600, 1550, 650, 500, 600, 450, 650, 1550, 650, 1550, 650, 1550, 650, 450, 600, 550, 550, 500, 600, 500, 600, 500, 600, 500, 600, 500, 600, 500, 600, 1600, 600, 1600, 600, 1600, 600, 1600, 600, 1550, 650}; //AnalysIR Batch Export (IRremote) - RAW

  Serial.println("Turning ON...");
  irsend.sendRaw (dataON, sizeof(dataON), khz);
  Serial.println("Aircon turned on!");
  delay(10000); //In this example, the signal will be repeated every 5 seconds, approximately.

  Serial.println("Turning OFF...");
  irsend.sendRaw (dataOFF, sizeof(dataOFF), khz);
  Serial.println("Aircon turned on!");
  delay(10000); //In this example, the signal will be repeated every 5 seconds, approximately.
}
