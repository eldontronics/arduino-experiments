#include <Servo.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define OLED_RESET 4
Adafruit_SSD1306 display(OLED_RESET);

#define LOGO16_GLCD_HEIGHT 64 
#define LOGO16_GLCD_WIDTH  128 

#if (SSD1306_LCDHEIGHT != 64)
#error("Height incorrect, please fix Adafruit_SSD1306.h!");
#endif

Servo	myServo;
int		servoPin				= 9;
String	receivedData			= "";
bool	isFinishedReceivingData = false;
int		interruptPin			= 2;
int		buttonState				= -1;
int		ledPin					= 13;
int		angle					= 0;
bool	reverseRotation			= false;

void setup() {
	Serial.begin(9600);
	myServo.attach(servoPin);
	myServo.write(0);

	pinMode(interruptPin, INPUT_PULLUP);
	attachInterrupt(digitalPinToInterrupt(interruptPin), buttonPushed, RISING);

	display.begin(SSD1306_SWITCHCAPVCC, 0x3C);							
	display.clearDisplay();
	display.setTextSize(1);
	display.setTextColor(WHITE);
	display.setCursor(0,0);
	display.println("OLED is now ready...");
	display.display();

	pinMode(ledPin, OUTPUT);
}

void loop() {

	if(!reverseRotation){ 
		angle++; 
	}
	else {
		angle--;
	}
	
	if (angle > 180) {
		reverseRotation = true;
	}
	
	if (angle == 0) {
		reverseRotation = false;
	}

	myServo.write(angle);

	display.clearDisplay();
//	display.setTextSize(1);
	display.setCursor(0, 0);
	display.println("Angle: ");
//	display.setTextSize(2);
	display.println(angle);
	display.display();

	

	//while (Serial.available() > 0) {
	//	char incomingByte = (char)(Serial.read());
	//	receivedData += (String) incomingByte;		
	//	delay(10);
	//}

	//if (receivedData != "") {
	//	display.clearDisplay();
	//	display.setCursor(0, 0);
	//	display.println("Angle: ");
	//	display.println(receivedData);
	//	display.display();

	//	myServo.write(receivedData.toInt());
	//	receivedData = "";
	//}

}

void buttonPushed() {
	// Send command to Winform to instruct the WinForm app to trigger face recognition
	// I am using COM6 to write my codes.
	Serial.write("InitializeFR");
}
