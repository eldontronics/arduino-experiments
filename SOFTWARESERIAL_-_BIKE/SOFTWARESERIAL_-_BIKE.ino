#include <SoftwareSerial.h>

SoftwareSerial HM10(3, 2); // RX = 2, TX = 3

char appData;
String inData = "";
String latestCommand = "";

void setup()
{
  Serial.begin(9600);
  Serial.println("HM10 serial started at 9600");
  HM10.begin(9600);
}

void loop()
{


  bool hasData = false;
  String receivedCommand = "";
  HM10.listen();

  while (HM10.available() > 0) {
    appData = HM10.read();
    receivedCommand += "" + (String)appData;
    hasData = true;
  }

  if (hasData) {
    
      if(receivedCommand == "L"){
        latestCommand = "LEFT";
      }

      if(receivedCommand == "R"){
        latestCommand = "RIGHT";
      }

      if(receivedCommand == "B" || receivedCommand == "F"){
        Serial.println("LIGHT OFF");
        latestCommand = "";
      }
      
  }

  Serial.println(latestCommand);
  delay(100);
  








}
