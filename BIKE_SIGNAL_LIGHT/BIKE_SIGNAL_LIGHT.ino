//We always have to include the library
#include "LedControl.h"
#include <SoftwareSerial.h>

SoftwareSerial HM10(3, 2); // RX = 2, TX = 3

#define __TURN_LEFT     0
#define __TURN_RIGHT    1

char appData;
String inData = "";
String latestCommand = "";

/*
  Now we need a LedControl to work with.
 ***** These pin numbers will probably not work with your hardware *****
  pin 13 is connected to the DataIn
  pin 11 is connected to the CLK
  pin 12 is connected to LOAD
  2 - there are two modules to be used.
*/
LedControl lc = LedControl(13, 11, 12, 2);

/* we always wait a bit between updates of the display */
unsigned long delaytime = 100;

void setup() {
  Serial.begin(9600);
  Serial.println("HM10 serial started at 9600");
  HM10.begin(9600);

  /*
    The MAX72XX is in power-saving mode on startup,
    we have to do a wakeup call
  */
  lc.shutdown(0, false);
  lc.shutdown(1, false);
  /* Set the brightness to a medium values */
  lc.setIntensity(0, 15);
  lc.setIntensity(1, 15);
  /* and clear the display */
  lc.clearDisplay(0);
  lc.clearDisplay(1);
}


void loop() {

blinkDirection(0, __TURN_LEFT);


//  bool hasData = false;
//  String receivedCommand = "";
//  HM10.listen();
//
//  while (HM10.available() > 0) {
//    appData = HM10.read();
//    receivedCommand += "" + (String)appData;
//    hasData = true;
//  }
//
//  if (hasData) {
//
//    if (receivedCommand == "L") {
//      latestCommand = "LEFT";
//    }
//
//    if (receivedCommand == "R") {
//      latestCommand = "RIGHT";
//    }
//
//    if (receivedCommand == "B" || receivedCommand == "F") {
//      Serial.println("LIGHT OFF");
//      latestCommand = "";
//    }
//
//  }
//
//  Serial.println(latestCommand);
//  
//  if (latestCommand == "LEFT") {
//    blinkDirection(0, __TURN_LEFT);
//  }
//
//  if (latestCommand == "RIGHT") {
//    blinkDirection(1, __TURN_RIGHT);
//  }
//
//  if (latestCommand == "") {
//    lc.clearDisplay(0);
//    lc.clearDisplay(1);
//  }



}


void blinkDirection(int deviceID, int directionID) {

  byte turnImage[8];

  if (directionID == __TURN_RIGHT) {
    turnImage[0] = B01100110;
    turnImage[1] = B00111100;
    turnImage[2] = B00011000;
    turnImage[3] = B10000001;
    turnImage[4] = B11000011;
    turnImage[5] = B01100110;
    turnImage[6] = B00111100;
    turnImage[7] = B00011000;
  }
  else {
    turnImage[0] = B00011000;
    turnImage[1] = B00111100;
    turnImage[2] = B01100110;
    turnImage[3] = B11000011;
    turnImage[4] = B10000001;
    turnImage[5] = B00011000;
    turnImage[6] = B00111100;
    turnImage[7] = B01100110;
  }

  byte black[8] = { B00000000, B00000000, B00000000, B00000000, B00000000, B00000000, B00000000, B00000000};

  for (int row = 0; row <= 7; row++) {
    lc.setRow(deviceID, row, turnImage[row]);
  }

  delay(delaytime);

  for (int row = 0; row <= 7; row++) {
    lc.setRow(deviceID, row, black[row]);
  }

  delay(delaytime);
}
