#include<avr/io.h>
#include<avr/interrupt.h>

volatile int flag = -1;

ISR(PCINT0_vect) {
  flag = flag * (-1);

  if (flag == 1) {
    digitalWrite(3, HIGH);
  }
  if (flag == -1) {
    digitalWrite(3, LOW);
  }
}

void setup() {
  pinMode(3, OUTPUT);
  GIMSK = 0b00100000;
  PCMSK = 0b00010011;
  sei();
  digitalWrite(3, LOW);
}

void loop() {
  while (1) {

  }
}


