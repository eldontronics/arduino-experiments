#include <AFMotor.h>

AF_DCMotor motor1(1); //1 means M1 in which the DC motor is connected
AF_DCMotor motor2(2); //2 means M2 in which the DC motor is connected

void setup() {
  motor1.setSpeed(255);
  motor1.run(RELEASE);

  motor2.setSpeed(255);
  motor2.run(RELEASE);
}

void loop()
{
  dc();
}

void dc()
{
  

  motor1.run(FORWARD);
  motor2.run(FORWARD);


  delay(10000);
  motor1.run(RELEASE);
  motor2.run(RELEASE);
  delay(10);

  motor1.run(BACKWARD);
  motor2.run(BACKWARD);

  delay(10000);


}

