#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <Adafruit_SSD1306.h>

#define OLED_RESET D0 // 16/D0 for Nodemcu, 4 for Arduino
Adafruit_SSD1306 display(OLED_RESET);

#if (SSD1306_LCDHEIGHT != 32)
#error("Height incorrect, please fix Adafruit_SSD1306.h!");
#endif

const char* ssid = "Eldric";
const char* password = "daomingerdec8";

void setup () {


  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);  // initialize with the I2C addr 0x3C (for the 128x32)
  display.display();
  delay(2000);
  display.clearDisplay();

  Serial.begin(115200);
  WiFi.begin(ssid, password);

  int dots = 0;
  setOLEDtext(0, 0, "Connecting", 1, 1, false);
  while (WiFi.status() != WL_CONNECTED) {
    delay(250); 
    setOLEDtext(dots, 8, ".", 1, 1, false);
    dots += 5;
  }

  // Print local IP address and start web server
  Serial.println("");
  Serial.println("WiFi connected.");
  setOLEDtext(0, 0, "WiFi connected!", 100, 1, true);

  Serial.println("IP address: ");
  setOLEDtext(0, 8, "IP/MAC Address", 100, 1, false);

  IPAddress currentIP = WiFi.localIP();
  Serial.println(WiFi.localIP());
  setOLEDtext(0, 16, (String)currentIP[0] + "." + (String)currentIP[1] + "." + (String)currentIP[2] + "." + (String)currentIP[3], 100, 1, false);
  setOLEDtext(0, 24, WiFi.macAddress() , 100, 1, false);

  delay(5000);

}

void loop() {

  if (WiFi.status() == WL_CONNECTED) { //Check WiFi connection status

    HTTPClient http;  //Declare an object of class HTTPClient

    http.begin("http://worldtimeapi.org/api/timezone/Asia/Manila");
    int httpCode = http.GET(); //Send the request

    if (httpCode > 0) { //Check the returning code
      showDateTimeData(http.getString());
    }
    http.end();   //Close connection
  }

  delay(30000); // update every 30 seconds

}

void setOLEDtext(int x, int y, String textToDisplay, int delayms, int textSize, bool clearText) {

  if (clearText) {
    display.clearDisplay();
  }

  display.setTextSize(textSize);
  display.setTextColor(WHITE);
  display.setCursor(x, y);
  display.print(textToDisplay);
  display.display();
  delay(delayms);
}

void showDateTimeData(String payload) {
  // Create Array for Field, Create Arra for Data
  String jsonField[15];
  String jsonData[15];
  int    jsonIndex = 0;

  // String payload = http.getString();   //Get the request response payload
  payload.replace("{", "");
  payload.replace("}", "");

  int commaIndex = payload.indexOf(",");

  while (commaIndex >= 0) {

    int delimiterIndex;
    String tmpFieldData = "";
    String tmpField = "";
    String tmpData = "";

    tmpFieldData = payload.substring(0, commaIndex);

    delimiterIndex = tmpFieldData.indexOf(":");
    tmpField = tmpFieldData.substring(0, delimiterIndex);
    tmpData =  tmpFieldData.substring(delimiterIndex + 1);

    tmpField.replace("\"", "");
    tmpData.replace("\"", "");

    jsonField[jsonIndex] = tmpField;
    jsonData[jsonIndex] = tmpData;

    jsonIndex++;

    payload.replace(payload.substring(0, commaIndex) + "," , "");
    commaIndex = payload.indexOf(",");

  }

  String dateTimeValue = jsonData[9];
  dateTimeValue = dateTimeValue.substring(0, dateTimeValue.indexOf("+") - 1 );

  String tmpDate = dateTimeValue.substring(0, dateTimeValue.indexOf("T") );
  String tmpTime = dateTimeValue.substring(dateTimeValue.indexOf("T") + 1);

  setOLEDtext(0, 0, tmpDate, 0, 1, true);
  setOLEDtext(0, 8, tmpTime.substring(0, 5), 0, 2, false);
  setOLEDtext(0, 24,  "UTC" + jsonData[1], 0, 1, false);
}
