#include <Servo.h>

Servo servo1;
Servo servo2;

int xAxis = A0;
int yAxis = A1;
int xValue = 0;
int yValue = 0;
int pressedPin = 2;
int notPressed = 0;


void setup() {
  // put your setup code here, to run once:

  Serial.begin(9600);

  servo1.attach(9);
  servo2.attach(10);

  servo1.write(0);
  servo2.write(0);
  
  pinMode(xAxis, INPUT);
  pinMode(yAxis, INPUT);
  pinMode(pressedPin, INPUT_PULLUP);
  
}

void loop() {
  // put your main code here, to run repeatedly:

  xValue = analogRead(xAxis);
  yValue = analogRead(yAxis);
  notPressed = digitalRead(pressedPin);

  Serial.print("x : "); Serial.print(map(xValue, 557, 1023, 0, 180));
  Serial.print(", y : "); Serial.println(map(yValue, 613, 1023, 0, 180));
  Serial.println("");
  
  servo1.write(map(xValue, 557, 1023, 0, 180));
  servo2.write(map(yValue, 613, 1023, 0, 180));

  delay(100);
}
