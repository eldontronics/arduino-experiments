#include <Stepper.h>

double stepsPerRevolution = 2048;
double degRevolution = 360;
double degUser = 45 / degRevolution;
double userSteps = degUser * stepsPerRevolution;

Stepper myStepper(stepsPerRevolution, 8, 10, 9, 11);  // Pin inversion to make the library work

void setup() {

  Serial.begin(9600);
  myStepper.setSpeed(10);

}

void loop() {

  // 1 rotation counterclockwise:
  Serial.println("counterclockwise");
  myStepper.step(stepsPerRevolution);
  delay(1000);

  // 1 rotation clockwise:
  Serial.println("clockwise");
  myStepper.step(-stepsPerRevolution);
  delay(1000);
}
