#include <SoftwareSerial.h>
#include <AFMotor.h>

AF_DCMotor motor1(3); //3 means M3 in which the DC motor is connected
AF_DCMotor motor2(4); //3 means M4 in which the DC motor is connected

SoftwareSerial bluetooth (3, 4); // 3 is TX on BLE, 4 is RX in BLE

int receivedCode = 0;

// DISTANCE SENSOR
//int trigPin = 13;
//int echoPin = 12;
//float duration, distance;
//long soundSpeed = 343; // meters per second

void setup() {
  bluetooth.begin(9600);
  Serial.begin(9600);

  motor1.setSpeed(255);
  motor2.setSpeed(255);

  motor1.run(RELEASE);
  motor2.run(RELEASE);

  // DISTANCE SENSOR
  // pinMode(trigPin, OUTPUT);
  // pinMode(echoPin, INPUT);

  Serial.println("Waiting for code...");
}

void loop() {


  while (bluetooth.available() > 0) {
    char receivedBluetoothChar = bluetooth.read();
    receivedCode = (int) receivedBluetoothChar;
    delay(10);
  }

  if (receivedCode == 3) {
    stopAllMotors();
    Serial.println("Forward");
    motor1.run(FORWARD); // Both motors rotate forward,
    motor2.run(FORWARD); // car moves forward.
  }
  if (receivedCode == 4) {
    stopAllMotors();
    Serial.println("Backward");
    motor1.run(BACKWARD); // Both motors rotate backward,
    motor2.run(BACKWARD); // car moves backward.
  }
  if (receivedCode == 1) {
    stopAllMotors();
    Serial.println("Left");
    motor2.run(FORWARD); // Turn motor 2 on, car goes left
  }
  if (receivedCode == 2) {
    stopAllMotors();
    Serial.println("Right");
    motor1.run(FORWARD); // Turn motor 1 on, car goes right
  }
  if (receivedCode == 5) {
    //Serial.println("Stop");
    stopAllMotors();
  }
  delay(5);

}

void stopAllMotors() {
  motor1.run(RELEASE); // Stop
  motor2.run(RELEASE); // stop
  delay(5);
}

