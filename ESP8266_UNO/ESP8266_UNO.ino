//Reference : http://www.instructables.com/id/Using-ESP-01-and-Arduino-UNO/

#include <SoftwareSerial.h>
#define TIMEOUT 5000 // mS

SoftwareSerial mySerial(0, 1); // RX, TX

void setup()
{

  Serial.begin(115200);
  mySerial.begin(115200);

  Serial.println("Sending command...");
  delay(1000);

  if (SendCommand("AT+RST", "Ready")) {
    Serial.println("Reset....OK");
    delay(1000);
  }
  delay(5000);
  if (SendCommand("AT+CWMODE=1", "OK")) {
    Serial.println("AT+CWMODE=1....OK");
    delay(1000);
  }
  if (SendCommand("AT+CIFSR", "OK")) {
    Serial.println("AT+CIFSR....OK");
    delay(1000);
  }
  if (SendCommand("AT+CIPMUX=1", "OK")) {
    Serial.println("AT+CIPMUX=1....OK");
    delay(1000);
  }
  if (SendCommand("AT+CIPSERVER=1,80", "OK")) {
    Serial.println("AT+CIPSERVER=1,80....OK");
    delay(1000);
  }
}

void loop() {
  
  //  button_state = digitalRead(button);
  //
  //  if (button_state == HIGH) {
  //    mySerial.println("AT+CIPSEND=0,23");
  //    mySerial.println("<h1>Button was pressed!</h1>");
  //    delay(1000);
  //    SendCommand("AT+CIPCLOSE=0", "OK");
  //  }
  //
  //  String IncomingString = "";
  //  boolean StringReady = false;
  //
  //  while (mySerial.available()) {
  //    IncomingString = mySerial.readString();
  //    StringReady = true;
  //  }
  //
  //  if (StringReady) {
  //    Serial.println("Received String: " + IncomingString);
  //
  //    if (IncomingString.indexOf("LED=ON") != -1) {
  //      digitalWrite(LED, HIGH);
  //    }
  //
  //    if (IncomingString.indexOf("LED=OFF") != -1) {
  //      digitalWrite(LED, LOW);
  //    }
  //  }
}

boolean SendCommand(String cmd, String ack) {
  mySerial.println(cmd); // Send "AT+" command to module
  if (!echoFind(ack)) // timed out waiting for ack string
    return true; // ack blank or ack found
}

boolean echoFind(String keyword) {
  byte current_char = 0;
  byte keyword_length = keyword.length();
  long deadline = millis() + TIMEOUT;
  while (millis() < deadline) {
    if (mySerial.available()) {
      char ch = mySerial.read();
      Serial.write(ch);
      if (ch == keyword[current_char])
        if (++current_char == keyword_length) {
          Serial.println();
          return true;
        }
    }
  }
  return false; // Timed out
}
