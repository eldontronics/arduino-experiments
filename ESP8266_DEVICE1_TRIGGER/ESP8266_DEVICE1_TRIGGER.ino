// NODEMCU

// This example shows how to connect to Cayenne using an ESP8266 and send/receive sample data.
// Make sure you install the ESP8266 Board Package via the Arduino IDE Board Manager and select the correct ESP8266 board before compiling. 

//#define CAYENNE_DEBUG
#define CAYENNE_PRINT Serial
#include <CayenneMQTTESP8266.h>

// WiFi network info.
char ssid[] = "Eldric";
char wifiPassword[] = "daomingerdec8";

// Cayenne authentication info. This should be obtained from the Cayenne Dashboard.
char username[] = "f8930340-8e0e-11e7-b546-bf6ccbcd8710";
char password[] = "c77677a1f50f6f6125ee0a81ad3707d2ebe86f1e";
char clientID[] = "6217cc80-ada7-11e7-a1da-536ee79fd847";

void setup() {
  pinMode(15, OUTPUT);
	Serial.begin(115200);
	Cayenne.begin(username, password, clientID, ssid, wifiPassword);
}

void loop() {
	Cayenne.loop();
}

CAYENNE_IN(15){
  if (getValue.asInt() == 1) { // NOTE: Channel = Virtual Pin
    digitalWrite(15, HIGH);
  }
  else {
    digitalWrite(15, LOW);
  }
}

