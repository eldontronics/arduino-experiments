#include <FastLED.h>

#define LED_PIN     6
#define COLOR_ORDER GRB
#define CHIPSET     WS2811
#define NUM_LEDS    36

#define BRIGHTNESS  200

CRGB leds[NUM_LEDS];

int potPin = A0;
int potValue = 0;

CRGB colors[] = { CRGB(255, 0, 0),
                  CRGB(255, 127, 0),
                  CRGB(255, 255, 0),
                  CRGB(0, 255, 0),
                  CRGB(0, 0, 255),
                  CRGB(75, 0, 130),
                  CRGB(148, 0, 211),
                  CRGB(255, 255, 255)
                };

void setup() {
  Serial.begin(9600);
  delay(3000); // sanity delay
  FastLED.addLeds<CHIPSET, LED_PIN, COLOR_ORDER>(leds, NUM_LEDS).setCorrection( TypicalLEDStrip );
  FastLED.setBrightness(BRIGHTNESS);

}

void loop()
{

  potValue =  map(analogRead(potPin), 0, 1023, -1, 8);
  for (int i = 0; i <= potValue; i++) {
    leds[i] = colors[i];
    FastLED.show();
  }

  for (int i = potValue + 1; i <= NUM_LEDS - 1; i++) {
    leds[i] = CRGB(0, 0, 0);
    FastLED.show();
  }



  delay(10);

}


