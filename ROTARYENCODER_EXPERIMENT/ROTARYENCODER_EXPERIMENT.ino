#include <LiquidCrystal_I2C.h>
LiquidCrystal_I2C lcd(0x3F,16,2);

int val = 0;
int encoder0PinA = 3; // CLK pin on Rotary Encoder
int encoder0PinB = 4; // DT pin on Rotary Encoder

//int encoder0PinA = 11; // CLK pin on Rotary Encoder
//int encoder0PinB = 12; // DT pin on Rotary Encoder
int encoder0Pos = 0;
int encoder0PinALast = LOW;
int n = LOW;
int ledPIN = 11;

int pressedPin = 2; // SW pin on Rotary Encoder
int notPressed = 0;


void setup() {
  // put your setup code here, to run once:


  Serial.begin(9600);
  lcd.init(); // initialize the lcd 
  lcd.backlight();
  lcd.home();
  lcd.setCursor(0, 0);
  lcd.print("Turn the dial of");
  lcd.setCursor(0, 1);
  lcd.print("potentiometer...");
    

  pinMode(ledPIN, OUTPUT);
  pinMode(encoder0PinA, INPUT);
  pinMode(encoder0PinB, INPUT);
  pinMode(pressedPin, INPUT_PULLUP);  
}

void loop() {

//  notPressed = !digitalRead(pressedPin);
//
//  if(!notPressed){
//    Serial.println("Pressed!");
//  }

  n = digitalRead(encoder0PinA);
  if( (encoder0PinALast == LOW) && (n = HIGH) ){
    if(digitalRead(encoder0PinB) == LOW){
      encoder0Pos--;
    }
    else{
      encoder0Pos++;
    }

    if( encoder0Pos > 1023 ) encoder0Pos = 1023;
    if( encoder0Pos < 0 ) encoder0Pos = 0;
    analogWrite(ledPIN, encoder0Pos);
    Serial.println(encoder0Pos);
    lcd.clear();
    lcd.print(encoder0Pos);
    
  }
  encoder0PinALast = n;
  
}
