#include <LiquidCrystal_I2C.h>
#include <dht.h>

LiquidCrystal_I2C lcd(0x3F,16,2);  // set the LCD address to 0x27 for a 16 chars and 2 line display
dht DHT;

#define DHT11_PIN     8
#define COGASSENSOR_PIN A3

float m = -0.318; //Slope 
float b = 1.133; //Y-Intercept 

void setup(){
  Serial.begin(9600);
                //  lcd.init();
                //  lcd.noBacklight();
                //  delay(100);
                //  lcd.backlight();  
                //  outputToLCD("Preparing DHT11","and MQ9 Sensors");

lcd.init();
lcd.backlight();

}

void loop()
{



    float sensor_volt;
    float RS_air; //  Get the value of RS via in a clear air
    float R0;  // Get the value of R0 via in LPG
    float sensorValue;

    
  int chk = DHT.read11(DHT11_PIN);
  

    /*--- Get a average data by testing 100 times ---*/
    for(int x = 0 ; x < 100 ; x++)
    {
        sensorValue = sensorValue + analogRead(COGASSENSOR_PIN);
    }
    sensorValue = sensorValue/100.0;
    /*-----------------------------------------------*/

    sensor_volt = sensorValue/1024*5.0;
    RS_air = (5.0-sensor_volt)/sensor_volt; // omit *RL
    R0 = RS_air/9.9; // The ratio of RS/R0 is 9.9 in LPG gas from Graph (Found using WebPlotDigitizer)
    
  char str[4];
  dtostrf(RS_air, 4, 1, str );

  char str2[4];
  dtostrf(R0, 4, 1, str2 );

  double ppm_log = (log10(RS_air / R0)-b)/m; //Get ppm value in linear scale according to the the ratio value
  double ppm = pow(10, ppm_log); //Convert ppm value to log scale 
  double percentage = ppm/10000; //Convert to percentage 

  
  outputToLCD("Temperature :", (String)DHT.temperature + " C");
  delay(2000);
  outputToLCD("Humidity :", (String)DHT.humidity + " %");
 delay(2000);
  outputToLCD("Air Resistance :", str);
 delay(2000);
  outputToLCD("Fresh Air :", str2);
 delay(2000);
  
}
  
                    void outputToLCD(String rowOneMessage, String rowTwoMessage){
                      lcd.clear();
                      lcd.setCursor(0, 0);
                      lcd.print(rowOneMessage);
                      lcd.setCursor(0, 1);
                      lcd.print(rowTwoMessage);  
                    }


