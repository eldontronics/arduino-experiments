int pinSegment[8] =   {2, 3, 4, 5, 6, 7, 8};
int pinDigit[4]  =   {10, 11, 12, 13};
int dPoint        =   9;

void setup() {
  // put your setup code here, to run once:
  pinMode(dPoint, OUTPUT);
  
  for(int i = 0; i < 8; i++){
    pinMode(pinSegment[i], OUTPUT);
  }

  for(int i = 0; i < 4; i++){
    pinMode(pinDigit[i], OUTPUT);
  }

}

void loop() {
  // put your main code here, to run repeatedly:
  digitalWrite(pinDigit[0], HIGH); // light the first digit (thousand)
  digitalWrite(pinDigit[1], LOW);
  digitalWrite(pinDigit[2], LOW);
  digitalWrite(pinDigit[3], LOW);
  digitalWrite(dPoint, HIGH);
  digitalWrite(pinSegment[0], HIGH); 
  digitalWrite(pinSegment[1], LOW);
  digitalWrite(pinSegment[2], LOW);
  digitalWrite(pinSegment[3], HIGH);
  digitalWrite(pinSegment[4], HIGH);
  digitalWrite(pinSegment[5], HIGH);
  digitalWrite(pinSegment[6], HIGH);
  delay(5);
  digitalWrite(pinDigit[0], LOW);
  digitalWrite(pinDigit[1], HIGH); // light the second digit (hundred)
  digitalWrite(pinDigit[2], LOW);
  digitalWrite(pinDigit[3], LOW);
  digitalWrite(dPoint, HIGH);
  digitalWrite(pinSegment[0], LOW); 
  digitalWrite(pinSegment[1], LOW);
  digitalWrite(pinSegment[2], HIGH);
  digitalWrite(pinSegment[3], LOW);
  digitalWrite(pinSegment[4], LOW);
  digitalWrite(pinSegment[5], HIGH);
  digitalWrite(pinSegment[6], LOW);
  delay(5);
  digitalWrite(pinDigit[0], LOW);
  digitalWrite(pinDigit[1], LOW);
  digitalWrite(pinDigit[2], HIGH);  // light the third digit (tens)
  digitalWrite(pinDigit[3], LOW);
  digitalWrite(dPoint, HIGH);
  digitalWrite(pinSegment[0], LOW); 
  digitalWrite(pinSegment[1], LOW);
  digitalWrite(pinSegment[2], LOW);
  digitalWrite(pinSegment[3], LOW);
  digitalWrite(pinSegment[4], HIGH);
  digitalWrite(pinSegment[5], HIGH);
  digitalWrite(pinSegment[6], LOW);
  delay(5);
  digitalWrite(pinDigit[0], LOW); // light the fourth digit (ones)
  digitalWrite(pinDigit[1], LOW);
  digitalWrite(pinDigit[2], LOW);
  digitalWrite(pinDigit[3], HIGH);
  digitalWrite(dPoint, HIGH);
  digitalWrite(pinSegment[0], HIGH); 
  digitalWrite(pinSegment[1], LOW);
  digitalWrite(pinSegment[2], LOW);
  digitalWrite(pinSegment[3], HIGH);
  digitalWrite(pinSegment[4], HIGH);
  digitalWrite(pinSegment[5], LOW);
  digitalWrite(pinSegment[6], LOW);
  delay(5);
}
