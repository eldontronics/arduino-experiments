#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define OLED_RESET 4

Adafruit_SSD1306 display(OLED_RESET);

#if (SSD1306_LCDHEIGHT != 64)
#error("Height incorrect, please fix Adafruit_SSD1306.h!");
#endif

// one sec = 6 deg = 0.10471967 rad
// 00 should be 1.570795

float degradsec   =  4.712385;
float degradmin   =  4.712385;
float degradhour  =  4.712385;

float px_sec;
float py_sec;

float px_min;
float py_min;

float px_hour;
float py_hour;

int seconds;
int minutes;
int hours;

void setup()   {
  Serial.begin(9600);
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);  // initialize with the I2C addr 0x3D (for the 128x64)
  display.clearDisplay();
  display.display();

  // Set the time here
  seconds = 50;
  minutes = 45;
  hours = 12;

  degradsec  = 4.712385 + (seconds * 0.10471967);
  degradmin  = 4.712385 + (minutes * 0.10471967) + (seconds / 60) * 0.10471967;
  degradhour = 4.712385 + (hours * 0.10471967 * 5) + ((minutes / 12) * 0.10471967);

}

void loop() {

  px_sec = ((display.width() / 2) + 20 * cos(degradsec));
  py_sec = ((display.height() / 2) + 20 * sin(degradsec));

  px_min = ((display.width() / 2) + 15 * cos(degradmin));
  py_min = ((display.height() / 2) + 15 * sin(degradmin));

  px_hour = ((display.width() / 2) + 10 * cos(degradhour));
  py_hour = ((display.height() / 2) + 10 * sin(degradhour));

  display.setCursor(0, 0);
  display.setTextSize(2);
  display.setTextColor(WHITE);
  display.println("");
  display.println(addLeadingZeroes(hours, 2));
  display.println(addLeadingZeroes(minutes, 2));
  display.println(addLeadingZeroes(seconds, 2));

  display.setCursor(98, 45);
  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.println("ELDON");
  display.setCursor(86, 55);
  display.println("myWatch");

  // Draw the border of the clock
  display.drawCircle(display.width() / 2, display.height() / 2, 21, WHITE);

  // Draw the seconds hand
  display.drawLine(display.width() / 2, display.height() / 2, px_sec + 1, py_sec + 1, WHITE);

  // Draw the minutes hand
  display.drawLine(display.width() / 2, display.height() / 2, px_min + 1, py_min + 1, WHITE);

  // Draw the hours hand
  display.drawLine(display.width() / 2, display.height() / 2, px_hour + 1, py_hour + 1, WHITE);  // hours hand

  display.display();
  delay(1);
  display.clearDisplay();

  degradsec += 0.10471967;
  seconds ++;

  if (seconds == 60) {
    seconds = 0;
    minutes++;

    degradmin += 0.10471967;
    degradsec = 4.712385;
  }

// Improve more here
  if (minutes % 12 == 0 && seconds == 0) {
    if (hours == 24 || hours == 0 || hours == 12) {
      degradhour = 4.712385;
    }
    else {
      degradhour += 0.10471967;
    }
  }

  if (minutes == 60) {
    degradhour += 0.10471967;
    degradmin = 4.712385;
    minutes = 0;
    hours++;
  }

  if (hours == 24 || hours == 12) {
    degradhour = 4.712385;
    if (hours == 24) {
      hours = 0;
    }
  }
}

String addLeadingZeroes(int number, int maxDigit) {
  String strNum = String(number, DEC);
  int leadingZeroes = maxDigit - strNum.length();
  for (int i = 0; i < leadingZeroes; i++) {
    strNum = "0" + strNum;
  }

  return strNum;
}

