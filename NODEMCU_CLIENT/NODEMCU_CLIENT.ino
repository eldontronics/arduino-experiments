// This example shows how to connect to Cayenne using an ESP8266 and send/receive sample data.
// Make sure you install the ESP8266 Board Package via the Arduino IDE Board Manager and select the correct ESP8266 board before compiling.

//#define CAYENNE_DEBUG
#define CAYENNE_PRINT Serial
#include <CayenneMQTTESP8266.h>

// WiFi network info.
char ssid[] = "Eldric";
char wifiPassword[] = "daomingerdec8";

// Cayenne authentication info. This should be obtained from the Cayenne Dashboard.
char username[] = "032651b0-8d81-11e7-9727-55550d1a07e7";
char password[] = "8d1d37cd312702aeaba4bf701156485c0cea70a0";
char clientID[] = "d984bb10-ec9b-11e7-a2f9-8f754ce96236";

unsigned long lastMillis = 0;


void setup() {
  Serial.begin(9600);
  Cayenne.begin(username, password, clientID, ssid, wifiPassword);
  pinMode(14, OUTPUT);
}

void loop() {
  Cayenne.loop();

  //Publish data every 10 seconds (10000 milliseconds). Change this value to publish at a different interval.
//  if (millis() - lastMillis > 10000) {
//    lastMillis = millis();
//    //Write data to Cayenne here. This example just sends the current uptime in milliseconds.
//    Cayenne.virtualWrite(0, lastMillis);
//    //Some examples of other functions you can use to send data.
//    //Cayenne.celsiusWrite(1, 22.0);
//    //Cayenne.luxWrite(2, 700);
//    //Cayenne.virtualWrite(3, 50, TYPE_PROXIMITY, UNIT_CENTIMETER);
//  }
}

//Default function for processing actuator commands from the Cayenne Dashboard.
//You can also use functions for specific channels, e.g CAYENNE_IN(1) for channel 1 commands.
CAYENNE_IN_DEFAULT()
{
  CAYENNE_LOG("CAYENNE_IN_DEFAULT(%u) - %s, %s", request.channel, getValue.getId(), getValue.asString());
  //Process message here. If there is an error set an error message using getValue.setError(), e.g getValue.setError("Error message");
  digitalWrite(request.channel,  getValue.asInt());
}
