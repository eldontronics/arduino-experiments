#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define OLED_RESET 4
Adafruit_SSD1306 display(OLED_RESET);

#if (SSD1306_LCDHEIGHT != 64)
#error("Height incorrect, please fix Adafruit_SSD1306.h!");
#endif

// Analog Pin A5 for the Probe Antenae
int antenaPin = A0;
int antenaValue = 0;
int mappedValue = 0;


void setup() {
  Serial.begin(9600);
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);
  display.clearDisplay();
  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(0, 0);
  display.print("Program ready...");
  display.display();
  delay(2000);
}

void loop() {
  antenaValue = analogRead(antenaPin);

  display.setCursor(0, 0);
  display.clearDisplay();
  display.setTextSize(1);
  display.print("EMF Value");
  display.setTextSize(8);
  display.setCursor(45, 10);
  display.print(antenaValue);
  display.display();
  // Serial print
  Serial.print("Raw value: ");
  Serial.println(antenaValue);
  delay(100);
}

