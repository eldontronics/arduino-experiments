// WHEEL 1
int motor1Pin1 = 5;
int motor1Pin2 = 6;

// WHEEL 2
int motor2Pin1 = 9;
int motor2Pin2 = 10;


void setup() {
  // put your setup code here, to run once:

  pinMode(motor1Pin1, OUTPUT);
  pinMode(motor1Pin2, OUTPUT);
  pinMode(motor2Pin1, OUTPUT);
  pinMode(motor2Pin2, OUTPUT);

}

void loop() {
  digitalWrite(motor1Pin1, HIGH); // FORWARD
  digitalWrite(motor1Pin2, LOW); // BACKWARD
  digitalWrite(motor2Pin1, HIGH); // FORWARD
  digitalWrite(motor2Pin2, LOW); // BACKWARD

  delay(1000);

  digitalWrite(motor1Pin1, LOW); // FORWARD
  digitalWrite(motor1Pin2, HIGH); // BACKWARD
  digitalWrite(motor2Pin1, LOW); // FORWARD
  digitalWrite(motor2Pin2, HIGH); // BACKWARD

  delay(1000);
}


////set the L293D’s three control pins to 9, 10, and 11 on the Arduino
//int enablePin = 11;
//int in1Pin = 10;
//int in2Pin = 9;
//
//void setup()
//{
// pinMode(enablePin, OUTPUT);
// pinMode(in1Pin, OUTPUT);
// pinMode(in2Pin, OUTPUT);
//}
//
//void loop()
//
//{
// analogWrite(enablePin, 255); // this sets the speed for the motor at 50
// digitalWrite(in1Pin, HIGH);
// digitalWrite(in2Pin, LOW);
// 
// // delay(2000); // delays for 2 seconds
////
//// analogWrite(enablePin, 255);
//// digitalWrite(in1Pin, LOW);
//// digitalWrite(in2Pin, HIGH);
//// delay(2000); // delays for 2 seconds
//}










































