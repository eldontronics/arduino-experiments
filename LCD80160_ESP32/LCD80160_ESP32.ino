#include <SD.h>
#include <SPI.h>
#include <WiFi.h>
#include <WiFiMulti.h>
#include <HTTPClient.h>

#include "LCD_Driver.h"
#include "GUI_Paint.h"
#include "image.h"


#define BUFFPIXEL   64
WiFiMulti wifiMulti;

struct Button {
  const uint8_t PIN;
  uint32_t numberKeyPresses;
  bool pressed;
};

struct PubDateTime {
  String Date;
  String Time;
};

Button sourceButton   = {35, 0, false};
Button titleButton    = {32, 0, false};
Button restartButton  = {34, 0, false};

PubDateTime newsPubDateTime;

void isrRestart()
{

  static unsigned long previousInterruptTime = 0;
  unsigned long currentInterruptTime = millis();

  if (currentInterruptTime - previousInterruptTime > 200)
  {
    restartButton.pressed = true;
  }
  previousInterruptTime = currentInterruptTime;
}

void isrSource()
{

  static unsigned long previousInterruptTime = 0;
  unsigned long currentInterruptTime = millis();

  if (currentInterruptTime - previousInterruptTime > 200)
  {
    sourceButton.pressed = true;
  }
  previousInterruptTime = currentInterruptTime;
}


void isrTitle()
{
  static unsigned long previousInterruptTime = 0;
  unsigned long currentInterruptTime = millis();

  if (currentInterruptTime - previousInterruptTime > 200)
  {
    titleButton.pressed = true;
  }
  previousInterruptTime = currentInterruptTime;
}

String rssNewsFeedURL[6] = {
  "https://www.inquirer.net/fullfeed",
  "https://www.philstar.com/rss/headlines",
  "http://manilastandard.net/feed/news",
  "https://www.manilatimes.net/rss-feed/",
  "http://www.senate.gov.ph/rss/rss_news.aspx"
};

String rssURL = "";
const char* ca = \
                 "-----BEGIN CERTIFICATE-----\n" \
                 "MIIEkjCCA3qgAwIBAgIQCgFBQgAAAVOFc2oLheynCDANBgkqhkiG9w0BAQsFADA/\n" \
                 "MSQwIgYDVQQKExtEaWdpdGFsIFNpZ25hdHVyZSBUcnVzdCBDby4xFzAVBgNVBAMT\n" \
                 "DkRTVCBSb290IENBIFgzMB4XDTE2MDMxNzE2NDA0NloXDTIxMDMxNzE2NDA0Nlow\n" \
                 "SjELMAkGA1UEBhMCVVMxFjAUBgNVBAoTDUxldCdzIEVuY3J5cHQxIzAhBgNVBAMT\n" \
                 "GkxldCdzIEVuY3J5cHQgQXV0aG9yaXR5IFgzMIIBIjANBgkqhkiG9w0BAQEFAAOC\n" \
                 "AQ8AMIIBCgKCAQEAnNMM8FrlLke3cl03g7NoYzDq1zUmGSXhvb418XCSL7e4S0EF\n" \
                 "q6meNQhY7LEqxGiHC6PjdeTm86dicbp5gWAf15Gan/PQeGdxyGkOlZHP/uaZ6WA8\n" \
                 "SMx+yk13EiSdRxta67nsHjcAHJyse6cF6s5K671B5TaYucv9bTyWaN8jKkKQDIZ0\n" \
                 "Z8h/pZq4UmEUEz9l6YKHy9v6Dlb2honzhT+Xhq+w3Brvaw2VFn3EK6BlspkENnWA\n" \
                 "a6xK8xuQSXgvopZPKiAlKQTGdMDQMc2PMTiVFrqoM7hD8bEfwzB/onkxEz0tNvjj\n" \
                 "/PIzark5McWvxI0NHWQWM6r6hCm21AvA2H3DkwIDAQABo4IBfTCCAXkwEgYDVR0T\n" \
                 "AQH/BAgwBgEB/wIBADAOBgNVHQ8BAf8EBAMCAYYwfwYIKwYBBQUHAQEEczBxMDIG\n" \
                 "CCsGAQUFBzABhiZodHRwOi8vaXNyZy50cnVzdGlkLm9jc3AuaWRlbnRydXN0LmNv\n" \
                 "bTA7BggrBgEFBQcwAoYvaHR0cDovL2FwcHMuaWRlbnRydXN0LmNvbS9yb290cy9k\n" \
                 "c3Ryb290Y2F4My5wN2MwHwYDVR0jBBgwFoAUxKexpHsscfrb4UuQdf/EFWCFiRAw\n" \
                 "VAYDVR0gBE0wSzAIBgZngQwBAgEwPwYLKwYBBAGC3xMBAQEwMDAuBggrBgEFBQcC\n" \
                 "ARYiaHR0cDovL2Nwcy5yb290LXgxLmxldHNlbmNyeXB0Lm9yZzA8BgNVHR8ENTAz\n" \
                 "MDGgL6AthitodHRwOi8vY3JsLmlkZW50cnVzdC5jb20vRFNUUk9PVENBWDNDUkwu\n" \
                 "Y3JsMB0GA1UdDgQWBBSoSmpjBH3duubRObemRWXv86jsoTANBgkqhkiG9w0BAQsF\n" \
                 "AAOCAQEA3TPXEfNjWDjdGBX7CVW+dla5cEilaUcne8IkCJLxWh9KEik3JHRRHGJo\n" \
                 "uM2VcGfl96S8TihRzZvoroed6ti6WqEBmtzw3Wodatg+VyOeph4EYpr/1wXKtx8/\n" \
                 "wApIvJSwtmVi4MFU5aMqrSDE6ea73Mj2tcMyo5jMd6jmeWUHK8so/joWUoHOUgwu\n" \
                 "X4Po1QYz+3dszkDqMp4fklxBwXRsW10KXzPMTZ+sOPAveyxindmjkW8lGy+QsRlG\n" \
                 "PfZ+G6Z6h7mjem0Y+iWlkYcV4PIWL1iwBi8saCbGS5jN2p8M+X+Q7UNKEkROb3N6\n" \
                 "KOqkqm57TH2H3eDJAkSnh6/DNFu0Qg==\n" \
                 "-----END CERTIFICATE-----\n";


void setup()
{
  Serial.begin(115200);
  pinMode(sourceButton.PIN, INPUT_PULLUP);
  pinMode(titleButton.PIN, INPUT_PULLUP);
  pinMode(restartButton.PIN, INPUT_PULLUP);

  attachInterrupt(sourceButton.PIN, isrSource, RISING);
  attachInterrupt(titleButton.PIN, isrTitle, RISING);
  attachInterrupt(restartButton.PIN, isrRestart, RISING);

  Config_Init();
  LCD_Init();
  LCD_Clear(BLACK);
  LCD_SetBacklight(100);

  Paint_NewImage(LCD_WIDTH, LCD_HEIGHT, 0, BLACK);
  Paint_Clear(BLACK);
  Paint_SetRotate(0);

  Serial.print("Wait");
  for (uint8_t t = 4; t > 0; t--) {
    Serial.printf(".");
    Serial.flush();
    delay(500);
  }

  wifiMulti.addAP("Eldric", "daomingerdec8");
  Paint_DrawString_EN(0, 0, "Connected",  &Font24, BLACK, WHITE);
  Paint_DrawString_EN(0, 20, "Please wait...", &Font12, BLACK, BLUE);

}


void loop() {

  for (int rssURLIndex = 0; rssURLIndex < 5; rssURLIndex++) {

    String line = "";

    rssURL = rssNewsFeedURL[rssURLIndex];

    if ((wifiMulti.run() == WL_CONNECTED)) {

      HTTPClient http;

      if (rssURL.indexOf("https://") >= 0) {
        http.begin(rssURL, ca); //HTTPS
      }
      else {
        http.begin(rssURL); //HTTP
      }

      int httpCode = http.GET();

      // httpCode will be negative on error
      if (httpCode > 0) {
        if (httpCode == HTTP_CODE_OK) {
          String payload = http.getString();
          line = payload;
          //Serialprintln(payload);
        }
      } else {
        Serial.printf("[HTTP] GET... failed, error: %s\n", http.errorToString(httpCode).c_str());
        http.end();
        return;
      }

      http.end();

      line.trim();
      // GET the source here:
      int startSourceIndex = line.indexOf("<title>");
      int endSourceIndex = line.indexOf("</title>") + String("</title").length() + 1;

      String sourceHeader = (line.substring(startSourceIndex, endSourceIndex));

      Serial.print("Source : ");

      // Replace some characters here:
      sourceHeader.replace("<![CDATA[", "'");
      sourceHeader.replace("]]>", "'");
      sourceHeader.replace("<title>", "");
      sourceHeader.replace("</title>", "");
      sourceHeader.replace("&gt;", ">");
      sourceHeader.replace("&mdash;", "-");
      sourceHeader.replace("&#x2018;", "'");
      sourceHeader.replace("&lsquo;", "'");
      sourceHeader.replace("&#x2019;", "'");
      sourceHeader.replace("&#8217;", "'");
      sourceHeader.replace("&#8216;", "'");
      sourceHeader.replace("&rsquo;", "'");
      sourceHeader.replace("-", "");
      sourceHeader.replace("–", "-");
      sourceHeader.replace("—", "-");
      sourceHeader.replace("'", "");
      sourceHeader.replace("  ", " ");
      sourceHeader.replace("&amp;", "&");
      sourceHeader.replace("&#241;", "ñ");
      sourceHeader.trim();

      Serial.println(sourceHeader);
      Serial.println();

      // Remove excess data up to the first <item> tag.
      line = line.substring(line.indexOf("<item>"));
      int startIndex = -1;
      int endIndex = -1;

      while (line.length() > 0) {

        startIndex = line.indexOf("<item>");
        endIndex = line.indexOf("</item>") + String("</item>").length() + 1;

        if (startIndex >= 0 && endIndex >= 0) {

          int startTitle = -1;
          int endTitle = -1;

          int pubDateTimeStart =  line.indexOf("<pubDate>") +  String("<pubDate>").length();
          int pubDateTimeEnd = line.indexOf("</pubDate>");

          Serial.println(line.substring(pubDateTimeStart, pubDateTimeEnd));
          GetPubDateTime(line.substring(pubDateTimeStart, pubDateTimeEnd));

          startTitle = line.indexOf("<title>");
          endTitle = line.indexOf("</title>");

          String newsRawTitle = line.substring(startTitle, endTitle);

          newsRawTitle.replace("<title><![CDATA[", "");
          newsRawTitle.replace("]]>", "");
          newsRawTitle.replace("<title>", "");

          // Replace some characters here:
          newsRawTitle.replace("&mdash;", "-");
          newsRawTitle.replace("&#x2018;", "'");
          newsRawTitle.replace("&lsquo;", "'");
          newsRawTitle.replace("&#x2019;", "'");
          newsRawTitle.replace("&rsquo;", "'");
          newsRawTitle.replace("&#8217;", "'");
          newsRawTitle.replace("&#8216;", "'");
          newsRawTitle.replace("&#8212;", "-");
          newsRawTitle.replace("&#8211;", "-");
          newsRawTitle.replace("‘", "'");
          newsRawTitle.replace("’", "'");
          newsRawTitle.replace("&amp;", "&");
          newsRawTitle.replace("&quot;", "\"");
          newsRawTitle.replace("&#039;", "'");
          newsRawTitle.replace("&#8230;", "...");
          newsRawTitle.replace("—", "-");
          newsRawTitle.replace("–", "-");
          newsRawTitle.replace("&#241;", "ñ");

          Serial.println(newsRawTitle);

          rssURL.replace("https://", "");
          rssURL.replace("http://", "");
          rssURL.replace("www.", "");
          rssURL = rssURL.substring(0, rssURL.indexOf("/"));
          rssURL.toUpperCase();
          Paint_Clear(BLACK);
          displayNews(sourceHeader, newsRawTitle, newsPubDateTime);

          // Exit from scrollText
          if (sourceButton.pressed) {
            sourceButton.pressed = false;
            break;
          }

          line = line.substring(endIndex - 1);
        }
        else {
          line = "";
        }
      }
    }
  }
}

void displayNews(String headerTitle, String headerDetail, PubDateTime pubDateTime) {
  char __newsSource[100];
  headerTitle.toCharArray(__newsSource, sizeof(__newsSource));
  char __newsItem[100];
  headerDetail.toCharArray(__newsItem, sizeof(__newsItem));

  Paint_DrawString_EN(0, 0, __newsSource, &Font12, BLACK, BLUE);
  Paint_DrawString_EN(0, 10, __newsItem, &Font12, BLACK, WHITE);

  for (int i = 1; i <= 100; i++) {

    if (titleButton.pressed) {
      Serial.println("Change news item...");
      Paint_Clear(BLACK);
      Paint_DrawString_EN(0, 0, "Changing", &Font12, BLACK, BLUE);
      Paint_DrawString_EN(0, 20, "News", &Font24, BLACK, WHITE);
      Paint_DrawString_EN(0, 40, "Item...", &Font24, BLACK, WHITE);
      
      delay(100);
      titleButton.pressed = false;
      return;
    }

    if (sourceButton.pressed) {
      Paint_Clear(BLACK);
      Paint_DrawString_EN(0, 0, "Changing", &Font12, BLACK, BLUE);
      Paint_DrawString_EN(0, 20, "News", &Font24, BLACK, WHITE);      
      Paint_DrawString_EN(0, 40, "Source...", &Font24, BLACK, WHITE);
      delay(100);
      return;
    }

    if (restartButton.pressed) {
      Paint_Clear(BLACK);
      Paint_DrawString_EN(0, 0,  "Restarting", &Font12, BLACK, RED);
      Paint_DrawString_EN(0, 20, "ESP32...", &Font24, BLACK, RED);
      delay(2000);
      restartButton.pressed = false;
      ESP.restart();
    }

    delay(100);
  }
}

void GetPubDateTime(String dateTime) {
  // Split the datetime value to date and time respectively. 4th space splits the two
  int indx = -1;
  
  int cnt = 0;
  for (int i = 0; i < dateTime.length(); i++) {
    if (dateTime.charAt(i) == ' ') {
      cnt++;
    }

    if (cnt == 4) {
      indx = i;
      break;
    }

  }

  newsPubDateTime.Date = dateTime.substring(0, indx);
  newsPubDateTime.Time = dateTime.substring(indx + 1);

}
