#include <Adafruit_SSD1306.h>
#include <SoftwareSerial.h>

#define OLED_RESET 4
Adafruit_SSD1306 display1(OLED_RESET);

bool isLineComplete = false;
const String gpsSignal = "$GPRMC";
const int waitDelay = 5;

SoftwareSerial neo6mGPS(11, 10);
int charCount = 0;
int charIndex = -1;

String nmeaStatement = "";

void setup()
{
  Serial.begin(9600);
  neo6mGPS.begin(9600);

  display1.begin(SSD1306_SWITCHCAPVCC, 0x3C);
  display1.display();
  display1.setTextSize(1);
  display1.setTextColor(WHITE);

}


void loop() {


  while (neo6mGPS.available() > 0) {

    char gpsData = (char)neo6mGPS.read();


    if (gpsData == '\n') {


      if (nmeaStatement.substring(0, 6) == gpsSignal) {

        // Get the second comma
        nmeaStatement = "$GPRMC,123519,A,4807.038,N,01131.000,E";
        int lineLength = nmeaStatement.length();
        char nmeaLineArray[lineLength];

        nmeaStatement.toCharArray(nmeaLineArray, lineLength);

        int commaIndex = -1;
        int commaCount = 0;

        int utcCommaIndex = -1;
        int flagCommaIndex = -1;
        int latCommaIndex = -1;
        int lngCommaIndex = -1;
        int latCommaDirectionIndex  = -1;
        int lngCommaDirectionIndex  = -1;

        // -----------------------------------------------
        for (int index = 0; index < lineLength; index++) {
          if (nmeaLineArray[index] == ',') {
            commaCount++;
          }
          // Get the index of the desired value:
          if (commaCount == 1 && utcCommaIndex == -1) {
            utcCommaIndex = index;
          }
          else if (commaCount == 2 && flagCommaIndex == -1) {
            flagCommaIndex = index;
          }
          else if (commaCount == 3 && latCommaIndex == -1) {
            latCommaIndex = index;
          }
          else if (commaCount == 4 && latCommaDirectionIndex == -1) {
            latCommaDirectionIndex = index;
          }
          else if (commaCount == 5 && lngCommaIndex == -1) {
            lngCommaIndex = index;
          }
          else if (commaCount == 6 && lngCommaDirectionIndex == -1) {
            lngCommaDirectionIndex = index;
          }

        }
        // -----------------------------------------------

        // TODO: Treat the signal here:
        String flagValue = nmeaStatement.substring(flagCommaIndex + 1, flagCommaIndex + 2);

        if (flagValue == "A") {

          // GET LAT
          char nmeaLatArrayFetch[8];
          for (int i = latCommaIndex + 1; i < latCommaDirectionIndex; i++) {
            nmeaLatArrayFetch[i - latCommaIndex - 1] = nmeaLineArray[i];
          }
          
          Serial.println((String)nmeaLatArrayFetch);
          display1.setCursor(0, 0);
          display1.setTextSize(1);
          display1.clearDisplay();
          display1.println((String)nmeaLatArrayFetch);
          display1.display();
        }
        else {
          display1.setCursor(0, 0);
          display1.setTextSize(1);
          display1.clearDisplay();
          display1.println("No GPS location!");
          display1.display();
        }

        delay(1000);
      }

      // Reset the string to empty string
      nmeaStatement = "";
    }
    else {
      nmeaStatement += gpsData;
    }
  }
}
