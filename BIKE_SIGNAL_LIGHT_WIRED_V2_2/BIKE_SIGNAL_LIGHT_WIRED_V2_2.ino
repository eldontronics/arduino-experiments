#include "LedControl.h"

#define __TURN_LEFT         0 // Device 1 (LED Matrix, at Index 0)
#define __TURN_RIGHT        1 // Device 2 (LED Matrix, at Index 1)

int     pushButtonLeft = 6;
int     pushButtonRight = 7;

char    appData;
String  inData = "";
String  latestCommand = "";

long    interval = 500;
long    previousMillis = 0;
bool    isLighted = false;
int     ledState = LOW;

LedControl lc = LedControl(13, 11, 12, 2);

unsigned long delaytime = 50;

void setup() {
  Serial.begin(9600);
  pinMode(pushButtonLeft, INPUT);
  pinMode(pushButtonRight, INPUT);

  lc.shutdown(0, false);
  lc.shutdown(1, false);

  /* Set the brightness to a medium values */
  lc.setIntensity(0, 15);
  lc.setIntensity(1, 15);

  /* and clear the display */
  lc.clearDisplay(0);
  lc.clearDisplay(1);
}


void loop() {

  if (digitalRead(pushButtonLeft)) {
    latestCommand = "LEFT";
  }
  else if (digitalRead(pushButtonRight)) {
    latestCommand = "RIGHT";
  }
  else {
    latestCommand = "";
  }

  if (latestCommand != "") {
    if (latestCommand == "LEFT") {
      lc.clearDisplay(1);
      blinkDirection(0, __TURN_LEFT);
    }

    if (latestCommand == "RIGHT") {
      lc.clearDisplay(0);
      blinkDirection(1, __TURN_RIGHT);
    }

    if (latestCommand == "") {
      lc.clearDisplay(0);
      lc.clearDisplay(1);
    }
  }
  else {
    // if lost connection from the device, turn all the matrices off.
    lc.clearDisplay(0);
    lc.clearDisplay(1);
    latestCommand = "";
  }

}


void blinkDirection(int deviceID, int directionID) {

  byte turnImage[8];

  if (directionID == __TURN_RIGHT) {
    turnImage[0] = B01100110;
    turnImage[1] = B00111100;
    turnImage[2] = B00011000;
    turnImage[3] = B10000001;
    turnImage[4] = B11000011;
    turnImage[5] = B01100110;
    turnImage[6] = B00111100;
    turnImage[7] = B00011000;
  }
  else {
    turnImage[0] = B00011000;
    turnImage[1] = B00111100;
    turnImage[2] = B01100110;
    turnImage[3] = B11000011;
    turnImage[4] = B10000001;
    turnImage[5] = B00011000;
    turnImage[6] = B00111100;
    turnImage[7] = B01100110;
  }

  byte black[8] = { B00000000,
                    B00000000,
                    B00000000,
                    B00000000,
                    B00000000,
                    B00000000,
                    B00000000,
                    B00000000
                  };

  unsigned long currentMillis = millis();

  if ((ledState == HIGH) && (currentMillis - previousMillis >= interval))
  {
    ledState = LOW;  // Turn it off
    previousMillis = currentMillis;  // Remember the time
    for (int row = 0; row <= 7; row++) {
      lc.setRow(deviceID, row, turnImage[row]);
    }
  }
  else if ((ledState == LOW) && (currentMillis - previousMillis >= interval))
  {
    ledState = HIGH;  // turn it on
    previousMillis = currentMillis;   // Remember the time
    for (int row = 0; row <= 7; row++) {
      lc.setRow(deviceID, row, black[row]);
    }
  }
}
