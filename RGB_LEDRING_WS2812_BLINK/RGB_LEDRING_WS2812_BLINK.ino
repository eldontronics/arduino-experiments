#include "FastLED.h"

#define NUM_LEDS 24
#define DATA_PIN 13
#define CLOCK_PIN 13

// Define the array of leds
CRGB leds[NUM_LEDS];

int delayTime = 100;

void setup() {
  FastLED.addLeds<WS2812, DATA_PIN, GRB>(leds, NUM_LEDS);
}

void loop() {
  blinkLedColor(0, CRGB(255, 0 , 0), delayTime);
  blinkLedColor(1, CRGB(255, 127 , 0), delayTime);
  blinkLedColor(2, CRGB(255, 255, 0), delayTime);
  blinkLedColor(3, CRGB(0, 255 , 0), delayTime);
  blinkLedColor(4, CRGB(0, 0 , 255), delayTime);
  blinkLedColor(5, CRGB(75 , 0, 130), delayTime);
  blinkLedColor(6, CRGB(148, 0 , 211), delayTime);
  blinkLedColor(7, CRGB(255, 255 , 255), delayTime);
}


void blinkLedColor(int ledInd, CRGB ledColor, int delaySec) {
  // Turn the LED on, then pause
  leds[ledInd] = (CRGB) ledColor;
  FastLED.show();
  delay(delaySec);
  // Now turn the LED off, then pause
  leds[ledInd] = CRGB::Black;
  FastLED.show();
  delay(delaySec);

}

