#include "FastLED.h"

#define NUM_LEDS 24
#define DATA_PIN A1

// Define the array of leds
CRGB leds[NUM_LEDS];

int delayTime = 1;

void setup() {
  FastLED.addLeds<WS2812, DATA_PIN, GRB>(leds, NUM_LEDS);
}

void loop() {

  for (int i = 0; i < NUM_LEDS; i++) {
    for (int j = 1; j <= 100; j++) {
      lightLedColor(i, CRGB::White);
      FastLED.setBrightness(j);
      delay(delayTime);
    }

  }

}


void blinkLedColor(int ledInd, CRGB ledColor, int delaySec) {
  // Turn the LED on, then pause
  leds[ledInd] = (CRGB) ledColor;
  FastLED.show();
  delay(delaySec);
  // Now turn the LED off, then pause
  leds[ledInd] = CRGB::Black;
  FastLED.show();
  delay(delaySec);
}

void lightLedColor(int ledInd, CRGB ledColor) {
  leds[ledInd] = (CRGB) ledColor;
  FastLED.show();
}

