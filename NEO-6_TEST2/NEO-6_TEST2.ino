#include <U8g2lib.h>

#ifdef U8X8_HAVE_HW_SPI
#include <SPI.h>
#endif
#ifdef U8X8_HAVE_HW_I2C
#include <Wire.h>
#endif

#include <SoftwareSerial.h>

SoftwareSerial mySerial(4, 3); // RX, TX of the MCU Board: TX of the module should go to Pin 4, RX of module goes to Pin 3
U8G2_SSD1306_128X64_NONAME_F_SW_I2C u8g2(U8G2_R0, /* clock=*/ SCL, /* data=*/ SDA, /* reset=*/ U8X8_PIN_NONE);   // All Boards without Reset of the Display

void setup() {
  Serial.begin(9600);
  mySerial.begin(9600);
  u8g2.begin();



}

void loop() { // run over and over

  String nmeaStatement = "ELDON TENORIO\n\nELDRIC";
  int lines = 1;
//
//    while (mySerial.available()) {
//  
//      if ((char)mySerial.read() == '\n') {

//      }
//      nmeaStatement = nmeaStatement + (char)mySerial.read();
//    }

  Serial.println(nmeaStatement);
  Serial.println("");

  // Treat the module response:

  int nmeaLength = nmeaStatement.length() + 1;
  char charArr[nmeaLength];
  nmeaStatement.toCharArray(charArr, nmeaLength);

  u8g2.clearBuffer();                   // clear the internal memory
  u8g2.setFontMode(1);
  u8g2.setFont(u8g2_font_u8glib_4_tr);  // choose a suitable font
  u8g2.drawStr(0,10,charArr);   // write something to the internal memory
  u8g2.sendBuffer();           // transfer internal memory to the display

  delay(1000);

}
