/*     Arduino Color Sensing Tutorial

    by Dejan Nedelkovski, www.HowToMechatronics.com

*/

#define S0 4
#define S1 5
#define S2 6
#define S3 7
#define sensorOut 8

int redValue;
int greenValue;
int blueValue;

int redPin    = 11;
int greenPin  = 10;
int bluePin    = 9;

int frequency = 0;

void setup() {

  pinMode(redPin, OUTPUT);
  pinMode(greenPin, OUTPUT);
  pinMode(bluePin, OUTPUT);

  pinMode(S0, OUTPUT);
  pinMode(S1, OUTPUT);
  pinMode(S2, OUTPUT);
  pinMode(S3, OUTPUT);
  pinMode(sensorOut, INPUT);

  // Setting frequency-scaling to 20%
  digitalWrite(S0, HIGH);
  digitalWrite(S1, LOW);

  Serial.begin(9600);
}

void loop() {
  // Setting red filtered photodiodes to be read
  digitalWrite(S2, LOW);
  digitalWrite(S3, LOW);
  
  // Reading the output frequency
  frequency = pulseIn(sensorOut, LOW);
  redValue = 255 - constrain(map(frequency, 28, 225, 0, 225), 0, 255);
  // Printing the value on the serial monitor
  Serial.print("R= ");//printing name
  Serial.print(redValue);//printing RED color frequency
  Serial.print("  ");
  delay(100);

  // Setting Green filtered photodiodes to be read
  digitalWrite(S2, HIGH);
  digitalWrite(S3, HIGH);
  // Reading the output frequency
  frequency = pulseIn(sensorOut, LOW);
  greenValue = 255 - constrain(map(frequency, 31, 286, 0, 225), 0, 255);
  // Printing the value on the serial monitor
  Serial.print("G= ");//printing name
  Serial.print(greenValue);//printing RED color frequency
  Serial.print("  ");
  delay(100);

  // Setting Blue filtered photodiodes to be read
  digitalWrite(S2, LOW);
  digitalWrite(S3, HIGH);
  // Reading the output frequency
  frequency = pulseIn(sensorOut, LOW);
  blueValue = 255 - constrain(map(frequency, 9, 84, 0, 225), 0, 255);
  // Printing the value on the serial monitor
  Serial.print("B= ");//printing name
  Serial.print(blueValue);//printing RED color frequency
  Serial.println("  ");

  setColor(redValue, greenValue, blueValue);
  delay(100);
}

void setColor(int red, int green, int blue)
{
#ifdef COMMON_ANODE
  red = 255 - red;
  green = 255 - green;
  blue = 255 - blue;
#endif
  analogWrite(redPin, red);
  analogWrite(greenPin, green);
  analogWrite(bluePin, blue);
}
