// #include <LiquidCrystal_I2C.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <dht.h>
#include <MyRealTimeClock.h>
MyRealTimeClock myRTC(5, 6, 7); // Assign Digital Pins CLK, DAT, RST

#define OLED_RESET 4
Adafruit_SSD1306 display(OLED_RESET);
//LiquidCrystal_I2C lcd(0x3F,16,2);  // set the LCD address to 0x27 for a 16 chars and 2 line display
dht DHT;

#define DHT11_PIN     8
#define COGASSENSOR_PIN A3

#if (SSD1306_LCDHEIGHT != 64)
#error("Height incorrect, please fix Adafruit_SSD1306.h!");
#endif

float m = -0.318; //Slope 
float b = 1.133; //Y-Intercept 

void setup(){
  Serial.begin(9600);
                //  lcd.init();
                //  lcd.noBacklight();
                //  delay(100);
                //  lcd.backlight();  
                //  outputToLCD("Preparing DHT11","and MQ9 Sensors");

  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);  // initialize with the I2C addr 0x3C (for the 128x32)
  display.clearDisplay();
}

void loop()
{


    
    float sensor_volt;
    float RS_air; //  Get the value of RS via in a clear air
    float R0;  // Get the value of R0 via in LPG
    float sensorValue;

    

  display.clearDisplay();
  int chk = DHT.read11(DHT11_PIN);
  

    /*--- Get a average data by testing 100 times ---*/
    for(int x = 0 ; x < 100 ; x++)
    {
        sensorValue = sensorValue + analogRead(COGASSENSOR_PIN);
    }
    sensorValue = sensorValue/100.0;
    /*-----------------------------------------------*/

    sensor_volt = sensorValue/1024*5.0;
    RS_air = (5.0-sensor_volt)/sensor_volt; // omit *RL
    R0 = RS_air/9.9; // The ratio of RS/R0 is 9.9 in LPG gas from Graph (Found using WebPlotDigitizer)
  
  double ppm_log = (log10(RS_air / R0)-b)/m; //Get ppm value in linear scale according to the the ratio value
  double ppm = pow(10, ppm_log); //Convert ppm value to log scale 
  double percentage = ppm/10000; //Convert to percentage 


  displayTime3();
  delay(5000);
  
  // -------------------------
  display.clearDisplay();
  display.setTextSize(1);
  display.setCursor(30,0);
  display.setTextColor(WHITE);
  display.print("Temperature");
  display.setCursor(20,10);
  display.setTextSize(2);
  display.print(DHT.temperature);
  display.print(" C");
  display.display();

  display.setTextSize(1);
  display.setCursor(35,30);
  display.setTextColor(WHITE);
  display.print("Humidity");
  display.setCursor(20,40);
  display.setTextSize(2);
  display.print(DHT.humidity);
  display.print(" %");
  display.display();
  delay(5000);
    
  // -------------------------------
  display.clearDisplay();
  display.setTextSize(1);
  display.setCursor(50,0);
  display.setTextColor(WHITE);
  display.print("ppm");
  display.setCursor(35,10);
  display.setTextSize(2);
  display.print(ppm);
  display.display();

  display.setTextSize(1);
  display.setCursor(25,30);
  display.setTextColor(WHITE);
  display.print("Concentration");
  display.setCursor(35,40);
  display.setTextSize(2);
  display.print(percentage);
  display.print("%");
  display.display();
  delay(5000);

  display.clearDisplay();
  display.setTextSize(1);
  display.setCursor(0,0);
  display.setTextColor(WHITE);
  display.println("Developed by");
  display.setTextSize(2);
  display.println("Eldon");
  display.print("Tenorio");
  display.display();
  delay(5000);
  
}

String addZeroes(int value, int lengthDigit){
  String sValue = (String)value;
  int valueLength = sValue.length();

  for(int i = 1; i<= (lengthDigit - valueLength); i++){
    sValue = "0" + sValue;
  }

  return sValue;
}


void displayTime3() {
  myRTC.updateTime();
  display.clearDisplay();
  display.setCursor(20,20);
  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.print(getDayOfTheWeekName(myRTC.dayofweek).substring(0,3) + ", ");
  display.println(addZeroes(myRTC.month,2) + "-" + addZeroes(myRTC.dayofmonth, 2) + "-" + addZeroes(myRTC.year, 2)); 
  display.setCursor(10,30);
  display.setTextSize(2);
  display.println(displayFormattedTime());
  display.display();
}

String displayFormattedTime(){
  String dayPeriod = " AM";
  String dayHours = myRTC.hours + "";

  // isShowColon = (myRTC.seconds % 2 == 0);
  if(myRTC.hours > 12){
    dayPeriod = " PM";
    dayHours = myRTC.hours - 12;
  }
  else{
    dayHours = myRTC.hours;
  }


  return (" " + dayHours + ":" + addZeroes(myRTC.minutes, 2) + dayPeriod);
}

String getDayOfTheWeekName(int iDayOfTheWeek){
  switch(iDayOfTheWeek){
    case 1:{
      return "Sunday";
      break;
    }
    case 2:{
      return "Monday";
      break;
    }
    case 3:{
      return "Tuesday";
      break;
    }
    case 4:{
      return "Wednesday";
      break;
    }
    case 5:{
      return "Thursday";
      break;
    }
    case 6:{
      return "Friday";
      break;
    }
    case 7:{
      return "Saturday";
      break;
    }
  }
}

String getMonthName(int iMonth){
  switch(iMonth){
    case 1:{
      return "January";
      break;
    }
   case 2:{
      return "February";
      break;
    }
   case 3:{
      return "March";
      break;
    }
   case 4:{
      return "April";
      break;
    }
   case 5:{
      return "May";
      break;
    }
   case 6:{
      return "June";
      break;
    }
   case 7:{
      return "July";
      break;
    }
   case 8:{
      return "August";
      break;
    }
   case 9:{
      return "September";
      break;
    }
   case 10:{
      return "October";
      break;
    }
   case 11:{
      return "November";
      break;
    }
   case 12:{
      return "December";
      break;
    }
  }
}
