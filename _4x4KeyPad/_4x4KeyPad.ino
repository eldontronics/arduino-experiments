
/*4x4 Matrix Keypad connected to Arduino
  This code prints the key pressed on the keypad to the serial port*/

#include <Keypad.h>
#include <LiquidCrystal_I2C.h>

LiquidCrystal_I2C lcd(0x3F, 16, 2);

const byte numRows = 4; //number of rows on the keypad
const byte numCols = 4; //number of columns on the keypad
//char keymap[numRows][numCols] =
//{
//  {'1', '4', '7', '*'},
//  {'2', '5', '8', '0'},
//  {'3', '6', '9', '#'},
//  {'A', 'B', 'C', 'D'}
//};

char keymap[numRows][numCols] =
{
  {'1', '2', '3', 'A'},
  {'4', '5', '6', 'B'},
  {'7', '8', '9', 'C'},
  {'*', '0', '#', 'D'}
};


//Code that shows the the keypad connections to the arduino terminals

byte rowPins[numRows] = {9, 8, 7, 6}; //Columns 0 to 3
byte colPins[numCols] = {5, 4, 3, 2}; //Rows 0 to 3

//initializes an instance of the Keypad class
Keypad myKeypad = Keypad(makeKeymap(keymap), rowPins, colPins, numRows, numCols);

bool isAccepted = false;

int maxCharLength = 4;
int charCount = 0;

String inputtedCode = "";
String storedKeyCode = "0921";

#define redLED    10
#define greenLED  11
#define blueLED   12
#define yellowLED 13

long prevMillis1 = 0;
long prevMillis2 = 0;

void setup()
{
  Serial.begin(9600);

  pinMode(redLED, OUTPUT);
  pinMode(greenLED, OUTPUT);
  pinMode(blueLED, OUTPUT);
  pinMode(yellowLED, OUTPUT);

  lcd.init();
  lcd.noBacklight();
  delay(1000);
  lcd.backlight();
  lcd.setCursor(0, 0);
  lcd.print(" Enter keycode:");
  lcd.setCursor(5, 1);

}

//If key is pressed, this key is stored in 'keypressed' variable
//If key is not equal to 'NO_KEY', then this key is printed out
//if count=17, then count is reset back to 0 (this means no key is pressed during the whole keypad scan process
void loop()
{

  char keypressed = myKeypad.getKey();
  if (keypressed != NO_KEY)
  {
    lcd.backlight();
    if (keypressed == '#') {
      if (inputtedCode == storedKeyCode) {
        lcd.clear();
        lcd.setCursor(0, 0);
        lcd.print("    Key code");
        lcd.setCursor(0, 1);
        lcd.print("    ACCEPTED");

        // set flag here ACCEPTED
        isAccepted = true;
        inputtedCode = "";
        charCount = 0;        

        // DO SOMETHING LIKE UNLOCK DOOR FOR 10 SECS, etc.

        //        digitalWrite(redLED, LOW);
        //        digitalWrite(greenLED, LOW);
        //        digitalWrite(blueLED, LOW);
        //        digitalWrite(yellowLED, LOW);

      }
      else {
        inputtedCode = "";
        charCount = 0;
        isAccepted = false;
        lcd.clear();
        lcd.setCursor(0, 0);
        lcd.print("    Key code");
        lcd.setCursor(0, 1);
        lcd.print("    REJECTED");
        delay(2000);
        lcd.clear();
        lcd.setCursor(0, 0);
        lcd.print(" Enter keycode:");
        lcd.setCursor(5, 1);

      }
    }
    else if (keypressed == '*') {
      Serial.println(keypressed);
      charCount = 0;
      inputtedCode = "";
      isAccepted = false;
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print(" Enter keycode:");
      lcd.setCursor(5, 1);

      //      digitalWrite(redLED, LOW);
      //      digitalWrite(greenLED, LOW);
      //      digitalWrite(blueLED, LOW);
      //      digitalWrite(yellowLED, LOW);

    }
    else {
      charCount++;
      if (charCount <= maxCharLength) {
        inputtedCode.concat(keypressed);
        lcd.print("*");
        Serial.println("Current code : " + inputtedCode);
      }
    }
  }

  //  if (isAccepted) {
  //    lcd.clear();
  //    lcd.noBacklight();
  //    // runDancingLights();
  //  }
}

void runDancingLights() {

  digitalWrite(redLED, HIGH);
  digitalWrite(greenLED, LOW);
  digitalWrite(blueLED, LOW);
  digitalWrite(yellowLED, LOW);
  delay(100);

  digitalWrite(redLED, LOW);
  digitalWrite(greenLED, HIGH);
  digitalWrite(blueLED, LOW);
  digitalWrite(yellowLED, LOW);
  delay(100);

  digitalWrite(redLED, LOW);
  digitalWrite(greenLED, LOW);
  digitalWrite(blueLED, HIGH);
  digitalWrite(yellowLED, LOW);
  delay(100);

  digitalWrite(redLED, LOW);
  digitalWrite(greenLED, LOW);
  digitalWrite(blueLED, LOW);
  digitalWrite(yellowLED, HIGH);
  delay(100);

  digitalWrite(redLED, LOW);
  digitalWrite(greenLED, LOW);
  digitalWrite(blueLED, HIGH);
  digitalWrite(yellowLED, LOW);
  delay(100);

  digitalWrite(redLED, LOW);
  digitalWrite(greenLED, HIGH);
  digitalWrite(blueLED, LOW);
  digitalWrite(yellowLED, LOW);
  delay(100);
}

