int pin1 = 2;
int pin2 = 3;                                    //                            --6--
int pin3 = 4;                                    //                         5 |     | 7
int pin4 = 5;                                    //                           |--4--|
int pin5 = 6;                                    //                         1 |     | 3
int pin6 = 7;                                    //                            --2--
int pin7 = 8;
int gnd1 = 11;                                 //                          gnd1 is display 1's gnd
int gnd2 = 9;                                   //                          gnd2 is display 2's gnd
int timer = 50;                               //   A timer, to run the for loop 10 times, which turns out as 1 second.
int value;                                        //   The value, part of the FADING display

// pin1 = E
// pin2 = D
// pin3 = C
// pin4 = F
// pin5 = F
// pin6 = A
// pin7 = B

void setup(){
  Serial.begin(9600);
  pinMode(pin1, OUTPUT);
  pinMode(pin2, OUTPUT);
  pinMode(pin3, OUTPUT);
  pinMode(pin4, OUTPUT);           //The following sets up all of the pins for use.
  pinMode(pin5, OUTPUT);
  pinMode(pin6, OUTPUT);
  pinMode(pin7, OUTPUT);
  pinMode(gnd1, OUTPUT);
  pinMode(gnd2, OUTPUT);  
}

void loop() {
    
//displayNumber(1, 1000, true);
//displayNumber(2, 1000, true);
//displayNumber(3, 1000, true);
//displayNumber(4, 1000, true);
//displayNumber(5, 1000, true);
//displayNumber(6, 1000, true);
//displayNumber(7, 1000, true);
//displayNumber(8, 1000, true);
//displayNumber(9, 1000, true);
//displayNumber(10, 1000, true);

  String number = "";
  char digits[2];
  
  for(int i = 0; i<=99; i++){
    number = (String)i;
    if(number.length() <2) number = "0" + number;
      number.toCharArray(digits, 3);
      for(int j = 1; j<= (1000 / 20); j++){
        showDigit(digits[1], 10, 0); //   
        showDigit(digits[0], 10, 1); // first in array placeholder = 1 and decrement.
      }
  }

  for(int i = 99; i>=0; i--){
      number = (String)i;
      if(number.length() < 2) number = "0" + number;
        number.toCharArray(digits, 3);
        for(int j = 1; j<= (1000 / 20); j++){
          showDigit(digits[1], 10, 0); //   
          showDigit(digits[0], 10, 1); // first in array placeholder = 1 and decrement.
        }
    }
  
}


void displayNumber(int number, int delayTime, bool preceedZero){
  String strNumber = (String) number;
  char charNumber[2];
  if(preceedZero && strNumber.length() < 2){
    strNumber = "0" + strNumber;
  }

   strNumber.toCharArray(charNumber, 3);
      
      for(int j = 1; j<= (delayTime / 20); j++){
        showDigit(charNumber[1], 10, 0);
        showDigit(charNumber[0], 10, 1);
      }

}


void showDigit(char digit, int delayTime, int placeholder){
  // 0 = ones
  // 1 = tens

  if(digit == '1'){
     for (int i=0; i< (delayTime / 10); i++){
       digitalWrite(pin1, B0);
       digitalWrite(pin2, B0);
       digitalWrite(pin3, B1);
       digitalWrite(pin4, B0);
       digitalWrite(pin5, B0);
       digitalWrite(pin6, B0);
       digitalWrite(pin7, B1);
  
        if(placeholder == 0){
          digitalWrite(gnd1, B1); // left
          digitalWrite(gnd2, B0); // right    
        }
       
        if(placeholder == 1){
          digitalWrite(gnd1, B0); // left
          digitalWrite(gnd2, B1); // right    
        }
       
       delay(10);
    }
  } // 1

 if(digit == '2'){
     for (int i=0; i< (delayTime / 10); i++){
       digitalWrite(pin1, B1);
       digitalWrite(pin2, B1);
       digitalWrite(pin3, B0);
       digitalWrite(pin4, B1);
       digitalWrite(pin5, B0);
       digitalWrite(pin6, B1);
       digitalWrite(pin7, B1);

        if(placeholder == 0){
          digitalWrite(gnd1, B1); // left
          digitalWrite(gnd2, B0); // right    
        }
       
        if(placeholder == 1){
          digitalWrite(gnd1, B0); // left
          digitalWrite(gnd2, B1); // right    
        }
       
       delay(10);
    }
  } // 2

 if(digit == '3'){
     for (int i=0; i< (delayTime / 10); i++){
       digitalWrite(pin1, B0);
       digitalWrite(pin2, B1);
       digitalWrite(pin3, B1);
       digitalWrite(pin4, B1);
       digitalWrite(pin5, B0);
       digitalWrite(pin6, B1);
       digitalWrite(pin7, B1);

        if(placeholder == 0){
          digitalWrite(gnd1, B1); // left
          digitalWrite(gnd2, B0); // right    
        }
       
        if(placeholder == 1){
          digitalWrite(gnd1, B0); // left
          digitalWrite(gnd2, B1); // right    
        }
       
       delay(10);
    }
  } // 3

 if(digit == '4'){
     for (int i=0; i< (delayTime / 10); i++){
       digitalWrite(pin1, B0);
       digitalWrite(pin2, B0);
       digitalWrite(pin3, B1);
       digitalWrite(pin4, B1);
       digitalWrite(pin5, B1);
       digitalWrite(pin6, B0);
       digitalWrite(pin7, B1);

        if(placeholder == 0){
          digitalWrite(gnd1, B1); // left
          digitalWrite(gnd2, B0); // right    
        }
       
        if(placeholder == 1){
          digitalWrite(gnd1, B0); // left
          digitalWrite(gnd2, B1); // right    
        }
       
       delay(10);
    }
  } // 4

 if(digit == '5'){
     for (int i=0; i< (delayTime / 10); i++){
       digitalWrite(pin1, B0);
       digitalWrite(pin2, B1);
       digitalWrite(pin3, B1);
       digitalWrite(pin4, B1);
       digitalWrite(pin5, B1);
       digitalWrite(pin6, B1);
       digitalWrite(pin7, B0);
     
        if(placeholder == 0){
          digitalWrite(gnd1, B1); // left
          digitalWrite(gnd2, B0); // right    
        }
       
        if(placeholder == 1){
          digitalWrite(gnd1, B0); // left
          digitalWrite(gnd2, B1); // right    
        }
       
       delay(10);
    }
  } // 5

 if(digit == '6'){
     for (int i=0; i< (delayTime / 10); i++){
        digitalWrite(pin1, B1);
        digitalWrite(pin2, B1);
        digitalWrite(pin3, B1);
        digitalWrite(pin4, B1);
        digitalWrite(pin5, B1);
        digitalWrite(pin6, B1);
        digitalWrite(pin7, B0);
       
        if(placeholder == 0){
          digitalWrite(gnd1, B1); // left
          digitalWrite(gnd2, B0); // right    
        }
       
        if(placeholder == 1){
          digitalWrite(gnd1, B0); // left
          digitalWrite(gnd2, B1); // right    
        }
       
       delay(10);
    }
  } // 6
  
 if(digit == '7'){
     for (int i=0; i< (delayTime / 10); i++){
       digitalWrite(pin1, B0);
       digitalWrite(pin2, B0);
       digitalWrite(pin3, B1);
       digitalWrite(pin4, B0);
       digitalWrite(pin5, B0);
       digitalWrite(pin6, B1);
       digitalWrite(pin7, B1);
       
        if(placeholder == 0){
          digitalWrite(gnd1, B1); // left
          digitalWrite(gnd2, B0); // right    
        }
       
        if(placeholder == 1){
          digitalWrite(gnd1, B0); // left
          digitalWrite(gnd2, B1); // right    
        }
       
       delay(10);
    }
  } // 7


 if(digit == '8'){
     for (int i=0; i< (delayTime / 10); i++){
        digitalWrite(pin1, B1);
        digitalWrite(pin2, B1);
        digitalWrite(pin3, B1);
        digitalWrite(pin4, B1);
        digitalWrite(pin5, B1);
        digitalWrite(pin6, B1);
        digitalWrite(pin7, B1);
          
        if(placeholder == 0){
          digitalWrite(gnd1, B1); // left
          digitalWrite(gnd2, B0); // right    
        }
       
        if(placeholder == 1){
          digitalWrite(gnd1, B0); // left
          digitalWrite(gnd2, B1); // right    
        }
       
       delay(10);
    }
  } // 8

if(digit == '9'){
     for (int i=0; i< (delayTime / 10); i++){
       digitalWrite(pin1, B0);
       digitalWrite(pin2, B0);
       digitalWrite(pin3, B1);
       digitalWrite(pin4, B1);
       digitalWrite(pin5, B1);
       digitalWrite(pin6, B1);
       digitalWrite(pin7, B1);
          
        if(placeholder == 0){
          digitalWrite(gnd1, B1); // left
          digitalWrite(gnd2, B0); // right    
        }
       
        if(placeholder == 1){
          digitalWrite(gnd1, B0); // left
          digitalWrite(gnd2, B1); // right    
        }
       
       delay(10);
    }
  } // 9

if(digit == '0'){
     for (int i=0; i< (delayTime / 10); i++){
       digitalWrite(pin1, B1);
       digitalWrite(pin2, B1);
       digitalWrite(pin3, B1);
       digitalWrite(pin4, B0);
       digitalWrite(pin5, B1);
       digitalWrite(pin6, B1);
       digitalWrite(pin7, B1);
          
        if(placeholder == 0){
          digitalWrite(gnd1, B1); // left
          digitalWrite(gnd2, B0); // right    
        }
       
        if(placeholder == 1){
          digitalWrite(gnd1, B0); // left
          digitalWrite(gnd2, B1); // right    
        }
       
       delay(10);
    }
  } // 0




  
}



void countDown(){
    for (int i=0; i<timer; i++){                     //   The for loop, for running the program 10 times.
   digitalWrite(pin1, B1);
   digitalWrite(pin2, B1);
   digitalWrite(pin3, B0);
   digitalWrite(pin4, B1);
   digitalWrite(pin5, B0);
   digitalWrite(pin6, B1);
   digitalWrite(pin7, B1);
   digitalWrite(gnd1, B0);                         
   digitalWrite(gnd2, B1);
   delay(10);                                        //          Writes 20 to the display.
   digitalWrite(pin1, B1);
   digitalWrite(pin2, B1);
   digitalWrite(pin3, B1);
   digitalWrite(pin4, B0);
   digitalWrite(pin5, B1);
   digitalWrite(pin6, B1);
   digitalWrite(pin7, B1);
   digitalWrite(gnd1, B1);
   digitalWrite(gnd2, B0);
   delay(10);
   
  }
  
for (int i=0; i<timer; i++){
   digitalWrite(pin1, B0);
   digitalWrite(pin2, B0);
   digitalWrite(pin3, B1);
   digitalWrite(pin4, B0);
   digitalWrite(pin5, B0);
   digitalWrite(pin6, B0);
   digitalWrite(pin7, B1);
   digitalWrite(gnd1, B0);
   digitalWrite(gnd2, B1);
   delay(10);                                        //          Writes 19 to the display.
   digitalWrite(pin1, B0);
   digitalWrite(pin2, B0);
   digitalWrite(pin3, B1);
   digitalWrite(pin4, B1);
   digitalWrite(pin5, B1);
   digitalWrite(pin6, B1);
   digitalWrite(pin7, B1);
   digitalWrite(gnd1, B1);
   digitalWrite(gnd2, B0);
   delay(10);
   
  }
  for (int i=0; i<timer; i++){
   digitalWrite(pin1, B0);
   digitalWrite(pin2, B0);
   digitalWrite(pin3, B1);
   digitalWrite(pin4, B0);
   digitalWrite(pin5, B0);
   digitalWrite(pin6, B0);
   digitalWrite(pin7, B1);
   digitalWrite(gnd1, B0);
   digitalWrite(gnd2, B1);
   delay(10);                                        //          Writes 18 to the display.
   digitalWrite(pin1, B1);
   digitalWrite(pin2, B1);
   digitalWrite(pin3, B1);
   digitalWrite(pin4, B1);
   digitalWrite(pin5, B1);
   digitalWrite(pin6, B1);
   digitalWrite(pin7, B1);
   digitalWrite(gnd1, B1);
   digitalWrite(gnd2, B0);
   delay(10);
   
  }
  for (int i=0; i<timer; i++){
   digitalWrite(pin1, B0);
   digitalWrite(pin2, B0);
   digitalWrite(pin3, B1);
   digitalWrite(pin4, B0);
   digitalWrite(pin5, B0);
   digitalWrite(pin6, B0);
   digitalWrite(pin7, B1);
   digitalWrite(gnd1, B0);
   digitalWrite(gnd2, B1);
   delay(10);                                        //          Writes 17 to the display.
   digitalWrite(pin1, B0);
   digitalWrite(pin2, B0);
   digitalWrite(pin3, B1);
   digitalWrite(pin4, B0);
   digitalWrite(pin5, B0);
   digitalWrite(pin6, B1);
   digitalWrite(pin7, B1);
   digitalWrite(gnd1, B1);
   digitalWrite(gnd2, B0);
   delay(10);
   
  }
  for (int i=0; i<timer; i++){
   digitalWrite(pin1, B0);
   digitalWrite(pin2, B0);
   digitalWrite(pin3, B1);
   digitalWrite(pin4, B0);
   digitalWrite(pin5, B0);
   digitalWrite(pin6, B0);
   digitalWrite(pin7, B1);
   digitalWrite(gnd1, B0);
   digitalWrite(gnd2, B1);
   delay(10);                                        //          Writes 16 to the display.
   digitalWrite(pin1, B1);
   digitalWrite(pin2, B1);
   digitalWrite(pin3, B1);
   digitalWrite(pin4, B1);
   digitalWrite(pin5, B1);
   digitalWrite(pin6, B1);
   digitalWrite(pin7, B0);
   digitalWrite(gnd1, B1);
   digitalWrite(gnd2, B0);
   delay(10);
   
  }
  for (int i=0; i<timer; i++){
   digitalWrite(pin1, B0);
   digitalWrite(pin2, B0);
   digitalWrite(pin3, B1);
   digitalWrite(pin4, B0);
   digitalWrite(pin5, B0);
   digitalWrite(pin6, B0);
   digitalWrite(pin7, B1);
   digitalWrite(gnd1, B0);
   digitalWrite(gnd2, B1);
   delay(10);                                        //          Writes 15 to the display.
   digitalWrite(pin1, B0);
   digitalWrite(pin2, B1);
   digitalWrite(pin3, B1);
   digitalWrite(pin4, B1);
   digitalWrite(pin5, B1);
   digitalWrite(pin6, B1);
   digitalWrite(pin7, B0);
   digitalWrite(gnd1, B1);
   digitalWrite(gnd2, B0);
   delay(10);
   
  }
  for (int i=0; i<timer; i++){
   digitalWrite(pin1, B0);
   digitalWrite(pin2, B0);
   digitalWrite(pin3, B1);
   digitalWrite(pin4, B0);
   digitalWrite(pin5, B0);
   digitalWrite(pin6, B0);
   digitalWrite(pin7, B1);
   digitalWrite(gnd1, B0);
   digitalWrite(gnd2, B1);
   delay(10);                                        //          Writes 14 to the display.
   digitalWrite(pin1, B0);
   digitalWrite(pin2, B0);
   digitalWrite(pin3, B1);
   digitalWrite(pin4, B1);
   digitalWrite(pin5, B1);
   digitalWrite(pin6, B0);
   digitalWrite(pin7, B1);
   digitalWrite(gnd1, B1);
   digitalWrite(gnd2, B0);
   delay(10);
   
  }
  for (int i=0; i<timer; i++){
   digitalWrite(pin1, B0);
   digitalWrite(pin2, B0);
   digitalWrite(pin3, B1);
   digitalWrite(pin4, B0);
   digitalWrite(pin5, B0);
   digitalWrite(pin6, B0);
   digitalWrite(pin7, B1);
   digitalWrite(gnd1, B0);
   digitalWrite(gnd2, B1);
   delay(10);                                        //          Writes 13 to the display.
   digitalWrite(pin1, B0);
   digitalWrite(pin2, B1);
   digitalWrite(pin3, B1);
   digitalWrite(pin4, B1);
   digitalWrite(pin5, B0);
   digitalWrite(pin6, B1);
   digitalWrite(pin7, B1);
   digitalWrite(gnd1, B1);
   digitalWrite(gnd2, B0);
   delay(10);
   
  }
  for (int i=0; i<timer; i++){
   digitalWrite(pin1, B0);
   digitalWrite(pin2, B0);
   digitalWrite(pin3, B1);
   digitalWrite(pin4, B0);
   digitalWrite(pin5, B0);
   digitalWrite(pin6, B0);
   digitalWrite(pin7, B1);
   digitalWrite(gnd1, B0);
   digitalWrite(gnd2, B1);
   delay(10);                                        //          Writes 12 to the display.
   digitalWrite(pin1, B1);
   digitalWrite(pin2, B1);
   digitalWrite(pin3, B0);
   digitalWrite(pin4, B1);
   digitalWrite(pin5, B0);
   digitalWrite(pin6, B1);
   digitalWrite(pin7, B1);
   digitalWrite(gnd1, B1);
   digitalWrite(gnd2, B0);
   delay(10);
   
  }
  for (int i=0; i<timer; i++){
   digitalWrite(pin1, B0);
   digitalWrite(pin2, B0);
   digitalWrite(pin3, B1);
   digitalWrite(pin4, B0);
   digitalWrite(pin5, B0);
   digitalWrite(pin6, B0);
   digitalWrite(pin7, B1);
   digitalWrite(gnd1, B0);
   digitalWrite(gnd2, B1);
   delay(10);                                        //          Writes 11 to the display.
   digitalWrite(pin1, B0);
   digitalWrite(pin2, B0);
   digitalWrite(pin3, B1);
   digitalWrite(pin4, B0);
   digitalWrite(pin5, B0);
   digitalWrite(pin6, B0);
   digitalWrite(pin7, B1);
   digitalWrite(gnd1, B1);
   digitalWrite(gnd2, B0);
   delay(10);
   
  }
  for (int i=0; i<timer; i++){
   digitalWrite(pin1, B0);
   digitalWrite(pin2, B0);
   digitalWrite(pin3, B1);
   digitalWrite(pin4, B0);
   digitalWrite(pin5, B0);
   digitalWrite(pin6, B0);
   digitalWrite(pin7, B1);
   digitalWrite(gnd1, B0);
   digitalWrite(gnd2, B1);
   delay(10);                                        //          Writes 10 to the display.
   digitalWrite(pin1, B1);
   digitalWrite(pin2, B1);
   digitalWrite(pin3, B1);
   digitalWrite(pin4, B0);
   digitalWrite(pin5, B1);
   digitalWrite(pin6, B1);
   digitalWrite(pin7, B1);
   digitalWrite(gnd1, B1);
   digitalWrite(gnd2, B0);
   delay(10);
   
  }
  for (int i=0; i<timer; i++){
   digitalWrite(pin1, B1);
   digitalWrite(pin2, B1);
   digitalWrite(pin3, B1);
   digitalWrite(pin4, B0);
   digitalWrite(pin5, B1);
   digitalWrite(pin6, B1);
   digitalWrite(pin7, B1);
   digitalWrite(gnd1, B0);
   digitalWrite(gnd2, B1);
   delay(10);                                        //          Writes 09 to the display.
   digitalWrite(pin1, B0);
   digitalWrite(pin2, B0);
   digitalWrite(pin3, B1);
   digitalWrite(pin4, B1);
   digitalWrite(pin5, B1);
   digitalWrite(pin6, B1);
   digitalWrite(pin7, B1);
   digitalWrite(gnd1, B1);
   digitalWrite(gnd2, B0);
   delay(10);
   
  }
  
  for (int i=0; i<timer; i++){
   digitalWrite(pin1, B1);
   digitalWrite(pin2, B1);
   digitalWrite(pin3, B1);
   digitalWrite(pin4, B0);
   digitalWrite(pin5, B1);
   digitalWrite(pin6, B1);
   digitalWrite(pin7, B1);
   digitalWrite(gnd1, B0);
   digitalWrite(gnd2, B1);
   delay(10);                                        //          Writes 08 to the display.
   digitalWrite(pin1, B1);
   digitalWrite(pin2, B1);
   digitalWrite(pin3, B1);
   digitalWrite(pin4, B1);
   digitalWrite(pin5, B1);
   digitalWrite(pin6, B1);
   digitalWrite(pin7, B1);
   digitalWrite(gnd1, B1);
   digitalWrite(gnd2, B0);
   delay(10);
   
  }
  for (int i=0; i<timer; i++){
   digitalWrite(pin1, B1);
   digitalWrite(pin2, B1);
   digitalWrite(pin3, B1);
   digitalWrite(pin4, B0);
   digitalWrite(pin5, B1);
   digitalWrite(pin6, B1);
   digitalWrite(pin7, B1);
   digitalWrite(gnd1, B0);
   digitalWrite(gnd2, B1);
   delay(10);                                        //          Writes 07 to the display.
   digitalWrite(pin1, B0);
   digitalWrite(pin2, B0);
   digitalWrite(pin3, B1);
   digitalWrite(pin4, B0);
   digitalWrite(pin5, B0);
   digitalWrite(pin6, B1);
   digitalWrite(pin7, B1);
   digitalWrite(gnd1, B1);
   digitalWrite(gnd2, B0);
   delay(10);
   
  }
  for (int i=0; i<timer; i++){
   digitalWrite(pin1, B1);
   digitalWrite(pin2, B1);
   digitalWrite(pin3, B1);
   digitalWrite(pin4, B0);
   digitalWrite(pin5, B1);
   digitalWrite(pin6, B1);
   digitalWrite(pin7, B1);
   digitalWrite(gnd1, B0);
   digitalWrite(gnd2, B1);
   delay(10);                                        //          Writes 06 to the display.
   digitalWrite(pin1, B1);
   digitalWrite(pin2, B1);
   digitalWrite(pin3, B1);
   digitalWrite(pin4, B1);
   digitalWrite(pin5, B1);
   digitalWrite(pin6, B1);
   digitalWrite(pin7, B0);
   digitalWrite(gnd1, B1);
   digitalWrite(gnd2, B0);
   delay(10);
   
  }
  for (int i=0; i<timer; i++){
   digitalWrite(pin1, B1);
   digitalWrite(pin2, B1);
   digitalWrite(pin3, B1);
   digitalWrite(pin4, B0);
   digitalWrite(pin5, B1);
   digitalWrite(pin6, B1);
   digitalWrite(pin7, B1);
   digitalWrite(gnd1, B0);
   digitalWrite(gnd2, B1);
   delay(10);                                        //          Writes 05 to the display.
   digitalWrite(pin1, B0);
   digitalWrite(pin2, B1);
   digitalWrite(pin3, B1);
   digitalWrite(pin4, B1);
   digitalWrite(pin5, B1);
   digitalWrite(pin6, B1);
   digitalWrite(pin7, B0);
   digitalWrite(gnd1, B1);
   digitalWrite(gnd2, B0);
   delay(10);
   
  }
  for (int i=0; i<timer; i++){
   digitalWrite(pin1, B1);
   digitalWrite(pin2, B1);
   digitalWrite(pin3, B1);
   digitalWrite(pin4, B0);
   digitalWrite(pin5, B1);
   digitalWrite(pin6, B1);
   digitalWrite(pin7, B1);
   digitalWrite(gnd1, B0);
   digitalWrite(gnd2, B1);
   delay(10);                                        //          Writes 04 to the display.
   digitalWrite(pin1, B0);
   digitalWrite(pin2, B0);
   digitalWrite(pin3, B1);
   digitalWrite(pin4, B1);
   digitalWrite(pin5, B1);
   digitalWrite(pin6, B0);
   digitalWrite(pin7, B1);
   digitalWrite(gnd1, B1);
   digitalWrite(gnd2, B0);
   delay(10);
   
  }
  for (int i=0; i<timer; i++){
   digitalWrite(pin1, B1);
   digitalWrite(pin2, B1);
   digitalWrite(pin3, B1);
   digitalWrite(pin4, B0);
   digitalWrite(pin5, B1);
   digitalWrite(pin6, B1);
   digitalWrite(pin7, B1);
   digitalWrite(gnd1, B0);
   digitalWrite(gnd2, B1);
   delay(10);                                        //          Writes 03 to the display.
   digitalWrite(pin1, B0);
   digitalWrite(pin2, B1);
   digitalWrite(pin3, B1);
   digitalWrite(pin4, B1);
   digitalWrite(pin5, B0);
   digitalWrite(pin6, B1);
   digitalWrite(pin7, B1);
   digitalWrite(gnd1, B1);
   digitalWrite(gnd2, B0);
   delay(10);
   
  }
  for (int i=0; i<timer; i++){
   digitalWrite(pin1, B1);
   digitalWrite(pin2, B1);
   digitalWrite(pin3, B1);
   digitalWrite(pin4, B0);
   digitalWrite(pin5, B1);
   digitalWrite(pin6, B1);
   digitalWrite(pin7, B1);
   digitalWrite(gnd1, B0);
   digitalWrite(gnd2, B1);
   delay(10);                                        //          Writes 02 to the display.
   digitalWrite(pin1, B1);
   digitalWrite(pin2, B1);
   digitalWrite(pin3, B0);
   digitalWrite(pin4, B1);
   digitalWrite(pin5, B0);
   digitalWrite(pin6, B1);
   digitalWrite(pin7, B1);
   digitalWrite(gnd1, B1);
   digitalWrite(gnd2, B0);
   delay(10);
   
  }
  for (int i=0; i<timer; i++){
   digitalWrite(pin1, B1);
   digitalWrite(pin2, B1);
   digitalWrite(pin3, B1);
   digitalWrite(pin4, B0);
   digitalWrite(pin5, B1);
   digitalWrite(pin6, B1);
   digitalWrite(pin7, B1);
   digitalWrite(gnd1, B0);
   digitalWrite(gnd2, B1);
   delay(10);                                        //          Writes 01 to the display.
   digitalWrite(pin1, B0);
   digitalWrite(pin2, B0);
   digitalWrite(pin3, B1);
   digitalWrite(pin4, B0);
   digitalWrite(pin5, B0);
   digitalWrite(pin6, B0);
   digitalWrite(pin7, B1);
   digitalWrite(gnd1, B1);
   digitalWrite(gnd2, B0);
   delay(10);
   
  }
  for (int i=0; i<timer; i++){
   digitalWrite(pin1, B1);
   digitalWrite(pin2, B1);
   digitalWrite(pin3, B1);
   digitalWrite(pin4, B0);
   digitalWrite(pin5, B1);
   digitalWrite(pin6, B1);
   digitalWrite(pin7, B1);
   digitalWrite(gnd1, B0);
   digitalWrite(gnd2, B1);
   delay(10);                                        //          Writes 00 to the display.
   digitalWrite(pin1, B1);
   digitalWrite(pin2, B1);
   digitalWrite(pin3, B1);
   digitalWrite(pin4, B0);
   digitalWrite(pin5, B1);
   digitalWrite(pin6, B1);
   digitalWrite(pin7, B1);
   digitalWrite(gnd1, B1);
   digitalWrite(gnd2, B0);
   delay(10);
   
  }
  

}

