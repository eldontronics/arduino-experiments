/*
  Cayenne ESP8266 Shield WiFi Example
  Adapted from Blynk's ESP8266_Shield_HardSer Example

  This sketch connects to the Cayenne server using an ESP8266 WiFi module as a shield connected
  via a hardware serial to an Arduino.

  You should install the ESP8266HardwareSerial.zip library via the Arduino IDE (Sketch->Include Library->Add .ZIP Library)
  from the Cayenne extras/libraries folder (e.g. My Documents\Arduino\libraries\Cayenne\extras\libraries) to compile this example.

  NOTE: Ensure a stable serial connection to ESP8266!
  Firmware version 1.0.0 (AT v0.22) or later is needed.
  You can change ESP baud rate. Connect to AT console and call:
  AT+UART_DEF=115200,8,1,0,0

  For Cayenne Dashboard widgets using digital or analog pins this sketch will automatically
  send data on those pins to the Cayenne server. If the widgets use Virtual Channels, data
  should be sent to those channels using virtualWrites. Examples for sending and receiving
  Virtual Channel data are under the Basics folder.
*/

//#define CAYENNE_DEBUG // Uncomment to show debug messages
//#define CAYENNE_PRINT Serial1 // Comment this out to disable prints and save space
#include <CayenneESP8266Shield.h>

// Cayenne authentication token. This should be obtained from the Cayenne Dashboard.
char token[] = "l88yaoh1yk"; // This is the Auth Token l88yaoh1yk, upsybzrbna

// Your network name and password.
char ssid[] = "Eldric";
char password[] = "daomingerdec8";

#define VIRTUAL_PIN V9

// Set ESP8266 Serial object
#define EspSerial Serial

ESP8266 wifi(EspSerial);

int ldrPin    = A0;
int ldrValue  = 0;

void setup()
{
  EspSerial.begin(115200);
  Cayenne.begin(token, wifi, ssid, password);
}

void loop()
{
  Cayenne.run();
}

// This function is called when the Cayenne widget requests data for the Virtual Pin.
CAYENNE_OUT(VIRTUAL_PIN)
{
  ldrValue = analogRead(ldrPin);
  // Read data from the sensor and send it to the virtual channel here.
  // You can write data using virtualWrite or other Cayenne write functions.
  // For example, to send a temperature in Celsius you can use the following:
  Cayenne.virtualWrite(VIRTUAL_PIN, map(ldrValue, 0, 1023, 0, 100));
}
