/*
  Controlling a servo position using a potentiometer (variable resistor)
  by Michal Rinott <http://people.interaction-ivrea.it/m.rinott>

  modified on 8 Nov 2013
  by Scott Fitzgerald
  http://www.arduino.cc/en/Tutorial/Knob
*/

#include <Servo.h>

Servo myservo;  // create servo object to control a servo


void setup() {
  myservo.attach(9);  // attaches the servo on pin 9 to the servo object
  myservo.write(0);
  delay(1000);
}

void loop() {
 
turnOnSwitch();
delay(2000);
turnOffSwitch();
delay(2000);


}


void turnOnSwitch() {
  for (int i = 0; i <= 180; i++) {
    myservo.write(i);
    delay(3);
  }
}

void turnOffSwitch() {
  for (int i = 180; i >= 0; i--) {
    myservo.write(i);
    delay(3);
  }
}
