float vSource   =   4.69; // 5.0V = 1023 Value;
int   vOutValue =   0;
int   vInPin    =   A0;
float vOut      =   0.0;
float amp       =   0.0;
float vDiff     =   0.0;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(vInPin, INPUT);
}

void loop() {
  vOutValue = analogRead(vInPin);
  vOut = ((vSource) / 1023) * vOutValue;
  amp = ((vSource - vOut) * 1000 / 10);

  Serial.print("Voltage in : ");
  Serial.println(vSource);

  Serial.print("Voltage out : ");
  Serial.println(vOut);

  Serial.print("Voltage Difference : ");
  Serial.println(vSource - vOut);
  Serial.println("");

  Serial.print("Ampered (mA) : ");
  Serial.println(amp);
  Serial.println("");


  delay(1000);
}
