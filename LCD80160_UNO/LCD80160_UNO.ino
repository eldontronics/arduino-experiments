#include <SPI.h>
#include <SD.h>
#include "LCD_Driver.h"
#include "GUI_Paint.h"
#include "image.h"


#define BUFFPIXEL   64

void setup()
{
  Serial.begin(115200);

  Config_Init();
  LCD_Init();
  LCD_Clear(BLACK);
  LCD_SetBacklight(100);

  Paint_NewImage(LCD_WIDTH, LCD_HEIGHT, 0, BLACK);
  Paint_Clear(BLACK);
  Paint_SetRotate(180);

  Serial.print("Initializing SD card...");
  if (!SD.begin(DEV_SD_CS)) {
    Paint_DrawString_EN(0, 0, "SD Card Failed.", &Font8, BLACK, GREEN);
    return;
  }


  Paint_DrawString_EN(0, 0, "SD Card OK...", &Font8, BLACK, GREEN);
  delay(500);
  Paint_DrawString_EN(0, 10, "Fetching images...", &Font8, BLACK, GREEN);
  delay(500);
  Paint_DrawString_EN(0, 20, "Displaying images...", &Font8, BLACK, GREEN);
  delay(500);

  //  Paint_DrawString_EN(0, 0, "123", &Font24, 0x000f, 0xfff0);
  //  Paint_DrawString_EN(0, 0, "Initializing...", &Font8, BLACK, GREEN);
  //  Paint_DrawString_CN(20, 45, "微雪电子",  &Font24CN, WHITE, RED);
  //  Paint_DrawRectangle(70, 10, 100, 40, RED, DRAW_FILL_EMPTY, DOT_PIXEL_2X2 );
  //  Paint_DrawLine(70, 10, 100, 40, MAGENTA, LINE_STYLE_SOLID, DOT_PIXEL_2X2);
  //  Paint_DrawLine(100, 10, 70, 40, MAGENTA, LINE_STYLE_SOLID, DOT_PIXEL_2X2);
  //  Paint_DrawImage(gImage_40X40, 120, 0, 40, 40);
  
  delay(1500);

}
void loop()
{
    displayImage("BERBER1.BMP");
    delay(2000);
    displayImage("BERBER2.BMP");
    delay(2000);
    displayImage("BERBER3.BMP");
    delay(2000);
    displayImage("MOMBER1.BMP");
    delay(2000);
    displayImage("DADBER1.BMP");
    delay(2000);
    displayImage("FAMBER1.BMP");
    delay(2000);
    displayImage("FAMBER2.BMP");
    delay(2000);
    displayImage("FAMBER3.BMP");
    delay(2000);
    displayImage("60180CAP.BMP");
    delay(2000);
    displayImage("IOSWP1.BMP");
    delay(2000);
    displayImage("FUJI1.BMP");
    delay(2000);
    displayImage("FUJI2.BMP");
}

void bmpDraw(String filename, uint8_t x, uint16_t y) {

  File     bmpFile;
  int      bmpWidth, bmpHeight;   // W+H in pixels
  uint8_t  bmpDepth;              // Bit depth (currently must be 24)
  uint32_t bmpImageoffset;        // Start of image data in file
  uint32_t rowSize;               // Not always = bmpWidth; may have padding
  uint8_t  sdbuffer[3 * BUFFPIXEL]; // pixel buffer (R+G+B per pixel)
  uint8_t  buffidx = sizeof(sdbuffer); // Current position in sdbuffer
  boolean  goodBmp = false;       // Set to true on valid header parse
  boolean  flip    = true;        // BMP is stored bottom-to-top
  int      w, h, row, col;
  uint8_t  r, g, b;
  uint32_t pos = 0, startTime = millis();

  //  if ((x >= tft.width()) || (y >= tft.height())) return;

  Serial.println();
  Serial.print(F("Loading image '"));
  Serial.print(filename);
  Serial.println('\'');

  // Open requested file on SD card
  if ((bmpFile = SD.open(filename)) == NULL) {
    Serial.print(F("File not found"));
    return;
  }

  // Parse BMP header
  if (read16(bmpFile) == 0x4D42) { // BMP signature
    Serial.print(F("File size: ")); Serial.println(read32(bmpFile));
    (void)read32(bmpFile); // Read & ignore creator bytes
    bmpImageoffset = read32(bmpFile); // Start of image data
    Serial.print(F("Image Offset: ")); Serial.println(bmpImageoffset, DEC);
    // Read DIB header
    Serial.print(F("Header size: ")); Serial.println(read32(bmpFile));

    bmpWidth  = read32(bmpFile);
    bmpHeight = read32(bmpFile);


    if (read16(bmpFile) == 1) { // # planes -- must be '1'
      bmpDepth = read16(bmpFile); // bits per pixel
      Serial.print(F("Bit Depth: ")); Serial.println(bmpDepth);
      if ((bmpDepth == 24) && (read32(bmpFile) == 0)) { // 0 = uncompressed

        goodBmp = true; // Supported BMP format -- proceed!
        Serial.print(F("Image size: "));
        Serial.print(bmpWidth);
        Serial.print('x');
        Serial.println(bmpHeight);

        // BMP rows are padded (if needed) to 4-byte boundary
        rowSize = (bmpWidth * 3 + 3) & ~3;

        // If bmpHeight is negative, image is in top-down order.
        // This is not canon but has been observed in the wild.
        if (bmpHeight < 0) {
          bmpHeight = -bmpHeight;
          flip      = false;
        }

        // Crop area to be loaded
        w = bmpWidth;
        h = bmpHeight;

        for (row = 0; row < h; row++) { // For each scanline...

          if (flip) {
            pos = bmpImageoffset + (bmpHeight - 1 - row) * rowSize;
          }
          else {
            pos = bmpImageoffset + row * rowSize;
          }
          if (bmpFile.position() != pos) { // Need seek?
            bmpFile.seek(pos);
            buffidx = sizeof(sdbuffer); // Force buffer reload
          }

          for (col = 0; col < w; col++) { // For each pixel...
            // Time to read more pixel data?
            if (buffidx >= sizeof(sdbuffer)) { // Indeed
              bmpFile.read(sdbuffer, sizeof(sdbuffer));
              buffidx = 0; // Set index to beginning
            }

            // Convert pixel from BMP to TFT format, push to display
            b = sdbuffer[buffidx++];
            g = sdbuffer[buffidx++];
            r = sdbuffer[buffidx++];

            uint16_t pixelColor = (((31 * (r + 4)) / 255) << 11) |
                                  (((63 * (g + 2)) / 255) << 5) |
                                  ((31 * (b + 4)) / 255);

            // Switch to get the correct orientation
            Paint_SetPixel(y + col, x + row, pixelColor);

          } // end pixel
        } // end scanline

        Serial.print(F("Loaded in "));
        Serial.print(millis() - startTime);
        Serial.println(" ms");
      } // end goodBmp

      Serial.print(r); Serial.print(",");
      Serial.print(g); Serial.print(",");
      Serial.println(b);
    }
  }

  bmpFile.close();
  if (!goodBmp) Serial.println(F("BMP format not recognized."));
}


// These read 16- and 32-bit types from the SD card file.
// BMP data is stored little-endian, Arduino is little-endian too.
// May need to reverse subscript order if porting elsewhere.

uint16_t read16(File f) {
  uint16_t result;
  ((uint8_t *)&result)[0] = f.read(); // LSB
  ((uint8_t *)&result)[1] = f.read(); // MSB
  return result;
}

uint32_t read32(File f) {
  uint32_t result;
  ((uint8_t *)&result)[0] = f.read(); // LSB
  ((uint8_t *)&result)[1] = f.read();
  ((uint8_t *)&result)[2] = f.read();
  ((uint8_t *)&result)[3] = f.read(); // MSB
  return result;
}


void printDirectory(File dir, int numTabs) {
  while (true) {

    File entry =  dir.openNextFile();
    if (! entry) {
      // no more files
      break;
    }
    for (uint8_t i = 0; i < numTabs; i++) {
      Serial.print('\t');
    }
    Serial.print(entry.name());
    if (entry.isDirectory()) {
      Serial.println("/");
      printDirectory(entry, numTabs + 1);
    } else {
      // files have sizes, directories do not
      Serial.print("\t\t");
      Serial.println(entry.size(), DEC);
    }
    entry.close();
  }
}


void displayImage(String filename) {

  //  File root = SD.open("/");
  //  File entry =  root.openNextFile();
  //
  //  if (! entry) {
  //    root.rewindDirectory();
  //    delay(10);
  //  }

  bmpDraw(filename, 0, 0);
  //  entry.close();
  //  root.close();

}
