/*************************************************************
  Download latest Blynk library here:
    https://github.com/blynkkk/blynk-library/releases/latest

  Blynk is a platform with iOS and Android apps to control
  Arduino, Raspberry Pi and the likes over the Internet.
  You can easily build graphic interfaces for all your
  projects by simply dragging and dropping widgets.

    Downloads, docs, tutorials: http://www.blynk.cc
    Sketch generator:           http://examples.blynk.cc
    Blynk community:            http://community.blynk.cc
    Follow us:                  http://www.fb.com/blynkapp
                                http://twitter.com/blynk_app

  Blynk library is licensed under MIT license
  This example code is in public domain.

 *************************************************************

  You’ll need:
   - Blynk App (download from AppStore or Google Play)
   - NodeMCU board
   - Decide how to connect to Blynk
     (USB, Ethernet, Wi-Fi, Bluetooth, ...)

  There is a bunch of great example sketches included to show you how to get
  started. Think of them as LEGO bricks  and combine them as you wish.
  For example, take the Ethernet Shield sketch and combine it with the
  Servo example, or choose a USB sketch and add a code from SendData
  example.
 *************************************************************/

/* Comment this out to disable prints and save space */
#define BLYNK_PRINT Serial

#include <ESP8266WiFi.h>
#include <BlynkSimpleEsp8266.h>

#include <Wire.h>  // Only needed for Arduino 1.6.5 and earlier
#include "SSD1306.h" // alias for `#include "SSD1306Wire.h"`

SSD1306  display(0x3c, D1, D2);

#include "DHT.h"

BlynkTimer timer;
int brightnessPercent;
bool isSoundDetected = false;

#define DHTTYPE DHT11
const int DHTPin = D3;
DHT dht(DHTPin, DHTTYPE);

// You should get Auth Token in the Blynk App.
// Go to the Project Settings (nut icon).
char auth[] = "bd887783c46d41fca3b273e8e8aaa9fb";

// Your WiFi credentials.
// Set password to "" for open networks.
char ssid[] = "Eldric";
char pass[] = "daomingerdec8";

void setup()
{
  // Debug console
  Serial.begin(9600);

  dht.begin();
  display.init();

  timer.setInterval(100L, sendSensorData);

  display.flipScreenVertically();
  display.setFont(ArialMT_Plain_10);

  display.setTextAlignment(TEXT_ALIGN_LEFT);
  display.setFont(ArialMT_Plain_10);
  display.drawString(0, 0, "NodeMCU with Blynk");
  display.drawString(0, 10, "Control devices wirelessly!");
  display.drawString(0, 20, "The Internet of Things (IoT)");
  display.drawString(0, 40, "Connecting to the Internet...");
  display.display();

  Blynk.begin(auth, ssid, pass);
  // You can also specify server:
  //Blynk.begin(auth, ssid, pass, "blynk-cloud.com", 8442);
  //Blynk.begin(auth, ssid, pass, IPAddress(192,168,1,100), 8442);
  delay(5000);
}


void sendSensorData() {

  display.clear();
  int  h = dht.readHumidity();
  int t = dht.readTemperature();


  Blynk.virtualWrite(V1, t);
  Blynk.virtualWrite(V2, h);

  if (digitalRead(D5) == HIGH) {
    display.drawString(0, 0, "Red LED : ON");
  }
  else {
    display.drawString(0, 0, "Red LED : OFF");
  }

  if (digitalRead(D6) == HIGH) {
    display.drawString(0, 10, "Blue LED : ON");
  }
  else {
    display.drawString(0, 10, "Blue LED : OFF");
  }

  if (digitalRead(D7) == HIGH) {
    display.drawString(0, 20, "Green LED : ON");
  }
  else {
    display.drawString(0, 20, "Green LED : OFF");
  }

  if (digitalRead(D8) == HIGH) {
    display.drawString(0, 30, "Sound detected : YES");
    Blynk.virtualWrite(V4, true);
  }
  else {
    display.drawString(0, 30, "Sound detected : NO");
    Blynk.virtualWrite(V4, false);
  }



  brightnessPercent = ((int)((analogRead(0) * 100)) / 1023);
  Blynk.virtualWrite(V3, brightnessPercent);

  display.drawString(0, 40, "Brightness Level : ");
  display.drawString(87, 40, (String) analogRead(0));


  display.drawString(0, 50, "T: " + (String) t);
  display.drawString(50, 50, "H: " + (String) h);

  display.display();

  delay(100);
}

void loop()
{
  Blynk.run();
  timer.run();
}
