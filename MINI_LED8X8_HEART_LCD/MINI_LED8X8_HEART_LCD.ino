#include <LiquidCrystal_I2C.h>


/*
 * 8x8 led array bitmaps
 * A simple eye that looks left, right and blinks
 */

// Initialize the LCD
LiquidCrystal_I2C lcd(0x3E,16,2);  // set the LCD address to 0x27 for a 16 chars and 2 line display
 
// Arduino pin definitions - (ordered to keep wiring tidy)
int col[] = {14,10,4,12,9,5,8,6};    //columns - driven LOW for on
int row[] = {11,7,3,15,2,13,16,17};  //rows - driven HIGH for on

// bitmaps of images to display
//byte ledBitmap[1][8] = {B01100110,B11111111,B11111111,B11111111,B01111110,B00111100,B00011000,B00000000};

byte ledBitmap[9][8] = {
  B01100110,B11111111,B11111111,B11111111,B01111110,B00111100,B00011000,B00000000, // Heart
  B00000000,B00000000,B00000000,B00000000,B00000000,B00000000,B00000000,B00000000, // blank
  B01100110,B11111111,B11111111,B11111111,B01111110,B00111100,B00011000,B00000000, // Heart
  B00000000,B00000000,B00000000,B00000000,B00000000,B00000000,B00000000,B00000000, // blank
  B11111111,B11111111,B11000000,B11111111,B11111111,B11000000,B11111111,B11111111, // E
  B11000000,B11000000,B11000000,B11000000,B11000000,B11000000,B11111111,B11111111, // L
  B11111110,B11111111,B11000011,B11000011,B11000011,B11000011,B11111111,B11111110, // D
  B11111111,B11111111,B11000011,B11000011,B11000011,B11000011,B11111111,B11111111, // O
  B11000011,B11100011,B11110011,B11111011,B11011111,B11001111,B11000111,B11000011  // N
};


void setup()
{

  lcd.init();                      // initialize the lcd 
  lcd.backlight();
  lcd.print("Kapatawaran at");
  lcd.setCursor(0, 1);
  lcd.print("pagpapakumbaba.");


  for (int i=0; i<8; i++)        //set arduino pins to be outputs
  {
    pinMode(row[i], OUTPUT);
    pinMode(col[i], OUTPUT);
  }
}

void ledout(int iBitMapNo, int hOffset, int vOffset, int iDuration)     //Display bitmap number - iBitMapNo
                                                                        //With a horizontal offset of - hOffset
                                                                        //With a vertical offset of - vOffset
                                                                        //Keep the bimap displayed for - iDuration
{
  for (int i=0; i<iDuration; i++)        //repeat image a few times to ensure the eye can see it
  {
    for (int j=0; j<8; j++)              //for each row
    {
      if (j+vOffset>=0 && j+vOffset<8)   //are we within the boundaries of the chosen bitmap
      {
        for (int k=0; k<8; k++)          //set leds in current row according to the bitmap
        {
          digitalWrite(col[k], !bitRead(ledBitmap[iBitMapNo][j+vOffset],k+hOffset));
        }
        digitalWrite(row[j], HIGH);      //display the row, lighting required leds
      }
      delay(1);                          //pause for it to be seen
      digitalWrite(row[j], LOW);         //turn row off again
    }
  }
}

void loop()
{
  
    ledout(0,0,0,50);            // Heart
//    ledout(1,0,0,30);            // Blank
//    ledout(2,0,0,30);            // Heart
//    ledout(3,0,0,30);            // Blank
//    ledout(4,0,0,30);            // E
//    ledout(5,0,0,30);            // L
//    ledout(6,0,0,30);            // D
//    ledout(7,0,0,30);            // O
//    ledout(8,0,0,30);            // N
}
