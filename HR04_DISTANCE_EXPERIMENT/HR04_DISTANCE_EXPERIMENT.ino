// DISTANCE SENSOR
int trigPin = 5;
int echoPin = 4;

long soundSpeed = 343; // meters per second

void setup() {
  // Distance Sensor
  Serial.begin (9600);
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
}

void loop() {
  float duration, distance;

  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);

  duration = pulseIn(echoPin, HIGH); // value is in microseconds
  duration = duration / 1000000; // convert microseconds to seconds
  distance = (duration / 2) * soundSpeed;
  distance = distance * 100;

  if (distance < 2000) {
    Serial.print(distance);
    Serial.println(" centimeters");
  }


  delay(100);
}
