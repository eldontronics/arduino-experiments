int buzzerPin = 9;

void setup(){
  Serial.begin(9600);
  pinMode(buzzerPin, OUTPUT);
}

void loop(){

  int sensorReading = analogRead(A5);
  int sensorPitch = map(sensorReading, 100, 400, 120, 1500);

  Serial.println(sensorReading);
  
  tone(buzzerPin, sensorPitch, 100);
  
  delay(1000);
 noTone(buzzerPin);
  delay(10);

}

