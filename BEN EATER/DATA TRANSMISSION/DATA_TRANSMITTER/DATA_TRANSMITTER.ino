#include <LiquidCrystal_I2C.h>
LiquidCrystal_I2C lcd(0x3B, 16, 2);

int SWITCH = 4;
int TX_DATA = 3;
int TX_CLK = 2;
bool isTransmitting = false;

int bitPosition = 0;

int TX_BPS = 40; // bits per second
char *message = "Eldric Marcus Tenorio";

void setup() {
  Serial.begin(9600);
  pinMode(TX_DATA, OUTPUT);
  pinMode(TX_CLK, OUTPUT);
  pinMode(SWITCH, INPUT);

  lcd.init();
  lcd.backlight();
  lcd.setCursor(0, 0);
  lcd.print("Transmitting...");
  lcd.setCursor(0, 1);
  lcd.print("Please wait...");

  delay(3000);

  transmitData();

}

void transmitData() {

  isTransmitting = true;

  // Serial.println("Transmitting...");

  for (int dataIndex = 0; dataIndex < strlen(message); dataIndex++) {

    char txChar = message[dataIndex];

    lcd.setCursor(0, 1);
    lcd.print("                ");

    lcd.setCursor(15, 1);
    lcd.print(txChar);


    for (int bitIndex = 0; bitIndex < 8; bitIndex++) {

      bool txBit = txChar & (0x80 >> bitIndex);

      digitalWrite(TX_DATA, txBit);
      digitalWrite(TX_CLK, HIGH);
      delay(1000 / TX_BPS / 2);
      digitalWrite(TX_CLK, LOW);

      // Serial.print(txBit);
      lcd.setCursor(bitIndex, 1);
      lcd.print(txBit);


      delay(1000 / TX_BPS / 2);

    }

    // Serial.println();

  }
  // Serial.println();
  digitalWrite(TX_DATA, LOW);
  isTransmitting = false;
}

void loop() {

  if (digitalRead(SWITCH) == HIGH) {
    if (!isTransmitting) {
      transmitData();
    }
  }

}


