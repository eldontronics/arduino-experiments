#include <LiquidCrystal_I2C.h>
LiquidCrystal_I2C lcd(0x3F, 16, 2);

int RX_CLK = 2;
int RX_DATA = 3;
volatile int bitPosition = 0;
volatile bool isUpdateLCD = false;
volatile byte rxByte = 0;
bool rxBit;

void setup() {
  Serial.begin(9600);
  pinMode(RX_DATA, INPUT);

  lcd.init();
  lcd.backlight();
  lcd.setCursor(0, 0);
  lcd.print("Receiving data..");

  Serial.println("Ready to receive data...");
  attachInterrupt(digitalPinToInterrupt(RX_CLK), onClockPulse, RISING);

}

void onClockPulse() {
  rxBit = digitalRead(RX_DATA);
  isUpdateLCD = true;

  if (bitPosition == 8) {
    bitPosition = 0;
  }
}

void loop() {
  if (isUpdateLCD) {
    isUpdateLCD = false;

    if (rxBit) {
      rxByte |= (0x80 >> bitPosition);
    }

    if (bitPosition == 0) {
      lcd.setCursor(0, 1);
      lcd.print("                ");
    }
    lcd.setCursor(bitPosition , 1);
    lcd.print(rxBit);

    bitPosition++;

    if (bitPosition == 8) {      
      lcd.setCursor(15, 1);
      lcd.print(char(rxByte));      
      rxByte = 0;
    }
  }
}
