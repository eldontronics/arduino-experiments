#include <LiquidCrystal_I2C.h>
#include <SoftwareSerial.h>

LiquidCrystal_I2C lcd(0x3F,16,2);  // set the LCD address to 0x27 for a 16 chars and 2 line display

SoftwareSerial bluetooth (10, 11); // D10 is TX on BLE, D11 is RX in BLE

int ledPin1 = 8;
int ledPin2 = 7;
int ledPin3 = 6;
int toggleSwitch1 = -1;
int toggleSwitch2 = -1;
int toggleSwitch3 = -1;
int receivedCode = 0;
String receivedBluetoothString = "";

void setup(){
  bluetooth.begin(9600);
  Serial.begin(9600);
  lcd.init();
  lcd.noBacklight();
  delay(100);
  lcd.backlight();

  outputToLCD("Waiting for","bluetooth code..");
  Serial.println("Waiting for code...");
  pinMode(ledPin1, OUTPUT);  
  pinMode(ledPin2, OUTPUT);  
  pinMode(ledPin3, OUTPUT);  
  
}

void loop(){
  while(bluetooth.available() > 0){
    char receivedBluetoothChar = bluetooth.read();
    receivedBluetoothString += receivedBluetoothChar;

    Serial.println(receivedBluetoothChar);
    if(receivedBluetoothChar == '\n'){
      Serial.println(receivedBluetoothString.toInt());

      // LED 1
      if(receivedBluetoothString.toInt() == 11111111){
        toggleSwitch1 *= -1;
        if(toggleSwitch1 == 1){
          digitalWrite(ledPin1, HIGH);  
          outputToLCD("Comp: Red LED", "Optn: Turned ON");
        }
        else if (toggleSwitch1 == -1){
          digitalWrite(ledPin1, LOW);  
          outputToLCD("Comp: Red LED", "Optn: Turned OFF");
        }
      }

      // LED 2
      if(receivedBluetoothString.toInt() == 22222222){
        toggleSwitch2 *= -1;
        if(toggleSwitch2 == 1){
          digitalWrite(ledPin2, HIGH);  
          outputToLCD("Comp: Yellow LED", "Optn: Turned ON");
        }
        else if (toggleSwitch2 == -1){
          digitalWrite(ledPin2, LOW);  
          outputToLCD("Comp: Yellow LED", "Optn: Turned OFF");
        }
      }

      // LED 3
      if(receivedBluetoothString.toInt() == 33333333){
        toggleSwitch3 *= -1;
        if(toggleSwitch3 == 1){
          digitalWrite(ledPin3, HIGH);  
          outputToLCD("Comp: Blue LED", "Optn: Turned ON");
        }
        else if (toggleSwitch3 == -1){
          digitalWrite(ledPin3, LOW);  
          outputToLCD("Comp: Blue LED", "Optn: Turned OFF");
        }
      }

      
      receivedBluetoothString = "";
    }
  }
}

void outputToLCD(String rowOneMessage, String rowTwoMessage){
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print(rowOneMessage);
  lcd.setCursor(0, 1);
  lcd.print(rowTwoMessage);  
}

