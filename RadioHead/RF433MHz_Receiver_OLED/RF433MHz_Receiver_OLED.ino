// ask_receiver.pde
// -*- mode: C++ -*-
// Simple example of how to use RadioHead to receive messages
// with a simple ASK transmitter in a very simple way.
// Implements a simplex (one-way) receiver with an Rx-B1 module

#include <RH_ASK.h>
#include <SPI.h> // Not actualy used but needed to compile
#include <TM1637Display.h>

// Module connection pins (Digital Pins)
#define CLK 6
#define DIO 7

TM1637Display display1(CLK, DIO);

RH_ASK driver;
String receivedMsg = "";

void setup()
{
  Serial.begin(9600);  // Debugging only
  display1.setBrightness(7);

  if (!driver.init())
    Serial.println("init failed");
}

void loop()
{
  uint8_t buf[RH_ASK_MAX_MESSAGE_LEN];
  uint8_t buflen = sizeof(buf);

  if (driver.recv(buf, &buflen)) // Non-blocking
  {
    int i;

    // Serial.print("Received data: ");
    for (int bufIndex = 0; bufIndex < buflen; bufIndex++) {
      //Serial.print((char)(buf[bufIndex]));
      receivedMsg += (char)(buf[bufIndex]);
    }
    Serial.println(receivedMsg);
    display1.showNumberDec(receivedMsg.toInt(), false, 4, 0); // number to be shown, show leading 0s, start, least significant (to show correctly, use 4,0)

    
    delay(100);

  }
  else {
    receivedMsg = "";
  }

}
