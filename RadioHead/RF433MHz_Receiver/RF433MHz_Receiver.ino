// ask_receiver.pde
// -*- mode: C++ -*-
// Simple example of how to use RadioHead to receive messages
// with a simple ASK transmitter in a very simple way.
// Implements a simplex (one-way) receiver with an Rx-B1 module

#include <RH_ASK.h>
#include <SPI.h> // Not actualy used but needed to compile
#include <LiquidCrystal_I2C.h>

LiquidCrystal_I2C lcd(0x3E, 16, 2); // set the LCD address to 0x27 for a 16 chars and 2 line display, A2 (0x3B) in I2C is soldered A0 (0x3E)

RH_ASK driver;
String receivedMsg = "";

void setup()
{
  Serial.begin(9600);  // Debugging only

  lcd.init();
  lcd.backlight();


  if (!driver.init())
    Serial.println("init failed");
}

void loop()
{
  uint8_t buf[RH_ASK_MAX_MESSAGE_LEN];
  uint8_t buflen = sizeof(buf);

  if (driver.recv(buf, &buflen)) // Non-blocking
  {
    int i;
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Received data:");
    Serial.print("Received data: ");
    for (int bufIndex = 0; bufIndex < buflen; bufIndex++) {
      // Serial.print((char)(buf[bufIndex]));
      receivedMsg += (char)(buf[bufIndex]);
    }

    lcd.setCursor(0, 1);
    lcd.print(receivedMsg);
    

    Serial.println(receivedMsg);
  }
  else {
    receivedMsg = "";
  }

}
