// ask_transmitter.pde
// -*- mode: C++ -*-
// Simple example of how to use RadioHead to transmit messages
// with a simple ASK transmitter in a very simple way.
// Implements a simplex (one-way) transmitter with an TX-C1 module

#include <RH_ASK.h>
#include <SPI.h> // Not actually used but needed to compile
#include <LiquidCrystal_I2C.h>

LiquidCrystal_I2C lcd(0x3F, 16, 2); // set the LCD address to 0x27 for a 16 chars and 2 line display

RH_ASK driver;

void setup()
{
  Serial.begin(9600);    // Debugging only
  lcd.init();
  lcd.backlight();

  if (!driver.init())
    Serial.println("init failed");
}

void loop()
{
  String mData = (String) random(1000, 9999);
  char msg[10];

  mData.toCharArray(msg, sizeof(msg));
  lcd.clear();

  lcd.setCursor(0, 0);
  lcd.print("Sending data:");
  Serial.print("Sending data: ");

  lcd.setCursor(0, 1);
  lcd.print(mData);
  Serial.println(mData);
  driver.send((uint8_t *)msg, strlen(msg));
  driver.waitPacketSent();
  delay(100);

}
