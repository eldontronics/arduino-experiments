//Utilitzem la llibreria que instal·la per defecte SoftwareSerial.h//
#include <SoftwareSerial.h>

//configurem els ports digitals 4 i 5 com a TX i RX respectivament//
SoftwareSerial RFID = SoftwareSerial(4, 5);

//definim les variables//
char caracter;
String llegir_id;

void setup()
{
  Serial.begin(9600);
  RFID.begin(9600);
}

void loop() {

  while (RFID.available() > 0)
  {
    caracter = RFID.read();
    llegir_id += caracter;
  }

  if (llegir_id.length() > 10) {
    llegir_id = llegir_id.substring(1, 11);
    Serial.println(llegir_id);
    // our_id = "";
  }
  delay(1000);
}










































///*
//   RFID-125
//   (c) 2017, Agis Wichert
//*/
//#include <SoftwareSerial.h>
//
//// RFID  | Nano
//// Pin 1 | D2
//// Pin 2 | D3
//SoftwareSerial Rfid = SoftwareSerial(2, 3); // RX of Nano connects to TX of RDM6300, TX of Nano connects to RX of RDM6300
//
//void setup() {
//  // Serial Monitor to see results on the computer
//  Serial.begin(9600);
//  // Communication to the RFID reader
//  Rfid.begin(9600);
//  Serial.println("125kHz ready...");
//}
//
//void loop() {
//  // check, if any data is available
//  if (Rfid.available() > 0 ) {
//    // as long as there is data available...
//    while (Rfid.available() > 0 ) {
//      // read a byte
//      int r = Rfid.read();
//      // print it to the serial monitor
//      Serial.print(r, DEC);
//      Serial.print(" ");
//    }
//    // linebreak
//    Serial.println();
//  }
//}
