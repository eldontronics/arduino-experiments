int ldrTopLeft      = A1;
int ldrTopRight     = A2;
int ldrBottomLeft   = A3;
int ldrBottomRight  = A4;

void setup() {
  Serial.begin(9600);
}

void loop() {
  int ldrTopLeftValue = analogRead(ldrTopLeft);
  int ldrTopRightValue = analogRead(ldrTopRight);
  int ldrBottomLeftValue = analogRead(ldrBottomLeft);
  int ldrBOttomRightValue = analogRead(ldrBottomRight);

  
  int mappedldrTopLeftValue = map(ldrTopLeft, 0, 1023, 0, 255);
  int mappedldrTopRightValue = map(ldrTopRight, 0, 1023, 0, 255);
  int mappedldrBottomLeftValue = map(ldrBottomLeft, 0, 1023, 0, 255);
  int mappedldrBottomRightValue = map(ldrBottomRight, 0, 1023, 0, 255);

  Serial.print("Top Left    : "); Serial.println(mappedldrTopLeftValue);
  Serial.print("Top Right   : "); Serial.println(mappedldrTopRightValue);
  Serial.print("Bottom Left : "); Serial.println(mappedldrBottomLeftValue);
  Serial.print("Bottom Right: "); Serial.println(mappedldrBottomRightValue);
  Serial.println("");
  

  delay(1000);

}
