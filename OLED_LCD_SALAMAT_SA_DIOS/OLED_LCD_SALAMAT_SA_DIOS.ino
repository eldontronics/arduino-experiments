#include <LiquidCrystal_I2C.h>
#include "U8glib.h"


LiquidCrystal_I2C lcd(0x3F, 16, 2);
U8GLIB_SSD1306_128X64 u8g(U8G_I2C_OPT_DEV_0 | U8G_I2C_OPT_NO_ACK | U8G_I2C_OPT_FAST);	// Fast I2C / TWI

void draw(void) {
  // graphic commands to redraw the complete screen should be placed here
  u8g.setFont(u8g_font_unifont);
  u8g.setFontPosTop();
  u8g.drawStr(15, 1, "Thanks be to");
  //u8g.drawHLine(0, 1+14, 40);
  u8g.setScale2x2();					// Scale up all draw procedures
  u8g.drawStr(20, 10, "GOD");			// actual display position is (0,24)
  // u8g.drawHLine(0, 12+14, 40);		// All other procedures are also affected
  u8g.undoScale();					// IMPORTANT: Switch back to normal mode
  u8g.drawStr(0, 50, "HAPPY SPBB ALL!");      // actual display position is (0,24)
}

void setup(void) {


  lcd.init();
  lcd.noBacklight();
  delay(500);
  lcd.backlight();

  lcd.setCursor(1, 0);
  lcd.print("2 Corinto 9:15");

  lcd.setCursor(1, 1);
  lcd.print("Zacarias  8:21");

  // flip screen, if required
  // u8g.setRot180();

  // set SPI backup if required
  //u8g.setHardwareBackup(u8g_backup_avr_spi);

  // assign default color value
  if ( u8g.getMode() == U8G_MODE_R3G3B2 )
    u8g.setColorIndex(255);     // white
  else if ( u8g.getMode() == U8G_MODE_GRAY2BIT )
    u8g.setColorIndex(3);         // max intensity
  else if ( u8g.getMode() == U8G_MODE_BW )
    u8g.setColorIndex(1);         // pixel on
}

void loop(void) {
  // picture loop
  u8g.firstPage();
  do {
    draw();
  } while ( u8g.nextPage() );

  // rebuild the picture after some delay
  delay(500);
}

