#include <LiquidCrystal_I2C.h>
#include <SoftwareSerial.h>

LiquidCrystal_I2C lcd(0x3F,16,2);  // set the LCD address to 0x27 for a 16 chars and 2 line display
SoftwareSerial bluetooth (10, 11); // D10 is TX on BLE, D11 is RX in BLE

int receivedCode = 0;
String receivedBluetoothString = "";

void setup(){
  bluetooth.begin(9600);
  Serial.begin(9600);
  lcd.init();
  lcd.noBacklight();
  delay(100);
  lcd.backlight();

  outputToLCD("Waiting for code","      ....");
  Serial.println("Waiting for code...");  
}

void loop(){

  while(bluetooth.available() > 0){
    char receivedBluetoothChar = bluetooth.read();
    if(receivedBluetoothChar != '\n'){
      receivedBluetoothString += receivedBluetoothChar;  
    }    
    delay(10); // 10
  }

 
 if(receivedBluetoothString != ""){
   if(receivedBluetoothString == "CLEAR"){
        lcd.noBacklight();
        delay(100);
        lcd.backlight();      
        outputToLCD("Waiting for code","      ....");
        Serial.println("Waiting for code...");     
   }
  else{
    outputToLCD("Received code:", receivedBluetoothString);
    
  }
  receivedBluetoothString = "";    
 }
}

void outputToLCD(String rowOneMessage, String rowTwoMessage){
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print(rowOneMessage);
  lcd.setCursor(0, 1);
  lcd.print(rowTwoMessage);  
}

