/*
   Initial Author: ryand1011 (https://github.com/ryand1011)

   Reads data written by a program such as "rfid_write_personal_data.ino"

   See: https://github.com/miguelbalboa/rfid/tree/master/examples/rfid_write_personal_data

   Uses MIFARE RFID card using RFID-RC522 reader
   Uses MFRC522 - Library
   -----------------------------------------------------------------------------------------
               MFRC522      Arduino       Arduino   Arduino    Arduino          Arduino
               Reader/PCD   Uno/101       Mega      Nano v3    Leonardo/Micro   Pro Micro
   Signal      Pin          Pin           Pin       Pin        Pin              Pin
   -----------------------------------------------------------------------------------------
   RST/Reset   RST          9             5         D9         RESET/ICSP-5     RST
   SPI SS      SDA(SS)      10            53        D10        10               10
   SPI MOSI    MOSI         11 / ICSP-4   51        D11        ICSP-4           16
   SPI MISO    MISO         12 / ICSP-1   50        D12        ICSP-1           14
   SPI SCK     SCK          13 / ICSP-3   52        D13        ICSP-3           15
*/

#include <SPI.h>
#include <MFRC522.h>
#include <LiquidCrystal_I2C.h>

#define RST_PIN         9           // Configurable, see typical pin layout above
#define SS_PIN          10          // Configurable, see typical pin layout above

MFRC522 mfrc522(SS_PIN, RST_PIN);   // Create MFRC522 instance
LiquidCrystal_I2C lcd(0x3B, 16, 2);

int myCardUIDS[2][4] = {
  {32, 149, 120, 81},
  {246, 245, 252, 6},
};


//*****************************************************************************************//
void setup() {
  Serial.begin(9600);                                           // Initialize serial communications with the PC

  lcd.init();
  lcd.backlight();

  SPI.begin();                                                  // Init SPI bus
  mfrc522.PCD_Init();                                              // Init MFRC522 card
  Serial.println(F("Read personal data on a MIFARE PICC:"));    //shows in serial that it is ready to read
}

//*****************************************************************************************//
void loop() {


  // Prepare key - all keys are set to FFFFFFFFFFFFh at chip delivery from the factory.
  MFRC522::MIFARE_Key key;
  for (byte i = 0; i < 6; i++) key.keyByte[i] = 0xFF;

  //some variables we need
  byte block;
  byte len;
  MFRC522::StatusCode status;



  //-------------------------------------------

  // Look for new cards
  if ( ! mfrc522.PICC_IsNewCardPresent()) {


    lcd.setCursor(0, 0);
    lcd.print("Ready...");
    lcd.setCursor(0, 1);
    lcd.print("Place card.");
    return;
  }


  // Select one of the cards
  if ( ! mfrc522.PICC_ReadCardSerial()) {
    return;
  }

  Serial.println(F("**Card Detected:**"));

  //-------------------------------------------



  //Validate if UID is valid




  mfrc522.PICC_DumpDetailsToSerial(&(mfrc522.uid)); //dump some details about the card

  //mfrc522.PICC_DumpToSerial(&(mfrc522.uid));      //uncomment this to see all blocks in hex

  //-------------------------------------------

  Serial.print(F("Name: "));

  byte buffer1[18];

  block = 4;
  len = 18;

  //------------------------------------------- GET FIRST NAME
  status = mfrc522.PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_A, 4, &key, &(mfrc522.uid)); //line 834 of MFRC522.cpp file
  if (status != MFRC522::STATUS_OK) {
    Serial.print(F("Authentication failed: "));
    Serial.println(mfrc522.GetStatusCodeName(status));

    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("AUTH Failed!");
    lcd.setCursor(0, 1);
    lcd.print("Place card again");

    return;
  }

  status = mfrc522.MIFARE_Read(block, buffer1, &len);
  if (status != MFRC522::STATUS_OK) {
    Serial.print(F("Reading failed: "));
    Serial.println(mfrc522.GetStatusCodeName(status));
    return;
  } else {
    lcd.clear();
  }

  if (!autheticateOwnerUID()) {
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Invalid card!");
    lcd.setCursor(0, 1);
    lcd.print("Place card again");
    delay(2000);
    lcd.clear();
  }
  else {




    lcd.setCursor(0, 0);
    //PRINT FIRST NAME
    for (uint8_t i = 0; i < 16; i++)
    {

      Serial.print(buffer1[i]);
      Serial.print(" ");

      if ( (buffer1[i] != 10) && (buffer1[i] != 13))
      {
        // lcd.write(buffer1[i]);
        Serial.print((int)buffer1[i]);
      }
    }


    Serial.print(" ");


    //---------------------------------------- GET LAST NAME

    byte buffer2[18];
    block = 1;

    status = mfrc522.PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_A, 1, &key, &(mfrc522.uid)); //line 834
    if (status != MFRC522::STATUS_OK) {
      Serial.print(F("Authentication failed: "));
      Serial.println(mfrc522.GetStatusCodeName(status));
      return;
    }

    status = mfrc522.MIFARE_Read(block, buffer2, &len);
    if (status != MFRC522::STATUS_OK) {
      Serial.print(F("Reading failed: "));
      Serial.println(mfrc522.GetStatusCodeName(status));
      return;
    }

    // String lName = "";
    //PRINT LAST NAME
    lcd.setCursor(0, 1);
    for (uint8_t i = 0; i < 16; i++) {

      Serial.print(buffer2[i]);
      Serial.print(" ");


      if ( (buffer2[i] != 10) && (buffer2[i] != 13)) {

        // lcd.write(buffer2[i]);
        Serial.print(buffer2[i]);

      }
    }

    delay(1000);
    lcd.clear();

    //----------------------------------------

    Serial.println(F("\n**End Reading**\n"));
  }
  //delay(1000); //change value if you want to read cards faster

  mfrc522.PICC_HaltA();
  mfrc522.PCD_StopCrypto1();

  lcd.clear();
}


bool autheticateOwnerUID() {

  int tmpCount = 0;
  int countCorrect = 0;

  for (int uIDs = 0; uIDs < 2; uIDs++) {

    for (byte i = 0; i < mfrc522.uid.size; i++) {

//      Serial.print("Card:");
//      Serial.print(mfrc522.uid.uidByte[i]);
//      Serial.print("Array:");
//      Serial.print(myCardUIDS[uIDs][i]);

      if (myCardUIDS[uIDs][i] == mfrc522.uid.uidByte[i]) tmpCount++;

    }

    if (tmpCount == 4) countCorrect++;
    tmpCount = 0;
  }

  return (countCorrect > 0);

}







//*****************************************************************************************//
