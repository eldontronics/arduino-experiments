#include <Servo.h>

Servo gateServo;

// Set Servo Pin
int servoPin = 11;

// Set the Distance Sensor Pins
int trigPin = 13;
int echoPin = 12;

// Set the 7-segment Pins
int a = 2;  //For displaying segment "a"
int b = 3;  //For displaying segment "b"
int c = 4;  //For displaying segment "c"
int d = 5;  //For displaying segment "d"
int e = 6;  //For displaying segment "e"
int f = 8;  //For displaying segment "f"
int g = 9;  //For displaying segment "g"

long soundSpeed = 343; // meters per second

void setup() {
  Serial.begin(9600);

  // Initialize servo
  gateServo.attach(servoPin);
  gateServo.write(0); // at default, gate is closed, 0 degrees

  // 7-Segment PinModes
  pinMode(a, OUTPUT);  // segment a
  pinMode(b, OUTPUT);  // segment b
  pinMode(c, OUTPUT);  // segment c
  pinMode(d, OUTPUT);  // segment d
  pinMode(e, OUTPUT);  // segment e
  pinMode(f, OUTPUT);  // segment f
  pinMode(g, OUTPUT);  // segment g

  // Distance Sensor PinModes
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
}

void loop() {
  
  int distanceInCm = getDistanceFromObjectInCm();
  
  Serial.println(distanceInCm);
  
  if (distanceInCm > 9) {
    turnOffAllSegments();
  }
  else {
    displayDigit(distanceInCm);
  }

  if (distanceInCm < 5) {
    gateServo.write(90);
  }
  else {
    gateServo.write(0);
  }

  delay(200);
}

int getDistanceFromObjectInCm() {
  
  float duration, distance;
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);

  duration = pulseIn(echoPin, HIGH); // value is in microseconds
  duration = duration / 1000000; // convert microseconds to seconds
  distance = (duration / 2) * soundSpeed;
  distance = distance * 100;

  return distance;
}

void displayDigit(int number) {
  if (number == 0) {
    digitZero();
  }
  else if (number == 1) {
    digitOne();
  }
  else if (number == 2) {
    digitTwo();
  }
  else if (number == 3) {
    digitThree();
  }
  else if (number == 4) {
    digitFour();
  }
  else if (number == 5) {
    digitFive();
  }
  else if (number == 6) {
    digitSix();
  }
  else if (number == 7) {
    digitSeven();
  }
  else if (number == 8) {
    digitEight();
  }
  else if (number == 9) {
    digitNine();
  }
}

void digitOne() {
  digitalWrite(a, LOW);
  digitalWrite(b, HIGH);
  digitalWrite(c, HIGH);
  digitalWrite(d, LOW);
  digitalWrite(e, LOW);
  digitalWrite(f, LOW);
  digitalWrite(g, LOW);

}

void digitTwo() {
  digitalWrite(a, HIGH);
  digitalWrite(b, HIGH);
  digitalWrite(c, LOW);
  digitalWrite(d, HIGH);
  digitalWrite(e, HIGH);
  digitalWrite(f, LOW);
  digitalWrite(g, HIGH);

}

void digitThree() {
  digitalWrite(a, HIGH);
  digitalWrite(b, HIGH);
  digitalWrite(c, HIGH);
  digitalWrite(d, HIGH);
  digitalWrite(e, LOW);
  digitalWrite(f, LOW);
  digitalWrite(g, HIGH);

}

void digitFour() {
  digitalWrite(a, LOW);
  digitalWrite(b, HIGH);
  digitalWrite(c, HIGH);
  digitalWrite(d, LOW);
  digitalWrite(e, LOW);
  digitalWrite(f, HIGH);
  digitalWrite(g, HIGH);

}

void digitFive() {
  digitalWrite(a, HIGH);
  digitalWrite(b, LOW);
  digitalWrite(c, HIGH);
  digitalWrite(d, HIGH);
  digitalWrite(e, LOW);
  digitalWrite(f, HIGH);
  digitalWrite(g, HIGH);

}

void digitSix() {
  digitalWrite(a, HIGH);
  digitalWrite(b, LOW);
  digitalWrite(c, HIGH);
  digitalWrite(d, HIGH);
  digitalWrite(e, HIGH);
  digitalWrite(f, HIGH);
  digitalWrite(g, HIGH);

}

void digitSeven() {
  digitalWrite(a, HIGH);
  digitalWrite(b, HIGH);
  digitalWrite(c, HIGH);
  digitalWrite(d, LOW);
  digitalWrite(e, LOW);
  digitalWrite(f, LOW);
  digitalWrite(g, LOW);

}

void digitEight() {
  digitalWrite(a, HIGH);
  digitalWrite(b, HIGH);
  digitalWrite(c, HIGH);
  digitalWrite(d, HIGH);
  digitalWrite(e, HIGH);
  digitalWrite(f, HIGH);
  digitalWrite(g, HIGH);

}

void digitNine() {
  digitalWrite(a, HIGH);
  digitalWrite(b, HIGH);
  digitalWrite(c, HIGH);
  digitalWrite(d, HIGH);
  digitalWrite(e, LOW);
  digitalWrite(f, HIGH);
  digitalWrite(g, HIGH);

}

void digitZero() {
  digitalWrite(a, HIGH);
  digitalWrite(b, HIGH);
  digitalWrite(c, HIGH);
  digitalWrite(d, HIGH);
  digitalWrite(e, HIGH);
  digitalWrite(f, HIGH);
  digitalWrite(g, LOW);

}

void turnOffAllSegments() {
  // turn all segments off
  digitalWrite(a, LOW);
  digitalWrite(b, LOW);
  digitalWrite(c, LOW);
  digitalWrite(d, LOW);
  digitalWrite(e, LOW);
  digitalWrite(f, LOW);
  digitalWrite(g, LOW);

}


