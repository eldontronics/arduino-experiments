/*
   Interactive nametag sketch for ATtiny85. Based on the Digispark
   ATtiny LCD example and the Adafruit "Measuring Sound Levels" example.
   Every 12000 milliseconds will change the display from my name to my website.
   On the second line, will measure and display sound levels.
   For more info check: https://platis.solutions/blog/2015/03/22/diy-interactive-name-tag/
   ATtiny85 I2C pins:
   ATtiny Pin 5 = SDA on DS1621  & GPIO
   ATtiny Pin 7 = SCK on DS1621  & GPIO
*/

#include <TinyWireM.h>                    // I2C Master lib for ATTinys which use USI - comment this out to use with standard arduinos
#include <LiquidCrystal_attiny.h>         // for LCD w/ GPIO MODIFIED for the ATtiny85
#include <RH_ASK.h>
//#include <SPI.h> // Not actually used but needed to compile


#define GPIO_ADDR 0x3F                    // the address of my i2c for LCD
#define COLS      20
#define ROWS      4
LiquidCrystal_I2C lcd(GPIO_ADDR, COLS, ROWS);  // set address & 16 chars / 2 lines


void setup() {
  lcd.init();
  lcd.noBacklight();
  delay(100);
  lcd.backlight();
  lcd.clear();

  lcd.clear();
  lcd.setCursor(0, 0);
  displayText("  Salamat sa DIOS!");
  lcd.setCursor(0, 1);
  displayText(" Happy Thanksgiving");
  lcd.setCursor(0, 2);
  displayText(" Members, Church of");
  lcd.setCursor(0, 3);
  displayText(" GOD  International");
}

void loop() {
  //  for (int num = 0; num <= 1000; num++) {
  //    lcd.clear();
  //    lcd.setCursor(0, 0);
  //    displayText("Counting:");
  //    lcd.setCursor(0, 1);
  //    displayText("Number : ");
  //    displayText((String) num);
  //    lcd.setCursor(0, 2);
  //    displayText("Eldric Marcus");
  //    lcd.setCursor(0, 3);
  //    displayText("Berber Prumprum-Shi");
  //
  //    delay(200);
  //  }
}

void displayText(String str) {
  char charBuf[COLS];
  str.toCharArray(charBuf, COLS);
  for (int ind = 0; ind < str.length(); ind++) {
    lcd.print(charBuf[ind]);
  }
}
