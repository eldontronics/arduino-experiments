//#if defined (__arm__) && defined (__SAM3X8E__)
//#define _delay_us(us) delayMicroseconds(us)
//#else
// #include <util/delay.h>
//#endif

#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <TinyGPS++.h>
#include <SoftwareSerial.h>

#define LOGO16_GLCD_HEIGHT 64
#define LOGO16_GLCD_WIDTH 128
#define OLED_RESET          4

Adafruit_SSD1306 display(OLED_RESET);

static const int RXPin = 8, TXPin = 9;
static const uint32_t GPSBaud = 115200;

// The TinyGPS++ object
TinyGPSPlus gps;

// The serial connection to the GPS device
SoftwareSerial ss(RXPin, TXPin);

void setup()
{
  Serial.begin(115200);
  ss.begin(GPSBaud);
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);

  display.clearDisplay();
  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(0, 0);
  display.print(F("Testing TinyGPS++ library v. "));
  display.println(TinyGPSPlus::libraryVersion());
  display.println(F("by Mikal Hart"));
  display.println();
  display.println(F("Waiting GPS Data..."));
  display.println();
  display.display();

  Serial.println(F("DeviceExample.ino"));
  Serial.println(F("A simple demonstration of TinyGPS++ with an attached GPS module"));
  Serial.print(F("Testing TinyGPS++ library v. "));
  Serial.println(TinyGPSPlus::libraryVersion());
  Serial.println(F("by Mikal Hart"));
  Serial.println(F("Waiting for GPS data..."));
  Serial.println();
}

void loop()
{

  // This sketch displays information every time a new sentence is correctly encoded.
  Serial.println("Attempting to fetch data...");
  while (ss.available() > 0)
    if (gps.encode(ss.read()))
      displayInfo();

//  if (millis() > 60000 && gps.charsProcessed() < 10)
//  {
//    Serial.println(F("No GPS detected: check wiring."));
//    display.println(F("NO GPS DETECTED.\nCHECK WIRING."));
//    display.display();
//
//    while (true);
//  }
}

void displayInfo()
{

  Serial.print(F("Location: "));
  if (gps.location.isValid())
  {
    Serial.print(gps.location.lat(), 6);
    Serial.print(F(","));
    Serial.print(gps.location.lng(), 6);
  }
  else
  {
    //    display.println("Location: INVALID");
    Serial.print(F("INVALID"));
  }

  Serial.print(F("  Date/Time: "));
  if (gps.date.isValid())
  {
    Serial.print(gps.date.month());
    Serial.print(F("/"));
    Serial.print(gps.date.day());
    Serial.print(F("/"));
    Serial.print(gps.date.year());
  }
  else
  {
    //    display.println("Date: INVALID");
    Serial.print(F("INVALID"));
  }

  Serial.print(F(" "));
  if (gps.time.isValid())
  {
    if (gps.time.hour() < 10) Serial.print(F("0"));
    Serial.print(gps.time.hour());
    Serial.print(F(":"));
    if (gps.time.minute() < 10) Serial.print(F("0"));
    Serial.print(gps.time.minute());
    Serial.print(F(":"));
    if (gps.time.second() < 10) Serial.print(F("0"));
    Serial.print(gps.time.second());
    Serial.print(F("."));
    if (gps.time.centisecond() < 10) Serial.print(F("0"));
    Serial.print(gps.time.centisecond());
  }
  else
  {
    Serial.print(F("Time: INVALID"));
  }
  display.display();
  Serial.println();
}
