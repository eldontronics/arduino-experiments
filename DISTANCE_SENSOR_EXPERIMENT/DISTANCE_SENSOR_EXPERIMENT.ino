// Pins in the Distance Sensor (HC-SR04)

int trigPin = 13;
int echoPin = 12;
long soundSpeed = 343; // meters per second

void setup() {
  Serial.begin (9600);
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
}

void loop() {
  float duration, distance;
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW); 
  duration = pulseIn(echoPin, HIGH); // value is in microseconds
  duration = duration / 1000000; // convert microseconds to seconds
  distance = (duration/2) * soundSpeed;
  Serial.print(distance * 100); // convert from meters to centimeters
  Serial.println(" centimeters");    
  delay(100);
}











