#include <Servo.h>

Servo testServo1;

int servoPin1 = 9;

void setup() {
  // put your setup code here, to run once:

  Serial.begin(9600);
  
  testServo1.attach(servoPin1);
  testServo1.write(0);
}

void loop() {

  testServo1.write(0);
  delay(1000);
  testServo1.write(90);
  delay(1000);
  testServo1.write(180);
  delay(1000);
  testServo1.write(90);
  delay(1000);
 
}
