#include <SPI.h>
#include <Adafruit_GFX.h>
#include <Adafruit_PCD8544.h>
Adafruit_PCD8544 display = Adafruit_PCD8544(3, 4, 5, 7, 6);

int x = 0;
String text = "      Eldric Marcus Cruz Tenorio";
void setup() {
  display.begin();
  display.clearDisplay();
  display.setContrast(80);
}
void loop() {
  display.setTextSize(1);
  display.clearDisplay();
  display.setTextColor(BLACK);
  display.println(text.substring(x, x + 13));
  x++;
  if (x > 42)
  {
    x = 0;
  }
  display.setTextSize(5);
  display.println(millis() / 1000);
  display.display();
  delay(300);
}
